"""helper functions
"""

import numpy as np
from scipy.signal import butter, filtfilt
from scipy import constants

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a


def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = filtfilt(b, a, data)
    return y


def calculate_Ep(wl):
    """distinct energy quanta for wavelengths (in nm)
    """
    return constants.Planck*((constants.c)/(wl*10**-9))


def calculate_photonflux(irradiance, wl):
    """convert power in Watts or Watts/m^2 to photon flux in mol/(m^2*s)

    Parameters
    ----------
    irradiance: array-like
        Irradiance measurement in watts/m^2.
    wl: array-like, int, float
        wavelengths (in nm).

    Returns
    -------
    EQF: array-like
        photon flux in mol/(m^2*s)
    """
    Ep = calculate_Ep(wl)
    Np = irradiance/Ep
    EQF = Np/constants.Avogadro
    return EQF
