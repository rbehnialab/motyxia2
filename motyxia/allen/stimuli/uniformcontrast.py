from .stim import Stim
from .util import *
import os
import numpy as np
import matplotlib.pyplot as plt
import random
import time
import h5py
import warnings
from itertools import product
from scipy.stats import truncnorm

from . import tools
from pathlib import Path
import pickle
import uuid


try:
    import skimage.external.tifffile as tf
except ImportError:
    import tifffile as tf


class UniformContrast(Stim):
    """
    Generate full field uniform luminance for recording spontaneous activity.
    Inherits from Stim.

    The full field uniform luminance stimulus presents a fixed background color
    which is normally taken to be grey.

    Parameters
    ----------
    monitor : monitor object
        contains display monitor information
    indicator : indicator object
        contains indicator information
    coordinate : str from {'degree','linear'}, optional
        specifies coordinates, defaults to 'degree'
    background : float, optional
        color of background. Takes values in [-1,1] where -1 is black and 1
        is white
    pregap_dur : float, optional
        amount of time (in seconds) before the stimulus is presented, defaults
        to `2.`
    postgap_dur : float, optional
        amount of time (in seconds) after the stimulus is presented, defaults
        to `3.`
    color : float, optional
        the choice of color to display in the stimulus, defaults to `0.` which
        is grey
    """

    def __init__(self, monitor, indicator, duration, color=0., pregap_dur=2.,
                 postgap_dur=3., background=0., coordinate='degree', seed=None):
        """
        Initialize UniformContrast object
        """

        super(UniformContrast, self).__init__(monitor=monitor,
                                              indicator=indicator,
                                              coordinate=coordinate,
                                              background=background,
                                              pregap_dur=pregap_dur,
                                              postgap_dur=postgap_dur,
                                              seed=seed)

        #self.stim_name = 'UniformContrast'
        self.duration = duration
        self.color = float(color)
        self.frame_config = ('is_display', 'indicator color [-1., 1.]')

    def generate_frames(self):
        """
        generate a tuple of parameters with information for each frame.

        Information contained in each frame:
             first element -
                  during display frames, value takes on 1 and value
                  is 0 otherwise
             second element - color of indicator
                  during display value is equal to 1 and during gaps value is
                  equal to -1
        """

        displayframe_num = int(self.duration * self.monitor.refresh_rate)

        frames = [(0, -1.)] * self.pregap_frame_num + \
                 [(1, 1.)] * displayframe_num + \
                 [(0, -1.)] * self.postgap_frame_num

        return tuple(frames)

    def _generate_frames_for_index_display(self):
        """ parameters are predefined here, nothing to compute. """
        if self.indicator.is_sync:
            # Parameters that define the stimulus
            frames = ((0, -1.), (1, 1.))
            return frames
        else:
            raise NotImplementedError("method not avaialable for non-sync indicator.")

    def _generate_display_index(self):
        """ compute a list of indices corresponding to each frame to display. """
        displayframe_num = int(self.duration * self.monitor.refresh_rate)
        index_to_display = [0] * self.pregap_frame_num + [1] * displayframe_num + \
                           [0] * self.postgap_frame_num
        return index_to_display

    def generate_movie_by_index(self):
        """ compute the stimulus movie to be displayed by index. """
        #start addition
        filename=self.stored_filename();
        is_caching_active_resp=self.check_is_caching_active(filename)
        if len(is_caching_active_resp)==2:
            return is_caching_active_resp[0],is_caching_active_resp[1]
        else:
            dirpath,endpath_np_short,endpath_seq_short,fmp,endpath_seq,endpath_np=is_caching_active_resp
            self.frames_unique = self._generate_frames_for_index_display()
            self.index_to_display = self._generate_display_index()

            num_frames = len(self.frames_unique)
            num_pixels_width = self.monitor.deg_coord_x.shape[0]
            num_pixels_height = self.monitor.deg_coord_x.shape[1]

            # Initialize numpy array of 0's as placeholder for stimulus routine
            full_sequence = np.ones((num_frames,
                                     num_pixels_width,
                                     num_pixels_height),
                                    dtype=np.float32) * self.background

            # Compute pixel coordinates for indicator
            indicator_width_min = int(self.indicator.center_width_pixel
                                   - self.indicator.width_pixel / 2)
            indicator_width_max = int(self.indicator.center_width_pixel
                                   + self.indicator.width_pixel / 2)
            indicator_height_min = int(self.indicator.center_height_pixel
                                    - self.indicator.height_pixel / 2)
            indicator_height_max = int(self.indicator.center_height_pixel
                                    + self.indicator.height_pixel / 2)

            display = self.color * np.ones((num_pixels_width,num_pixels_height),
                                           dtype=np.float32)

            for i, frame in enumerate(self.frames_unique):
                if frame[0] == 1:
                    full_sequence[i] = display

                # Insert indicator pixels
                full_sequence[i, indicator_height_min:indicator_height_max,
                              indicator_width_min:indicator_width_max] = frame[1]

            monitor_dict = dict(self.monitor.__dict__)
            indicator_dict = dict(self.indicator.__dict__)
            NF_dict = dict(self.__dict__)
            NF_dict.pop('monitor')
            NF_dict.pop('indicator')
            full_dict = {'stimulation': NF_dict,
                         'monitor': monitor_dict,
                         'indicator': indicator_dict}
            # end addition
            self.write_seq_to_cache(dirpath,endpath_np_short,full_dict,endpath_seq_short, full_sequence,fmp,endpath_seq,endpath_np)
            return full_sequence, full_dict

    def generate_movie(self):
        """
        generate movie for uniform contrast display frame by frame.

        Returns
        -------
        full_seq : nd array, uint8
            3-d array of the stimulus to be displayed.
        full_dict : dict
            dictionary containing the information of the stimulus.
        """
        #start addition
        filename=self.stored_filename();
        is_caching_active_resp=self.check_is_caching_active(filename)
        if len(is_caching_active_resp)==2:
            return is_caching_active_resp[0],is_caching_active_resp[1]
        else:
            dirpath,endpath_np_short,endpath_seq_short,fmp,endpath_seq,endpath_np=is_caching_active_resp
            self.frames = self.generate_frames()

            full_seq = np.zeros((len(self.frames),
                                 self.monitor.deg_coord_x.shape[0],
                                 self.monitor.deg_coord_x.shape[1]),
                                dtype=np.float32)

            indicator_width_min = int(self.indicator.center_width_pixel
                                   - self.indicator.width_pixel / 2)
            indicator_width_max = int(self.indicator.center_width_pixel
                                   + self.indicator.width_pixel / 2)
            indicator_height_min = int(self.indicator.center_height_pixel
                                    - self.indicator.height_pixel / 2)
            indicator_height_max = int(self.indicator.center_height_pixel
                                    + self.indicator.height_pixel / 2)

            background = np.ones((np.size(self.monitor.deg_coord_x, 0),
                                  np.size(self.monitor.deg_coord_x, 1)),
                                 dtype=np.float32) * self.background

            display = np.ones((np.size(self.monitor.deg_coord_x, 0),
                               np.size(self.monitor.deg_coord_x, 1)),
                              dtype=np.float32) * self.color

            if not (self.coordinate == 'degree' or self.coordinate == 'linear'):
                raise LookupError("`coordinate` value not in {'degree','linear'}")

            for i in range(len(self.frames)):
                curr_frame = self.frames[i]

                if curr_frame[0] == 0:
                    curr_FC_seq = background
                else:
                    curr_FC_seq = display

                curr_FC_seq[indicator_height_min:indicator_height_max,
                indicator_width_min:indicator_width_max] = curr_frame[1]

                full_seq[i] = curr_FC_seq

                if i in np.arange(0, len(self.frames), len(self.frames) / 10):
                    print('Generating numpy sequence: ' +
                           str(int(100 * (i + 1) / len(self.frames))) + '%')

            mondict = dict(self.monitor.__dict__)
            indicator_dict = dict(self.indicator.__dict__)
            indicator_dict.pop('monitor')
            NFdict = dict(self.__dict__)
            NFdict.pop('monitor')
            NFdict.pop('indicator')
            full_dict = {'stimulation': NFdict,
                         'monitor': mondict,
                         'indicator': indicator_dict}
            # end addition
            self.write_seq_to_cache(dirpath,endpath_np_short,full_dict,endpath_seq_short, full_seq,fmp,endpath_seq,endpath_np)
            return full_seq, full_dict
