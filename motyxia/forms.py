import numpy as np
from collections import defaultdict
from copy import deepcopy
from glob import glob
import os

try:
    import nidaqmx.system
    NI = True
except ImportError:
    NI = False

try:
    ARDUINO = False
    import serial.tools.list_ports # To see the list of available Arduinos
    ports = list(serial.tools.list_ports.comports())
    for p in ports:
        if "Arduino" in p.description:
            ARDUINO = True
            break
except ImportError:
    ARDUINO = False

from flask_wtf import FlaskForm as Form
from wtforms import Form as NoCsrfForm
from wtforms import StringField, IntegerField, BooleanField, FloatField, \
    SelectField, FieldList, FormField, HiddenField
from wtforms.validators import InputRequired, Optional, NumberRange, \
    ValidationError

from . import STIMULITYPES

#TODO validator for SetFieldList if some are None
#TODO allow SetFieldList to have FormField
NONES = ['', None, 'None', 'null', 'NONE']


class EvalValidator:

    def __init__(self, instances=None, list_instances=None, func_validate=None):
        #check if
        self.instances = instances
        #iterate over data and check each element
        self.list_instances = list_instances
        #function to check
        self.func_validate = func_validate

    def __call__(self, form , field):

        #if it is nothing
        if field.data in NONES:
            return None

        if hasattr(field, 'noneval_data'):
            try:
                data = eval(field.noneval_data)
            except Exception as e:
                raise ValidationError(f'cannot evaluate data: {e}')
        else:
            data = field.data
            if isinstance(data, str):
                try:
                    data = eval(data)
                except Exception as e:
                    raise ValidationError(f'cannot evaluate data: {e}')

        if self.instances is not None:
            if not isinstance(data, self.instances):
                raise ValidationError(
                    f'data not {self.instances}, but {type(data)}')

        if self.list_instances is not None and isinstance(data, (list, tuple)):
            for idata in data:
                if not isinstance(idata, self.list_instances):
                    raise ValidationError(
                        f'data not {self.list_instances}, but {type(data)}')

        if self.func_validate is not None:
            if not self.func_validate(data):
                raise ValidationError(
                    f'data does not follow basic constraint of {self.func_validate}'
                )


class SetFieldList(FieldList):

    def __init__(self, *args, entries=2, **kwargs):
        kwargs.pop('min_entries', None)
        kwargs.pop('max_entries', None)
        super().__init__(
            *args, min_entries=entries, max_entries=entries, **kwargs)


class ListField(StringField):

    def process_data(self, value):
        return super().process_data(str(value))

    @property
    def noneval_data(self):
        data = super().__getattribute__('data')
        if data in NONES:
            return data
        else:
            if data.startswith('[') and data.endswith(']'):
                return data
            else:
                return f'[{data}]'

    @property
    def eval_data(self):
        data = self.noneval_data
        if data in NONES:
            return data
        else:
            return eval(data)

    def __getattribute__(self, name):
        #dirty trick to get evaluated data
        if name == 'data':
            return self.eval_data
        else:
            #return getattr(self, name)
            return super().__getattribute__(name)

class OrListField(ListField):

    @property
    def noneval_data(self):
        data = super().noneval_data
        if data in NONES:
            return data
        elif ',' not in data:
            #remove brackets
            return data[1:-1]
        else:
            return data

class SetHiddenField(HiddenField):

    def __init__(*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.default = kwargs.get('default')

    def __getattribute__(self, name):
        if name == 'data':
            return self.default
        else:
            return super().__getattribute__(name)

class FormMixin:
    hidden_entries = None

    def populate_form(self, formatted_dict):
        """populate form from formatted_dict
        """
        for field in self:
            key = field.short_name

            if key in formatted_dict:
                data = formatted_dict[key]

                if isinstance(field, SetFieldList):
                    if data is None:
                        for subfield in field:
                            subfield.process_data(data)

                    else:
                        assert len(data) == len(field)
                        for idata, subfield in zip(data, field):
                            subfield.process_data(idata)

                elif data is None:
                    continue

                elif isinstance(field, FieldList):
                    while len(field) > 1:
                        field.pop_entry()

                    subfield = field[0]

                    if isinstance(subfield, FormField):
                        for idata in data:
                            sanitized_data = \
                                subfield.populate_form(idata).data
                            field.append_entry(sanitized_data)

                    else:
                        for idata in data:
                            field.append_entry(idata)

                elif isinstance(field, FormField):
                    field.populate_form(data)

                else:
                    field.process_data(data)

        return self

    def rm_hidden_entries(self):
        """removes hidden entries in form to allow for dynamic lists
        """

        self.hidden_entries = {}
        self.subhidden_entries = []

        for field in self:

            if isinstance(field, FieldList) \
                and not isinstance(field, SetFieldList):

                #print(field.id)
                #print(field.data)

                hidden_entry = field.entries.pop(0)
                self.hidden_entries[field] = hidden_entry

                for subfield in field:
                    #check if subfield is formfield and pop entry
                    if isinstance(subfield, FormField):
                        #print(subfield.id)
                        subfield.rm_hidden_entries()
                        self.subhidden_entries.append(subfield)

    def append_hidden_entries(self):
        """reinsert hidden entries in form to allow for dynamiv lists
        """

        if self.hidden_entries is None:
            pass

        else:
            for subfield in self.subhidden_entries:
                subfield.append_hidden_entries()

            for field, hidden_entry in self.hidden_entries.items():

                field.entries.insert(0, hidden_entry)

            self.hidden_entries = None

    @staticmethod
    def get_field_data(field):

        def _get_field_data(field, nan_return):
            if field.data in NONES:
                return nan_return
            elif field.data is np.nan:
                return nan_return
            else:
                return field.data

        if isinstance(field, BooleanField):
            return _get_field_data(field, False)
        else:
            return _get_field_data(field, None)

    def get_formatted(self):
        """get data from form and reformat for
        execution and saving
        """
        formatted_dict = {}
        name = None # if name stays None only return dict
        for field in self:

            key = field.short_name

            if key == 'csrf_token':
                continue

            elif key == 'name':
                name = field.data

            elif isinstance(field, FieldList):
                formatted_dict[key] = []

                for subfield in field:
                    if isinstance(subfield, FormField):
                        formatted_dict[key].append(subfield.get_formatted())

                    else:
                        formatted_dict[key].append(
                            self.get_field_data(subfield))

                # No Nones are allowed in a list, they will be removed
                # or the key will be set to None
                are_None = [value is None for value in formatted_dict[key]]

                if np.any(are_None) and isinstance(field, SetFieldList):
                    formatted_dict[key] = None

                elif np.all(are_None):
                    formatted_dict[key] = None

                else:
                    formatted_dict[key] = [
                        value
                        for value in formatted_dict[key]
                        if value is not None]

            elif isinstance(field, FormField):
                formatted_dict[key] = field.get_formatted()

            else:
                formatted_dict[key] = self.get_field_data(field)

        if name is None:
            return formatted_dict

        else:
            return formatted_dict, name


class RecordingForm(Form, FormMixin):
    subject_name = StringField(
        'Subject name', validators=[InputRequired()])
    recording_id = IntegerField(
        'Recording ID', default=1, validators=[InputRequired()])
    subrecording_id = IntegerField(
        'Subrecording ID', validators=[Optional()]
    )
    experimenter_name = StringField(
        'Experimenter Name', default='Cthulu',
        validators=[InputRequired()])

class BehaviorForm(Form, FormMixin):
    subject_name = StringField(
        'Subject name', validators=[InputRequired()])
    recording_id = IntegerField(
        'Recording ID', default=1, validators=[InputRequired()])
    subrecording_id = IntegerField(
        'Subrecording ID', validators=[Optional()]
    )
    experimenter_name = StringField(
        'Experimenter Name', default='Cthulu',
        validators=[InputRequired()])

def ao_mapping_form_dynamic(app_config):

    class AoMappingForm(NoCsrfForm, FormMixin):
        mapping_name = StringField(
            'analog output name', validators=[InputRequired()])
        if app_config['coresettings']['has_ao']:
            system = nidaqmx.system.System.local()
            ao_chan_names = [
                (chan.name, chan.name)
                for device in system.devices
                for chan in device.ao_physical_chans
            ]
            device = SelectField(
                'device analog output',
                choices=ao_chan_names
            )
        if app_config['coresettings']['has_arduino']:
            ports = list(serial.tools.list_ports.comports())
            arduino_names = [
                    (p.device, p.device)
                    for p in ports
                    ]
            device = SelectField(
                'device analog output',
                choices=arduino_names
            )
        else:
            device = StringField(
                'device analog output', default='',
                validators=[InputRequired()]
            )
            # output = StringField(
            #     'output channel', default='ao0',
            #     validators=[InputRequired()]
            # )
        if app_config['coresettings']['has_arduino']:
            arduino_pin = FloatField(
                    'pin connected to the LED', default="",
                    validators=[Optional()])
        vmin = FloatField(
            'minimum allowed voltage', default=0,
            validators=[InputRequired()]
        )
        vmax = FloatField(
            'maximum allowed voltage', default=5,
            validators=[InputRequired()]
        )
        zero_intensity = FloatField(
            'voltage of zero intensity', default=0,
            validators=[InputRequired()]
        )
        measurement_file = SelectField(
            'measurement file',
            choices=[
                (
                    os.path.split(filename)[-1],
                    os.path.split(filename)[-1]
                )
                for filename in np.sort(glob(
                    os.path.join(app_config['measurementsdir'], '*')
                ))
                if not filename.endswith(
                    ('.cal', '.IRRADCAL', '.RADCAL', '.csv', '.npy'))
            ]+[('', 'unity mapping')],
        )
        measurement_unit = SelectField(
            'unit of measurement',
            choices=[
                ('', ''),
                ('W/m2', 'W/m^2'),
                ('nW/mm2', 'nW/mm^2'),
                ('uW/cm2', 'uW/cm^2'),
                ('E', 'E'),
                ('mE', 'mE'),
                ('uE', 'uE'),
                ('nE', 'nE'),
            ]
        )
        polyfit_degree = IntegerField(
            'degrees for polyfit',
            default=0,
            validators=[InputRequired()]
        )
        preinterp_method = SelectField(
            'preinterpolation method',
            choices=[
                ('monotonic', 'monotonic'),
                ('spline', 'spline'),
                ('', '')
            ],
            default='monotonic'
        )
        measurementsdir = SelectField(
            'measurements directory',
            choices=[(app_config['measurementsdir'], 'standard')],
        )
        # load_function?

    return AoMappingForm


class BlankForm(Form, FormMixin):
    pass


def core_form_dynamic(*args, app_config=None, **kwargs):

    class CoreForm(Form, FormMixin):
        name = StringField('core name', validators=[InputRequired()])
        trigger_frequency = FloatField(
            'trigger frequency (Hz)', default=0, validators=[InputRequired()])
        if NI:
            system = nidaqmx.system.System.local()
            ao_chan_names = [
                (chan.name, chan.name)
                for device in system.devices
                for chan in device.ao_physical_chans
            ]
            trigger_device = SelectField(
                'NI trigger device',
                choices=[('','')]+ao_chan_names
            )
        else:
            trigger_device = StringField(
                'NI trigger device', default='',
                validators=[Optional()]
            )
        has_u3 = BooleanField('Labjack', default=False) #add NI analog output
        has_projectors = BooleanField('Projectors', default=False)
        has_ao = BooleanField('LED analog output', default=False)
        has_arduino = BooleanField('Arduino output', default=False)
        single_tasks = BooleanField(
            'Group analog outputs into devices', default=True)

    return CoreForm(*args, **kwargs)


def ao_form_dynamic(*args, app_config=None, **kwargs):

    class AoForm(Form, FormMixin):
        name = StringField('name',
                           validators=[InputRequired()])
        opsin_sensitivities = SelectField(
            'opsin sensitivities',
            choices=[
                (
                    filename,
                    os.path.split(filename)[-1]
                )
                for filename in np.sort(glob(
                    os.path.join(app_config['datadir'], '*.csv')
                ))
                if 'opsin' in filename
            ]+[('', 'NULL')],
        )
        ao_params = FieldList(
            FormField(ao_mapping_form_dynamic(app_config)),
            min_entries=2)

    if app_config['coresettings'] is None:
        return BlankForm(*args, **kwargs)

    elif app_config['coresettings']['has_ao'] or app_config['coresettings']['has_arduino']:
        return AoForm(*args, **kwargs)

    else:
        return BlankForm(*args, **kwargs)


def projector_form_dynamic(*args, app_config=None, **kwargs):


    class ProjectorForm(Form, FormMixin):
        name = StringField('projector name', validators=[InputRequired()])
        rgb_projector = IntegerField(
            'screen number for RGB projector',
            validators=[Optional()])
        uv_projector = IntegerField(
            'screen number for UV projector', validators=[Optional()])
        position = SetFieldList(
            IntegerField(
                'projector position (pix)',
                default=0, validators=[InputRequired()]))
        alignment_shift = SetFieldList(
            FloatField('alignment shift', validators=[Optional()]))
        framepack = BooleanField(
            'framepacking', default=True)
        fullscreen = BooleanField(
            'fullscreen', default=True)
        scale = SetFieldList(
            FloatField('scale', default=1, validators=[InputRequired()]))
        offset = SetFieldList(
            IntegerField('offset (pix)',
                         default=0, validators=[InputRequired()]))

    if app_config['coresettings'] is None:
        return BlankForm(*args, **kwargs)

    elif app_config['coresettings']['has_projectors']:
        return ProjectorForm(*args, **kwargs)

    else:
        return BlankForm(*args, **kwargs)


def measurement_form_dynamic(*args, app_config=None, **kwargs):

    class MeasurementForm(Form, FormMixin):
        # name = StringField(
        #     'measurement name', validators=[InputRequired()])
        integration_time = FloatField(
            'integration time (s)', default=0.1,
            validators=[InputRequired(), NumberRange(0.001)]
        )
        max_integration_time = FloatField(
            'max integration time (s)', default=15,
            validators=[InputRequired(), NumberRange(0.01)]
        )
        min_integration_time = FloatField(
            'min integration time (s)', default=0.01,
            validators=[InputRequired(), NumberRange(0.001)]
        )
        number_of_steps = IntegerField(
            'number of measurement steps', default=100,
            validators=[InputRequired(), NumberRange(0)]
        )
        scale = SelectField(
            'step scaling',
            choices=[
                ('log', 'log'),
                ('linear', 'linear'),
                ('reverselog', 'reverselog')
            ]
        )
        wavelength_min = FloatField(
            'wavelength minimum', default=300,
            validators=[InputRequired(), NumberRange(200, 500)]
        )
        wavelength_max = FloatField(
            'wavelength maximum', default=700,
            validators=[InputRequired(), NumberRange(400, 800)]
        )
        baseline_interval = SetFieldList(
            FloatField(
                'min/max baseline calculation (nm)',
                validators=[Optional()]))
        return_value = FloatField(
            'voltage return value',
            validators=[Optional()]
        )
        correct_dark_counts = BooleanField(
            'correct dark counts', default=True
        )
        correct_nonlinearity = BooleanField(
            'correct nonlinearity', default=True
        )
        average = IntegerField(
            'average over', default=20,
            validators=[InputRequired()]
        )
        calibration_file = SelectField(
            'calibration file',
            choices=[
                (
                    filename,
                    os.path.split(filename)[-1]
                )
                for filename in np.sort(
                    np.concatenate([
                        glob(
                            os.path.join(
                                app_config['measurementsdir'],
                                ext)
                            )
                        for ext in ['*.cal', '*.IRRADCAL', '*.RADCAL']
                        ])
                    )
                ]+[('', '')],
        )

    return MeasurementForm(*args, **kwargs)

#TODO initial color intensity of projectors - LATER WHEN INTEGRATING
#TODO baseline_intensity of analog output
def background_form_dynamic(*args, app_config=None, **kwargs):

    #base class background form
    class BackgroundForm(Form, FormMixin):
        name = StringField(
            'background name',
            default='None', validators=[InputRequired()])

    if app_config['projectorsettings'] is not None:
        if app_config['projectorsettings'].get('rgb_projector', None) \
            is not None:
            setattr(
                BackgroundForm,
                "color_mode",
                SelectField(
                    'initial color mode for rgb projector',
                    choices = [
                        ('rgb', 'rgb'), ('red', 'red'),
                        ('green', 'green'), ('blue', 'blue'),
                        ],
                    validators=[InputRequired()]
                )
            )
            setattr(
                BackgroundForm,
                "rgb_intensity",
                FloatField(
                    'rgb projector intensity',
                    default=-1,
                    validators=[InputRequired(), NumberRange(-1, 1)]
                )
            )

        if app_config['projectorsettings'].get('uv_projector', None) \
            is not None:
            setattr(
                BackgroundForm,
                "uv_intensity",
                FloatField(
                    'uv projector intensity',
                    default=-1,
                    validators=[InputRequired(), NumberRange(-1, 1)]
                )
            )


    if app_config['aosettings'] is not None:

        for ao_param in app_config['aosettings'].get('ao_params', []):
            name = ao_param['mapping_name']
            setattr(
                BackgroundForm,
                name,
                FloatField(
                    f'{name} intensity (in measurement units)',
                    validators=[Optional()]
                )
            )

        BackgroundForm.background_spectrum = SelectField(
            'background spectrum',
            choices=[('', 'NULL')]+[
                (
                    filename,
                    os.path.split(filename)[-1]
                )
                for filename in np.sort(glob(os.path.join(app_config['datadir'], '*.csv')))
                if 'opsin' not in filename
            ]+[
                (
                    filename,
                    os.path.split(filename)[-1]
                )
                for filename in np.sort(glob(os.path.join(app_config['datadir'], '*.json')))
                if 'opsin' not in filename
            ]+[('', 'NULL')],
        )
        BackgroundForm.background_intensity = FloatField(
            'background intensity (measurement unit)',
            validators=[Optional(), NumberRange(0)]
        )

    return BackgroundForm(*args, *kwargs)


def monitor_form_dynamic(*args, app_config=None, **kwargs):

    class MonitorForm(Form, FormMixin):
        name = StringField('name for monitor', validators=[InputRequired()])
        resolution = SetFieldList(
            IntegerField('resolution (pix)', validators=[InputRequired()]))
        refresh_rate = IntegerField(
            'refresh rate (Hz)', default=60,
            validators=[InputRequired()])
        luminance = FloatField(
            'luminance (nW/mm2)', validators=[Optional(), NumberRange(0.01)])
        mon_width_cm = FloatField(
            'monitor width (cm)', default=15, validators=[InputRequired()])
        mon_height_cm = FloatField(
            'monitor height (cm)', validators=[InputRequired()])
        downsample_rate = IntegerField(
            'screen downsampling', validators=[InputRequired()], default=1)
        dis = FloatField(
            'screen distance to eye (cm)', validators=[InputRequired()])
        C2T_cm = FloatField(
            'gaze center to screen top (cm)',
            validators=[InputRequired()])
        C2A_cm = FloatField(
            'gaze center to screen anterior (cm)',
            validators=[InputRequired()])
        center_coordinates = SetFieldList(
            FloatField('alt/azi from eye to monitor (degrees)',
                       validators=[InputRequired()]))
        visual_field = SelectField(
            'visual field', choices=[('left', 'left'), ('right', 'right')],
            validators=[InputRequired()])

    if app_config['coresettings'] is None:
        return MonitorForm(*args, **kwargs)

    elif app_config['coresettings']['has_projectors']:
        return MonitorForm(*args, **kwargs)

    else:
        del(MonitorForm.resolution)
        del(MonitorForm.luminance)
        del(MonitorForm.downsample_rate)
        del(MonitorForm.mon_width_cm)
        del(MonitorForm.mon_height_cm)
        # del(MonitorForm.C2T_cm)
        # del(MonitorForm.C2A_cm)
        return MonitorForm(*args, **kwargs)


def indicator_form_dynamic(*args, app_config=None, **kwargs):

    class IndicatorForm(Form, FormMixin):
        name = StringField(
            'indicator name', default='default',
            validators=[InputRequired()])
        width_cm = FloatField(
            'indicator width (cm)', default=1,
            validators=[InputRequired()])
        height_cm = FloatField(
            'indicator height (cm)', default=1,
            validators=[InputRequired()])
        position = SelectField(
            'indicator position', validators=[InputRequired()],
            choices=[
                ('southwest', 'southwest'), ('southeast', 'southeast'),
                ('northwest', 'northwest'), ('northeast', 'northeast')])
        is_sync = BooleanField('is synced with stimuli', default=True)
        freq = FloatField(
            'photodiode frequency (Hz)',
            default=2, validators=[InputRequired()])

    if app_config['coresettings'] is None:
        return BlankForm(*args, **kwargs)

    elif app_config['coresettings']['has_projectors']:
        return IndicatorForm(*args, **kwargs)

    else:
        return BlankForm(*args, **kwargs)


class StimulusForm(NoCsrfForm, FormMixin):
    idx = IntegerField(
        'queue number',
        validators=[InputRequired(), NumberRange(1)], default=1)
    seed = IntegerField(
        'seed',
        validators=[Optional()]
    )
    background = FloatField(
        'background', validators=[InputRequired(), NumberRange(-1, 1)])
    color_mode = SelectField(
        'color',
        choices = [
            ('rgb', 'rgb'), ('red', 'red'),
            ('green', 'green'), ('blue', 'blue'),
            ('uv', 'uv')],
        validators=[InputRequired()])
    coordinate = SelectField(
        'coordinate system', validators=[InputRequired()],
        choices=[('linear', 'linear'), ('degree', 'degree')],
        default='degree')
    pregap_dur = FloatField(
        'before duration (s)', validators=[InputRequired(), NumberRange(0)],
        default=10)
    postgap_dur = FloatField(
        'after duration (s)', validators=[InputRequired(), NumberRange(0)],
        default=10)

def ff_input_field_dynamic(
    name, ao_params, field, *args, fieldlist=None, **kwargs):

    class InputForm(NoCsrfForm, FormMixin):
        pass

    for ao_param in ao_params:
        if fieldlist is None:
            setattr(
                InputForm,
                ao_param['mapping_name'],
                field(
                    f"{ao_param['mapping_name']} {name}",
                    *args, **kwargs)
            )
        else:
            raise NotImplementedError('fieldlist for ff stimulus')
            # setattr(
            #     InputForm,
            #     ao_param['mapping_name'],
            #     fieldlist(
            #         field(ao_param['mapping_name'], *args, **kwargs),
            #         min_entries=1
            #     )
            # )

    return FormField(InputForm)


def ff_stimulus_form_dynamic(ao_params, datadir, has_ao=True):
    class FFStimulusForm(NoCsrfForm, FormMixin):
        idx = IntegerField(
            'queue number',
            validators=[InputRequired(), NumberRange(1)], default=1)
        background = ff_input_field_dynamic(
            'background', ao_params, FloatField,
            validators=[Optional()]
        )
        background_spectrum = SelectField(
            'background spectrum',
            choices=[('', 'NULL')]+[
                (
                    filename,
                    os.path.split(filename)[-1]
                )
                for filename in np.sort(glob(os.path.join(datadir, '*.csv')))
                if 'opsin' not in filename
            ]+[
                (
                    filename,
                    os.path.split(filename)[-1]
                )
                for filename in np.sort(glob(os.path.join(datadir, '*.json')))
                if 'opsin' not in filename
            ]+[('', 'NULL')],
        )
        background_intensity = FloatField(
            'background intensity (measurement unit)',
            validators=[Optional(), NumberRange(0)]
        )

        if has_ao:
            pregap_dur = FloatField(
                'before duration (s)',
                validators=[InputRequired(), NumberRange(0)],
                default=10)
            postgap_dur = FloatField(
                'after duration (s)',
                validators=[InputRequired(), NumberRange(0)],
                default=10)
            iteration = IntegerField(
                'iterations', default=1,
                validators=[InputRequired(), NumberRange(0)]
            )

    return FFStimulusForm


def steps_form_dynamic(app_config):
    """
    """

    ao_params = app_config['aosettings']['ao_params']
    datadir = app_config['datadir']
    has_ao = app_config['coresettings']['has_ao']
    StepsForm = ff_stimulus_form_dynamic(ao_params, datadir,has_ao)

    StepsForm.values_dict = ff_input_field_dynamic(
        'intensities', ao_params, ListField,
        validators=[Optional(), EvalValidator(list, (float, int))]
    )
    if not has_ao:
        StepsForm.opposing_dict = ff_input_field_dynamic(
            'opposing LED', ao_params, ListField,
            validators=[Optional(), EvalValidator(list, (float, int))]
        )

    else: # in case of the behavior this is not necessary
        StepsForm.durations = ListField(
            'duration (s)',
            validators=[InputRequired(), EvalValidator(list, (float, int))]
        )
        StepsForm.pause_durations = ListField(
            'pause duration (s)',
            validators=[InputRequired(), EvalValidator(list, (float, int))]
        )
        StepsForm.repeat = IntegerField(
            'repeats', default=1,
            validators=[InputRequired(), NumberRange(1)]
        )

    StepsForm.randomize = BooleanField(
        'randomize', default=True
    )
    StepsForm.mixing = BooleanField(
        'mix values', default=False
    )

    return StepsForm

def spectra_steps_form_dynamic(app_config):
    """
    """

    ao_params = app_config['aosettings']['ao_params']
    datadir = app_config['datadir']
    has_ao = app_config['coresettings']['has_ao']
    StepsForm = ff_stimulus_form_dynamic(ao_params, datadir,has_ao)

    StepsForm.single_wavelength_range = ListField(
        'min, max OR values of single wavelength (nm)',
        validators=[
            InputRequired(),
            EvalValidator(
                list, (float, int),
                lambda x: (len(x) > 1) and np.all(np.asarray(x) >= 0))
            ]
        # FloatField(
        #     'min/max single wavelength (nm)',
        #     validators=[InputRequired(), NumberRange(0)])
    )
    StepsForm.number_single_wavelength = IntegerField(
        'number of single wavelengths',
        validators=[InputRequired(), NumberRange(1)]
    )
    StepsForm.single_wavelength_sigma = FloatField(
        'standard deviation of single wavelengths (nm)',
        validators=[InputRequired(), NumberRange(0)]
    )
    StepsForm.single_wavelength_method = SelectField(
        'distribution type',
        choices=[
            ('pdf', 'pdf'),
            ('cdf', 'cdf'),
            ('inverse_cdf', 'inverse_cdf'),
            ('cdf+inverse_cdf', 'cdf+inverse_cdf')
            ]
    )
    StepsForm.wavelength_combinations = OrListField(
        'number or set of wavelength mixings',
        validators=[Optional(), EvalValidator((list, int), (tuple,list))]
    )
    # IntegerField(
    #     'number of wavelength mixings',
    #     default=3,
    #     validators=[InputRequired(), NumberRange(0)]
    # )
    StepsForm.combination_steps = IntegerField(
        'number of combinations per wavelength mixing',
        default=5,
        validators=[InputRequired(), NumberRange(0)]
    )
    StepsForm.combinations_log_distance = BooleanField(
        'use log to calculate wavelength distances',
        default=False
    )
    StepsForm.log_fractions = BooleanField(
        'use log fractions for mixing wavelengths',
        default=False
    )
    StepsForm.equiluminance_plane = SelectField(
        'calculation of equiluminance plane',
        choices=[
            ('linear', 'linear'),
            ('isomeasure', 'isomeasure'),
            ('log', 'log')
            ]
    )
    StepsForm.equiluminance_plane_discard = ListField(
        'discard opsins for equiluminance calculation',
        validators=[Optional(), EvalValidator(list, (str))]
    )
    StepsForm.luminant_multiples = ListField(
        'luminant multiples',
        validators=[Optional(), EvalValidator(list, (float, int))]
    )
    StepsForm.mix_method = SelectField(
        'method for mixing with background',
        choices=[
            ('constant', 'constant'),
            ('addition', 'addition'),
            ('filterconstant', 'filterconstant'),
            ('filteraddition', 'filteraddition'),
            ('both', 'both')],
        default='constant'
    )
    StepsForm.use_minimizer = SelectField(
        'minimization method',
        choices=[
            ('', 'linear'),
            ('log', 'log'),
            ('log_disperse', 'log_disperse'),
            ('disperse', 'disperse')
        ],
        default=''
    )
    StepsForm.cholesky_whitening = BooleanField(
        'whiten opsins',
        default=False
    )
    StepsForm.opsin_weights = ListField(
        'opsin weights [rh1, rh3, rh4, rh5, rh6]',
        validators=[Optional(), EvalValidator(list, (float, int))]
    )
    StepsForm.durations = ListField(
        'duration (s)',
        validators=[InputRequired(), EvalValidator(list, (float, int))]
    )
    StepsForm.pause_durations = ListField(
        'pause duration (s)',
        validators=[InputRequired(), EvalValidator(list, (float, int))]
    )
    StepsForm.repeat = IntegerField(
        'repeats', default=1,
        validators=[InputRequired(), NumberRange(1)]
    )
    StepsForm.randomize = BooleanField(
        'randomize', default=True
    )
    return StepsForm

def spectra_test_steps_form_dynamic(app_config):
    """
    """

    ao_params = app_config['aosettings']['ao_params']
    datadir = app_config['datadir']
    has_ao = app_config['coresettings']['has_ao']
    StepsForm = ff_stimulus_form_dynamic(ao_params, datadir,has_ao)

    StepsForm.led_to_test = SelectField(
        'led to test',
        choices=[
            (ao_param['mapping_name'], ao_param['mapping_name'])
            for ao_param in ao_params
        ]
    )
    StepsForm.luminant_multiples = ListField(
        'luminant multiples',
        validators=[Optional(), EvalValidator(list, (float, int))]
    )
    StepsForm.mix_method = SelectField(
        'method for mixing with background',
        choices=[
            ('constant', 'constant'),
            ('addition', 'addition'),
            ('both', 'both')],
        default='constant'
    )
    StepsForm.use_minimizer = SelectField(
        'minimization method',
        choices=[
            ('', 'linear'),
            ('log', 'log'),
            ('log_disperse', 'log_disperse'),
            ('disperse', 'disperse')
        ],
        default=''
    )
    StepsForm.cholesky_whitening = BooleanField(
        'whiten opsins',
        default=False
    )
    StepsForm.opsin_weights = ListField(
        'opsin weights [rh1, rh3, rh4, rh5, rh6]',
        validators=[Optional(), EvalValidator(list, (float, int))]
    )
    StepsForm.durations = ListField(
        'duration (s)',
        validators=[InputRequired(), EvalValidator(list, (float, int))]
    )
    StepsForm.pause_durations = ListField(
        'pause duration (s)',
        validators=[InputRequired(), EvalValidator(list, (float, int))]
    )
    StepsForm.repeat = IntegerField(
        'repeats', default=1,
        validators=[InputRequired(), NumberRange(1)]
    )
    StepsForm.randomize = BooleanField(
        'randomize', default=True
    )
    return StepsForm


class UniformContrastForm(StimulusForm):
    color = FloatField(
        'intensity', validators=[InputRequired(), NumberRange(-1, 1)])
    duration = FloatField(
        'duration (s)', validators=[InputRequired(), NumberRange(0)])


class FlashingCircleForm(StimulusForm):
    #TODO smooth_func parameter
    center = SetFieldList(
        FloatField('center (alt/azi)', validators=[InputRequired()])
    )
    radius = FloatField('radius', validators=[InputRequired(), NumberRange(0)])
    is_smooth_edge = BooleanField('smooth edge', default=False)
    smooth_width_ratio = FloatField(
        'ratio between smooth band width and radius',
        default=0.2, validators=[InputRequired(), NumberRange(0, 1)]
    )
    flash_frame_num = IntegerField(
        'number of frames', validators=[InputRequired(), NumberRange(0)]
    )
    midgap_dur = FloatField(
        'gap between flashes (s)', default=1,
        validators=[InputRequired(), NumberRange(0)]
    )
    iteration = IntegerField(
        'iterations',
        default=1, validators=[InputRequired(), NumberRange(0)]
    )


class DriftingGratingCircleForm(StimulusForm):
    #TODO smooth_func parameter
    center = SetFieldList(
        FloatField('center (alt/azi)', validators=[InputRequired()])
    )
    sf_list = ListField(
        'spatial frequencies (cycles/unit)',
        validators=[InputRequired(), EvalValidator(list, (float, int))]
    )
    tf_list = ListField(
        'temporal frequencies (Hz)',
        validators=[InputRequired(), EvalValidator(list, (float, int))]
    )
    dire_list = ListField(
        'directions (degrees)',
        validators=[InputRequired(), EvalValidator(list, (float, int))]
    )
    con_list = ListField(
        'contrasts',
        validators=[InputRequired(), EvalValidator(list, (float, int))]
    )
    radius_list = ListField(
        'radii',
        validators=[InputRequired(), EvalValidator(list, (float, int))]
    )
    block_dur = FloatField(
        'block duration (s)', validators=[InputRequired(), NumberRange(0)])
    is_blank_block = BooleanField('blank block', default=True)
    is_smooth_edge = BooleanField('smooth edge', default=False)
    smooth_width_ratio = FloatField(
        'ratio between smooth band width and radius',
        default=0.2, validators=[InputRequired(), NumberRange(0, 1)]
    )
    midgap_dur = FloatField(
        'gap between flashes (s)', default=1,
        validators=[InputRequired(), NumberRange(0)]
    )
    iteration = IntegerField(
        'iterations',
        default=1, validators=[InputRequired(), NumberRange(0)]
    )


class SparseNoiseForm(StimulusForm):
    grid_space = SetFieldList(
        FloatField('grid space (alt/azi)', validators=[InputRequired()]),
    )
    probe_size = SetFieldList(
        FloatField('probe size (width/height)', validators=[InputRequired()]),
    )
    probe_orientation = FloatField(
        'probe orientation (degrees)', validators=[InputRequired()],
        default=0
    )
    probe_frame_num = IntegerField(
        'probe frames number', validators=[InputRequired()],
        default=90
    )
    subregion = SetFieldList(
        FloatField(
            'subregion (minalt, maxalt, minazi, maxazi)',
            validators=[Optional()]),
        entries=4
    )
    sign = SelectField(
        'sign', choices=[('ON-OFF', 'ON-OFF'), ('ON', 'ON'), ('OFF', 'OFF')]
    )
    iteration = IntegerField(
        'iteration', default=1,
        validators=[InputRequired()]
    )
    is_include_edge = BooleanField(
        'include edge cases', default=True
    )
    minmax = SetFieldList(
        FloatField(
            'minimum and maximum',
            validators=[InputRequired(), NumberRange(-1, 1)]
        )
    )
    refresh_distance = FloatField(
        'refresh distance',
        validators=[Optional(), NumberRange(0)], default=0)
    pause_frame_num = IntegerField(
        'pause frame number',
        validators=[InputRequired(), NumberRange(0)], default=0
    )


class LocallySparseNoiseForm(SparseNoiseForm):
    min_distance = FloatField(
        'minimum distance',
        validators=[InputRequired(), NumberRange(0)], default=0)
    repeat = IntegerField(
        'repeat', validators=[InputRequired()], default=1)

class WhiteNoiseForm(SparseNoiseForm):
    noise_duration = FloatField(
        'duration of each iteration (sec)',
        validators=[InputRequired(), NumberRange(0)],
        default=10
    )


def stimuli_form_dynamic(*args, app_config=None, **kwargs):

    class StimuliForm(Form, FormMixin):
        stimuli_types = list(STIMULITYPES.keys())
        name = StringField(
            'stimulus set name', validators=[InputRequired()])

    if app_config['coresettings'] is None:
        return BlankForm(*args, **kwargs)

    if app_config['coresettings']['has_projectors']:
        # Add projector stimuli here
        StimuliForm.is_by_index = BooleanField(
            'select frames by index', default=True)
        StimuliForm.is_interpolate = BooleanField(
            'interpolate generated images', default=True)
        StimuliForm.sparse = FieldList(FormField(SparseNoiseForm), min_entries=1)
        StimuliForm.locallysparse = FieldList(
            FormField(LocallySparseNoiseForm), min_entries=1)
        StimuliForm.whitenoise = FieldList(
            FormField(WhiteNoiseForm), min_entries=1)
        StimuliForm.uniform = FieldList(FormField(UniformContrastForm), min_entries=1)
        StimuliForm.flashingcircle = FieldList(
            FormField(FlashingCircleForm), min_entries=1)
        StimuliForm.driftinggrating = FieldList(
            FormField(DriftingGratingCircleForm), min_entries=1)

    if app_config['coresettings'].get('has_ao', False) \
        or app_config['coresettings'].get('has_arduino', False):
        #only works if it has been set already
        if app_config['aosettings'].get('ao_params', None) is None:
            pass
        #Add full field stimuli dynamically
        else:
            StimuliForm.ff_steps = FieldList(
                FormField(steps_form_dynamic(app_config)),
                min_entries=1
            )
            StimuliForm.ff_spectra_steps = FieldList(
                FormField(spectra_steps_form_dynamic(app_config)),
                min_entries=1
            )
            StimuliForm.ff_spectra_test_steps = FieldList(
                FormField(spectra_test_steps_form_dynamic(app_config)),
                min_entries=1
            )

    return StimuliForm(*args, **kwargs)
