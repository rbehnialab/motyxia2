#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun  2 22:48:43 2019

@author: samimollard
"""

import matplotlib.pyplot as plt
import seabreeze.spectrometers as sb
import serial
import numpy as np
import time
from motyxia.utils import read_json
from motyxia.fullfield import devices

plt.style.use('dark_background')


def SendArduino(value,arduino_pin,ser):

    """
    Method to send a pwm value to a given LED of the Arduino
    
    Args:
        value (int): pwm value of the LED
        arduino_pin (int): pin the LED is connected to
        ser: the serial object (here the Arduino)
        
    Returns: Nothing, it just send the appropriate data to the arduino
    
    We are going to use the same method as the author of MARGO:
    we convert the pwm value into binary (12 bits), then split it
    into two bytes, then convert them to decimal and send them to the 
    arduino as bytes
    
    """
    value = np.uint32(value)
    byte1 = value >> 8 & 0b1111 #Getting the first four left bits of value
    byte2 = value & 0b11111111 #Getting the 8 right bits of value 
    board = int(arduino_pin//24) #Pin 11 is on board 0, pin 25 is on board 1 and so on
    LED = int(arduino_pin%24) #Pin 25 is LED 1 of board 2, pin 18 is LED 18 of board 1 and so on
    ser.write(bytearray([byte1,byte2,board,LED]))
    

integration_time = 0.001
average = 1000

ser = serial.Serial("COM5",115200)
time.sleep(1)
devicess = sb.list_devices()
spec = sb.Spectrometer(devicess[0])
spec.integration_time_micros(integration_time*10**6)
wls = spec.wavelengths()
    
colors = ['xkcd:indigo','xkcd:violet','xkcd:azure','xkcd:green','xkcd:goldenrod','xkcd:tomato']
labels = ['UV','Purple','Blue','Green','Yellow','Red']
    
#Doing the measurments one by one
intensities = []
for i in range(6):
    SendArduino(4000,i,ser)
    time.sleep(2)
    average_intensity = np.zeros(wls.shape)
    for j in range(average):
        average_intensity += spec.intensities(correct_dark_counts=True, correct_nonlinearity=True)
        time.sleep(integration_time)
    intensities.append(average_intensity/average)
    time.sleep(2)
    SendArduino(0,i,ser)
    time.sleep(2)
    
figure,ax = plt.subplots()
for i in range(6):
    ax.plot(wls,intensities[i],label = labels[i],color = colors[i])

ax.set_ylabel('Intensities (photon counts)')
ax.set_title('Intensities by wavelength,one LED at a time')
ax.set_xlabel('Wavelength')
ax.set_xlim(300,700)
ax.set_ylim(0,16500)
ax.legend()
plt.savefig('measurment_one.png',dpi=300)

    
#doing the measurments with all LED on
for i in range(6):
    SendArduino(4000,i,ser)
    time.sleep(2)

intensities = np.zeros(wls.shape)
for i in range(average):
    intensities += spec.intensities(correct_dark_counts=True, correct_nonlinearity=True)
    time.sleep(integration_time)
intensities /= average

plt.plot(wls,intensities,color = 'w')
plt.ylabel('Intensities (photon counts)')
plt.title('Intensities by wavelength,every LED at the same time')
plt.xlabel('Wavelength')
plt.xlim(300,700)
plt.ylim(0,16500)
plt.savefig('measurment_all.png',dpi=300)

for i in range(6):
    SendArduino(0,i,ser)
    
 #OR
config = read_json('temp_config.json')
sb_device = sb.list_devices()[0]
 
for ao_param in config['aosettings']['ao_params']:
        ao_param['measurement_file'] = None


if 'arduino_pin' in config['aosettings']['ao_params'][0]: #The user checked "arduino" in the core configuration so we have an arduino

    arduino_devices = devices.ArduinoDevices(
        *config['aosettings']['ao_params']
    )

    for arduino_mapping in arduino_devices.arduino_mappings.values():

        arduino_mapping.measure(sb_device, **config['measurementsettings'])   
        
figure,ax = plt.subplots()
for i in range(6):
    ax.plot( list(arduino_devices.arduino_mappings.values())[i].wavelengths, list(arduino_devices.arduino_mappings.values())[i].photonfluxes[0],label = labels[i],color = colors[i])

ax.set_ylabel('Intensities (W/m2)')
ax.set_title('Intensities by wavelength,one LED at a time')
ax.set_xlabel('Wavelength')
ax.legend()
plt.savefig('measurment_one_calibrated.png',dpi=300)    