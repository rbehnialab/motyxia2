import numpy as np
import matplotlib.pyplot as plt

def find_all_paths(graph, start, end, path=[], method=None):
    """Given a node dictionary return all the paths
    from start to end
    """
    path = path + [start]
    if start == end:
        return [path]
    if start not in graph:
        return []
    paths = []

    for node in graph[start]:

        if node not in path:
            newpaths = find_all_paths(graph, node, end, path, method)

            for newpath in newpaths:

                if method is None:
                    paths.append(newpath)

                elif 'same' in method:
                    if len(newpath) == len(graph.keys()):
                        paths.append(newpath)
                    if method == 'sameonce':
                        return paths

                else:
                    raise NameError(f'method {method}.')

    return paths

def find_hamiltonian_graphs(graph, start, end, method='sameonce'):
    """Find Hamiltonian paths
    """
    assert start != end, 'start and end cannot be the same'

    keys = set(graph.keys())
    hamiltonians = []

    paths = find_all_paths(graph, start, end, method=method)
    for index, path in enumerate(paths):

        assert len(path) == len(set(path)), 'bugs'

        if set(path) == keys:
            hamiltonians.append(path)

    return hamiltonians

def create_graph_from_adjacency(adj):
    """create graph dictionary from adjacency matrix
    """
    graph = {}
    for index, row in enumerate(adj):
        graph[index] = np.where(row)[0].tolist()
    return graph

def in_hull(p, hull):
    """
    Determine if points in `p` are in `hull`

    `p` should be a `NxK` coordinate matrix of `N` points in `K` dimensions
    `hull` is either a scipy.spatial.Delaunay object or the `MxK` array of the
    coordinates of `M` points in `K`dimensions for which Delaunay triangulation
    will be computed

    Parameters
    ----------
    p : array
        NxK coordinate matrix of N points in K dimensions
    hull :
        either a scipy.spatial.Delaunay object or the `MxK` array of the
        coordinates of `M` points in `K`dimensions for which Delaunay
        triangulation will be computed

    Returns
    -------
    is_in_hull : ndarray of int
        Indices of simplices containing each point. Points outside the
        triangulation get the value -1.
    """
    from scipy.spatial import Delaunay
    if not isinstance(hull, Delaunay):
        hull = Delaunay(hull)

    return hull.find_simplex(p) >= 0


def get_warped_probes(deg_coord_alt, deg_coord_azi, probes, width,
                      height, ori=0., background_color=0.):
    """
    Generate a frame (matrix) with multiple probes defined by 'porbes', `width`,
    `height` and orientation in degrees. visual degree coordinate of each pixel is
    defined by deg_coord_azi, and deg_coord_alt

    Parameters
    ----------
    deg_coord_alt : ndarray
        2d array of warped altitude coordinates of monitor pixels
    deg_coord_azi : ndarray
        2d array of warped azimuth coordinates of monitor pixels
    probes : tuple or list
        each element of probes represents a single probe (center_alt, center_azi, sign)
    width : float
         width of the square in degrees
    height : float
         height of the square in degrees
    ori : float
        angle in degree, should be [0., 180.]
    foreground_color : float, optional
         color of the noise pixels, takes values in [-1,1] and defaults to `1.`
    background_color : float, optional
         color of the background behind the noise pixels, takes values in
         [-1,1] and defaults to `0.`
    Returns
    -------
    frame : ndarray
         the warped s
    """

    dis_bool = get_warped_gridlocs(
    	deg_coord_alt, deg_coord_azi, probes, width,
        height, ori)

    return get_warped_probes_with_dis_bool(
        dis_bool, probes, background_color)


def get_warped_gridlocs(
	deg_coord_alt, deg_coord_azi, grid_locs, width,
    height, ori=0.):
    """
    Return boolean for changing intensities of warpped frame
    """

    grid_locs = np.asarray(grid_locs)

    ori_arc = (ori % 360.) * 2 * np.pi / 360.

    #distance each pixel is from center in width and height dimension
    dis_width = np.abs(
        np.cos(ori_arc)
        * (deg_coord_azi[..., None] - grid_locs[:,1][None, None, :]) +
        np.sin(ori_arc)
        * (deg_coord_alt[..., None] - grid_locs[:,0][None, None, :]))

    dis_height = np.abs(
        np.cos(ori_arc + np.pi / 2)
        * (deg_coord_azi[..., None] - grid_locs[:,1][None, None, :]) +
        np.sin(ori_arc + np.pi / 2)
        * (deg_coord_alt[..., None] - grid_locs[:,0][None, None, :]))

    dis_bool = np.logical_and(
        dis_width <= width / 2.,
        dis_height <= height / 2.)

    return dis_bool


def get_warped_probes_with_dis_bool(dis_bool, probes, background_color):
    """warp multiple probes - each probe must have all locations
    """

    probes = np.asarray(probes)
    frame = dis_bool.astype(float) * probes[:, 2][None, None, :]

    dis_count = np.sum(dis_bool, axis=-1)
    if np.any(dis_count > 1):
        warnings.warn("overlapping probes in frame. performing averaging.")
        counts = dis_count.copy()
        counts[counts == 0] = 1
        frame = np.sum(frame, axis=-1)/counts

    else:
        frame = np.sum(frame, axis=-1)

    # add background color
    frame[~dis_count.astype(bool)] = background_color

    return frame.astype(np.float32)


def blur_cos(dis, sigma):
    """
    return a smoothed value [0., 1.] given the distance to center (with sign)
    and smooth width. this is using cosine curve to smooth edge

    parameters
    ----------
    dis : ndarray
        array that store the distance from the current pixel to blurred band center
    sigma : float
        definition of the width of blurred width, here is the length represent
        half cycle of the cosin function

    returns
    -------
    blurred : float
        blurred value
    """
    dis_f = dis.astype(np.float32)
    sigma_f = abs(float(sigma))

    blur_band = (np.cos((dis_f - (sigma_f / -2.)) * np.pi / sigma_f) + 1.) / 2.

    # plt.imshow(blur_band)
    # plt.show()

    blur_band[dis_f < (sigma_f / -2.)] = 1.
    blur_band[dis_f > (sigma_f / 2.)] = 0.

    # print blur_band.dtype

    return blur_band

def get_circle_mask(map_alt, map_azi, center, radius, is_smooth_edge=False,
                    blur_ratio=0.2, blur_func=blur_cos, is_plot=False):
    """
    Generate a binary mask of a circle with given `center` and `radius`

    The binary mask is generated on a map with coordinates for each pixel
    defined by `map_x` and `map_y`

    Parameters
    ----------
    map_alt  : ndarray
        altitude coordinates for each pixel on a map
    map_azi  : ndarray
        azimuth coordinates for each pixel on a map
    center : tuple
        coordinates (altitude, azimuth) of the center of the binary circle mask
    radius : float
        radius of the binary circle mask
    is_smooth_edge : bool
        if True, use 'blur_ratio' and 'blur_func' to smooth circle edge
    blur_ratio : float, option, default 0.2
        the ratio between blurred band width to radius, should be smaller than 1
        the middle of blurred band is the circle edge
    blur_func : function object to blur edge
    is_plot : bool

    Returns
    -------
    circle_mask : ndarray (dtype np.float32) with same shape as map_alt and map_azi
        if is_smooth_edge is True
            weighted circle mask, with smoothed edge
        if is_smooth_edge is False
            binary circle mask, takes values in [0.,1.]
    """

    if map_alt.shape != map_azi.shape:
        raise ValueError('map_alt and map_azi should have same shape.')

    if len(map_alt.shape) != 2:
        raise ValueError('map_alt and map_azi should be 2-d.')

    dis_mat = np.sqrt((map_alt - center[0]) ** 2 + (map_azi - center[1]) ** 2)
    # plt.imshow(dis_mat)
    # plt.show()

    if is_smooth_edge:
        sigma = radius * blur_ratio
        circle_mask = blur_func(dis=dis_mat - radius, sigma=sigma)
    else:
        circle_mask = np.zeros(map_alt.shape, dtype=np.float32)
        circle_mask[dis_mat <= radius] = 1.

    if is_plot:
        plt.imshow(circle_mask)
        plt.show()

    return circle_mask

def get_grating(alt_map, azi_map, dire=0., spatial_freq=0.1,
                center=(0., 60.), phase=0., contrast=1.):
    """
    Generate a grating frame with defined spatial frequency, center location,
    phase and contrast

    Parameters
    ----------
    azi_map : ndarray
        x coordinates for each pixel on a map
    alt_map : ndarray
        y coordinates for each pixel on a map
    dire : float, optional
        orientation angle of the grating in degrees, defaults to 0.
    spatial_freq : float, optional
        spatial frequency (cycle per unit), defaults to 0.1
    center : tuple, optional
        center coordinates of circle {alt, azi}
    phase : float, optional
        defaults to 0.
    contrast : float, optional
        defines contrast. takes values in [0., 1.], defaults to 1.

    Returns
    -------
    frame :
        a frame as floating point 2-d array with grating, value range [0., 1.]
    """

    if azi_map.shape != alt_map.shape:
        raise ValueError('map_alt and map_azi should have same shape.')

    if len(azi_map.shape) != 2:
        raise ValueError('map_alt and map_azi should be 2-d.')

    axis_arc = ((dire + 90.) * np.pi / 180.) % (2 * np.pi)

    map_azi_h = np.array(azi_map, dtype=np.float32)
    map_alt_h = np.array(alt_map, dtype=np.float32)

    distance = (np.sin(axis_arc) * (map_azi_h - center[1]) -
                np.cos(axis_arc) * (map_alt_h - center[0]))

    grating = np.sin(distance * 2 * np.pi * spatial_freq - phase)

    grating = grating * contrast  # adjust contrast

    grating = (grating + 1.) / 2.  # change the scale of grating to be [0., 1.]

    return grating

def get_grid_locations(subregion, grid_space, monitor_azi, monitor_alt, is_include_edge=True,
                       is_plot=False):
    """
    generate all the grid points in display area (covered by both subregion and
    monitor span), designed for SparseNoise and LocallySparseNoise stimuli.

    Parameters
    ----------
    subregion : list, tuple or np.array
        the region on the monitor that will display the sparse noise,
        [min_alt, max_alt, min_azi, max_azi], all floats
    grid_space : tuple or list of two floats
        grid size of probes to be displayed, [altitude, azimuth]
    monitor_azi : 2-d array
        array mapping monitor pixels to azimuth in visual space
    monitor_alt : 2-d array
        array mapping monitor pixels to altitude in visual space
    is_include_edge : bool, default True,
        if True, the displayed probes will cover the edge case and ensure that
        the entire subregion is covered.
        If False, the displayed probes will exclude edge case and ensure that all
        the centers of displayed probes are within the subregion.
    is_plot : bool

    Returns
    -------
    grid_locations : n x 2 array,
        refined [alt, azi] pairs of probe centers going to be displayed
    """
    if grid_space[0] >= np.abs(subregion[1] - subregion[0]):
        #this is for bars
        rows = np.array([subregion[0]])
    else:
        rows = np.arange(subregion[0],
                         subregion[1] + grid_space[0],
                         grid_space[0])

    if grid_space[1] >= np.abs(subregion[3] - subregion[2]):
        #this is for bars
        columns = np.array([subregion[2]])
    else:
        columns = np.arange(subregion[2],
                            subregion[3] + grid_space[1],
                            grid_space[1])

    azis, alts = np.meshgrid(columns, rows)

    grid_locations = np.transpose(np.array([alts.flatten(), azis.flatten()]))

    left_alt = monitor_alt[:, 0]
    right_alt = monitor_alt[:, -1]
    top_azi = monitor_azi[0, :]
    bottom_azi = monitor_azi[-1, :]

    left_azi = monitor_azi[:, 0]
    right_azi = monitor_azi[:, -1]
    top_alt = monitor_alt[0, :]
    bottom_alt = monitor_alt[-1, :]

    left_azi_e = left_azi - grid_space[1]
    right_azi_e = right_azi + grid_space[1]
    top_alt_e = top_alt + grid_space[0]
    bottom_alt_e = bottom_alt - grid_space[0]

    all_alt = np.concatenate((left_alt, right_alt, top_alt, bottom_alt))
    all_azi = np.concatenate((left_azi, right_azi, top_azi, bottom_azi))

    all_alt_e = np.concatenate((left_alt, right_alt, top_alt_e, bottom_alt_e))
    all_azi_e = np.concatenate((left_azi_e, right_azi_e, top_azi, bottom_azi))

    monitorPoints = np.array([all_alt, all_azi]).transpose()
    monitorPoints_e = np.array([all_alt_e, all_azi_e]).transpose()

    # get the grid points within the coverage of monitor
    if is_include_edge:
        grid_locations = grid_locations[in_hull(grid_locations, monitorPoints_e)]
    else:
        grid_locations = grid_locations[in_hull(grid_locations, monitorPoints)]

    # grid_locations = np.array([grid_locations[:, 1], grid_locations[:, 0]]).transpose()

    if is_plot:
        f = plt.figure()
        ax = f.add_subplot(111)
        ax.plot(monitorPoints[:, 1], monitorPoints[:, 0], '.r', label='monitor')
        ax.plot(monitorPoints_e[:, 1], monitorPoints_e[:, 0], '.g', label='monitor_e')
        ax.plot(grid_locations[:, 1], grid_locations[:, 0], '.b', label='grid')
        ax.legend()
        plt.show()

    return grid_locations
