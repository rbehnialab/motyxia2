"""package for all colorvision related things
"""

from .utils import butter_lowpass_filter, calculate_photonflux
