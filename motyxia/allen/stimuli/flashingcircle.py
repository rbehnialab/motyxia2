from .stim import Stim
from .util import *
import os
import numpy as np
import matplotlib.pyplot as plt
import random
import time
import h5py
import warnings
from itertools import product
from scipy.stats import truncnorm

from . import tools

try:
    import skimage.external.tifffile as tf
except ImportError:
    import tifffile as tf


class FlashingCircle(Stim):
    """
    Generate flashing circle stimulus.

    Stimulus routine presents a circle centered at the position `center`
    with given `radius`.

    Parameters
    ----------
    monitor : monitor object
        contains display monitor information
    indicator : indicator object
        contains indicator information
    coordinate : str from {'degree','linear'}, optional
        specifies coordinates, defaults to 'degree'
    background : float, optional
        color of background. Takes values in [-1,1] where -1 is black and 1
        is white
    pregap_dur : float, optional
        amount of time (in seconds) before the stimulus is presented, defaults
        to `2.`
    postgap_dur : float, optional
        amount of time (in seconds) after the stimulus is presented, defaults
        to `3.`
    center : 2-tuple, optional
        center coordinate (altitude, azimuth) of the circle in degrees, defaults to (0.,60.).
    radius : float, optional
        radius of the circle, defaults to `10.`
    is_smooth_edge : bool
        True, smooth circle edge with smooth_width_ratio and smooth_func
        False, do not smooth edge
    smooth_width_ratio : float, should be smaller than 1.
        the ratio between smooth band width and radius, circle edge is the middle
        of smooth band
    smooth_func : function object
        this function take to inputs
            first, ndarray storing the distance from each pixel to smooth band center
            second, smooth band width
        returns smoothed mask with same shape as input ndarray
    color : float, optional
        color of the circle, takes values in [-1,1], defaults to `-1.`
    iteration : int, optional
        total number of flashes, defaults to `1`.
    flash_frame : int, optional
        number of frames that circle is displayed during each presentation
        of the stimulus, defaults to `3`.
    """

    def __init__(self, monitor, indicator, coordinate='degree', center=(0., 60.),
                 radius=10., is_smooth_edge=False, smooth_width_ratio=0.2,
                 smooth_func=blur_cos, color=-1., flash_frame_num=3,
                 pregap_dur=2., postgap_dur=3., background=0., midgap_dur=1.,
                 iteration=1, seed=None):

        """
        Initialize `FlashingCircle` stimulus object.
        """

        super(FlashingCircle, self).__init__(monitor=monitor,
                                             indicator=indicator,
                                             background=background,
                                             coordinate=coordinate,
                                             pregap_dur=pregap_dur,
                                             postgap_dur=postgap_dur,
                                             seed=seed)

        #self.stim_name = 'FlashingCircle'
        self.center = center
        self.radius = float(radius)
        self.color = float(color)
        self.flash_frame_num = int(flash_frame_num)
        self.frame_config = ('is_display', 'indicator color [-1., 1.]')
        self.is_smooth_edge = is_smooth_edge
        self.smooth_width_ratio = float(smooth_width_ratio)
        self.smooth_func = smooth_func
        self.midgap_dur = float(midgap_dur)
        self.iteration = int(iteration)

        if self.pregap_frame_num + self.postgap_frame_num == 0:
            raise ValueError('pregap_frame_num + postgap_frame_num should be larger than 0.')

        self.clear()

    def set_flash_frame_num(self, flash_frame_num):
        self.flash_frame_num = flash_frame_num
        self.clear()

    def set_color(self, color):
        self.color = color
        self.clear()

    def set_center(self, center):
        self.center = center
        self.clear()

    def set_radius(self, radius):
        self.radius = radius
        self.clear()

    @property
    def midgap_frame_num(self):
        return int(self.midgap_dur * self.monitor.refresh_rate)

    def generate_frames(self):
        """
        function to generate all the frames needed for the stimulation.


        Information contained in each frame:
           first element :
                during a gap, the value is equal to 0 and during display the
                value is equal to 1
           second element :
                corresponds to the color of indicator
                if indicator.is_sync is True, during stimulus the value is
                equal to 1., whereas during a gap the value isequal to -1.;
                if indicator.is_sync is False, indicator color will alternate
                between 1. and -1. at the frequency as indicator.freq
        Returns
        -------
        frames : list
            list of information defining each frame.
        """

        frames = [[0, -1.]] * self.pregap_frame_num

        for iter in range(self.iteration):

            if self.indicator.is_sync:
                frames += [[0, -1.]] * self.midgap_frame_num
                frames += [[1, 1.]] * self.flash_frame_num
            else:
                frames += [[0, -1.]] * self.midgap_frame_num
                frames += [[1, -1.]] * self.flash_frame_num

        frames += [[0, -1.]] * self.postgap_frame_num

        frames = frames[self.midgap_frame_num:]

        if not self.indicator.is_sync:
            for frame_ind in range(frames.shape[0]):
                # mark unsynchronized indicator
                if np.floor(frame_ind // self.indicator.frame_num) % 2 == 0:
                    frames[frame_ind, 1] = 1.
                else:
                    frames[frame_ind, 1] = -1.

        frames = [tuple(x) for x in frames]

        return tuple(frames)

    def _generate_frames_for_index_display(self):
        """
        frame structure: first element: is_gap (0:False; 1:True).
                         second element: indicator color [-1., 1.]
        """
        if self.indicator.is_sync:
            gap = (0., -1.)
            flash = (1., 1.)
            frames = (gap, flash)
            return frames
        else:
            raise NotImplementedError( "method not available for non-sync indicator")

    def _generate_display_index(self):
        """ compute a list of indices corresponding to each frame to display. """
        if self.indicator.is_sync:

            index_to_display = [0] * self.pregap_frame_num

            for iter in range(self.iteration):
                index_to_display += [0] * self.midgap_frame_num
                index_to_display += [1] * self.flash_frame_num

            index_to_display += [0] * self.postgap_frame_num
            index_to_display = index_to_display[self.midgap_frame_num:]

            return index_to_display
        else:
            raise NotImplementedError( "method not available for non-sync indicator")

    def generate_movie_by_index(self):
        """ compute the stimulus movie to be displayed by index. """

        # compute unique frame parameters
        self.frames_unique = self._generate_frames_for_index_display()
        self.index_to_display = self._generate_display_index()

        num_frames = len(self.frames_unique)
        num_pixels_width = self.monitor.deg_coord_x.shape[0]
        num_pixels_height = self.monitor.deg_coord_x.shape[1]

        full_sequence = np.zeros((num_frames,
                                  num_pixels_width,
                                  num_pixels_height),
                                 dtype=np.float32)

        indicator_width_min = int(self.indicator.center_width_pixel
                               - self.indicator.width_pixel / 2)
        indicator_width_max = int(self.indicator.center_width_pixel
                               + self.indicator.width_pixel / 2)
        indicator_height_min = int(self.indicator.center_height_pixel
                                - self.indicator.height_pixel / 2)
        indicator_height_max = int(self.indicator.center_height_pixel
                                + self.indicator.height_pixel / 2)

        background = self.background * np.ones((num_pixels_width,
                                                num_pixels_height),
                                               dtype=np.float32)

        if self.coordinate == 'degree':
            map_azi = self.monitor.deg_coord_x
            map_alt = self.monitor.deg_coord_y

        elif self.coordinate == 'linear':
            map_azi = self.monitor.lin_coord_x
            map_alt = self.monitor.lin_coord_y
        else:
            raise LookupError("`coordinate` not in {'linear','degree'}")

        circle_mask = get_circle_mask(map_alt=map_alt, map_azi=map_azi,
                                      center=self.center, radius=self.radius,
                                      is_smooth_edge=self.is_smooth_edge,
                                      blur_ratio=self.smooth_width_ratio,
                                      blur_func=self.smooth_func).astype(np.float32)
        # plt.imshow(circle_mask)
        # plt.show()

        for i, frame in enumerate(self.frames_unique):
            if frame[0] == 1:
                full_sequence[i] = self.color * circle_mask - background * (circle_mask - 1)

            full_sequence[i, indicator_height_min:indicator_height_max,
            indicator_width_min:indicator_width_max] = frame[1]

        mondict = dict(self.monitor.__dict__)
        indicator_dict = dict(self.indicator.__dict__)
        indicator_dict.pop('monitor')
        NFdict = dict(self.__dict__)
        NFdict.pop('monitor')
        NFdict.pop('indicator')
        NFdict.pop('smooth_func')
        full_dict = {'stimulation': NFdict,
                     'monitor': mondict,
                     'indicator': indicator_dict}

        return full_sequence, full_dict

    def generate_movie(self):
        """
        generate movie frame by frame.
        """

        self.frames = self.generate_frames()

        full_seq = np.zeros((len(self.frames), self.monitor.deg_coord_x.shape[0],
                             self.monitor.deg_coord_x.shape[1]),
                            dtype=np.float32)

        indicator_width_min = int(self.indicator.center_width_pixel -
                               (self.indicator.width_pixel / 2))
        indicator_width_max = int(self.indicator.center_width_pixel +
                               (self.indicator.width_pixel / 2))
        indicator_height_min = int(self.indicator.center_height_pixel -
                                (self.indicator.height_pixel / 2))
        indicator_height_max = int(self.indicator.center_height_pixel +
                                (self.indicator.height_pixel / 2))

        background = np.ones((np.size(self.monitor.deg_coord_x, 0),
                              np.size(self.monitor.deg_coord_x, 1)),
                             dtype=np.float32) * self.background

        if self.coordinate == 'degree':
            map_azi = self.monitor.deg_coord_x
            map_alt = self.monitor.deg_coord_y

        elif self.coordinate == 'linear':
            map_azi = self.monitor.lin_coord_x
            map_alt = self.monitor.lin_coord_y
        else:
            raise LookupError("`coordinate` not in {'linear','degree'}")

        circle_mask = get_circle_mask(map_alt=map_alt, map_azi=map_azi,
                                      center=self.center, radius=self.radius,
                                      is_smooth_edge=self.is_smooth_edge,
                                      blur_ratio=self.smooth_width_ratio,
                                      blur_func=self.smooth_func).astype(np.float32)

        for i in range(len(self.frames)):
            curr_frame = self.frames[i]

            if curr_frame[0] == 0:
                curr_FC_seq = background
            else:
                curr_FC_seq = ((circle_mask * self.color) +
                               ((-1 * (circle_mask - 1)) * background))

            curr_FC_seq[indicator_height_min:indicator_height_max,
            indicator_width_min:indicator_width_max] = curr_frame[1]

            full_seq[i] = curr_FC_seq

            if i in range(0, len(self.frames), len(self.frames) / 10):
                print('Generating numpy sequence: '
                       + str(int(100 * (i + 1) / len(self.frames))) + '%')

        mondict = dict(self.monitor.__dict__)
        indicator_dict = dict(self.indicator.__dict__)
        indicator_dict.pop('monitor')
        NFdict = dict(self.__dict__)
        NFdict.pop('monitor')
        NFdict.pop('indicator')
        NFdict.pop('smooth_func')
        full_dict = {'stimulation': NFdict,
                     'monitor': mondict,
                     'indicator': indicator_dict}

        return full_seq, full_dict
