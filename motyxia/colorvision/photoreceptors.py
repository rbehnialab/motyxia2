"""class to calculate photon capture and more
"""

import numpy as np
import pandas as pd
import warnings
from scipy.optimize import lsq_linear, least_squares

from .spectrum import Filter, Filters, Spectrum, Spectra

#TODO self screening of photoreceptors, unit conscious

class Opsin(Filter):

    def capture(self, other, intensity=None, combine=False):
        """capture method
        """
        # TODO
        if isinstance(other, Spectrum):
            if intensity is None:
                intensity = other.i
            if not isinstance(other, Filter):
                wl, opsin, spectrum = self._interpolate(other)
                opsin = Filter(opsin, wl)
                spectrum = Spectrum(spectrum, wl, intensity=intensity)
                return np.sum(spectrum.s * spectrum.i * opsin.s)

            else:
                raise NotImplementedError('cannot capture a filter')

        elif isinstance(other, Spectra):
            if intensity is None:
                intensity = other.i
            if not isinstance(other, Filters):
                wl, opsin, spectra = self._interpolate(other)
                opsin = Filter(opsin, wl)
                spectra = Spectra(spectra, wl, intensities=intensity)
                captures = np.dot(opsin.s, spectra.s * spectra.i[None, :])
                if combine:
                    return np.sum(captures)
                else:
                    return captures

            else:
                raise NotImplementedError('cannot capture a filter')

        else:
            raise TypeError(
                f'cannot use capture method with type {type(other)}')

class Opsins(Filters):

    def capture(self, other, intensity=None, combine=False):
        """capture method
        """
        # TODO
        if isinstance(other, Spectrum):
            if intensity is None:
                intensity = other.i
            if not isinstance(other, Filter):
                wl, opsins, spectrum = self._interpolate(other)
                opsins = Filters(opsins, wl)
                spectrum = Spectrum(spectrum, wl, intensity=intensity)
                return np.dot(spectrum.s * spectrum.i, opsins.s)

            else:
                raise NotImplementedError('cannot capture a filter')

        elif isinstance(other, Spectra):
            if intensity is None:
                intensity = other.i
            if not isinstance(other, Filters):
                wl, opsins, spectra = self._interpolate(other)
                opsins = Filters(opsins, wl)
                spectra = Spectra(spectra, wl, intensities=intensity)
                captures = np.dot(spectra.s.T * spectra.i[:, None], opsins.s)
                if combine:
                    return np.sum(captures, axis=0)
                else:
                    return captures

            else:
                raise NotImplementedError('cannot capture a filter')

        else:
            raise TypeError(
                f'cannot use capture method with type {type(other)}')

    def fit(
        self, other, target_capture,
        spectra_bounds=(-np.inf, np.inf),
        bg_q=None, use_minimizer=None,
        weights=None, cholesky_whitening=False,
        **kwargs
    ):
        """fit a combination of spectra to target photon capture
        and return the weights for the spectra
        """
        if np.any(np.isnan(target_capture)):
            raise ValueError('nans in target capture.')
        if not isinstance(other, Spectra):
            raise TypeError(f'spectra not Spectra, but {type(other)}')

        #TODO handling wavelength difference

        wl, opsins, spectra = self._interpolate(other)
        opsins /= np.max(opsins, axis=0, keepdims=True)
        spectra *= other.i[None, :] / np.sum(spectra, axis=0, keepdims=True)

        X = np.dot(opsins.T, spectra) #calculating photon capture!!!

        if bg_q is not None:
            X /= bg_q[:, None]

        if cholesky_whitening:
            # whiten
            cov = np.dot(opsins.T, opsins)
            var = np.diag(cov)
            cc = cov/np.sqrt(var[:,None]*var[None,:])
            C = np.linalg.cholesky(cc)
            Cinv = np.linalg.inv(C)
            X = np.dot(Cinv, X)
            target_capture = np.dot(Cinv, target_capture)

        # qi/bi = w1 * qi1/bi + w2 * qi2/bi ...
        # qj/bj = w1 * qj1/bj + w2 * qj2/bj ...

        if weights is not None:
            weights = np.asarray(weights)

            if use_minimizer is None:
                use_minimizer = 'linear'

        spectra_bounds = tuple(spectra_bounds.T)

        def calculate_res(w, X, target_capture):

            # residuals = (np.dot(X, w) - target_capture)**2
            if use_minimizer == 'linear':
                residuals = (
                    (np.dot(X, w)) - (target_capture)
                    )**2
            elif use_minimizer == 'log':
                residuals = (
                    np.log(np.dot(X, w)) - np.log(target_capture)
                    )**2
            elif use_minimizer == 'log_disperse':
                residuals = (
                    (np.log(np.dot(X, w))
                     - np.log(target_capture)
                     )/np.log(target_capture)
                    )**2
            elif use_minimizer == 'disperse':
                residuals = (
                    (np.dot(X, w) - target_capture)/target_capture
                )**2
            else:
                raise NameError(
                    'res calculation {} '
                    'does not exist.'.format(use_minimizer))

            if weights is None:
                return residuals
            else:
                return weights * residuals

        # try:
        if use_minimizer is not None:
            # x0 = np.mean(spectra_bounds, axis=0)
            result = lsq_linear(
                X, target_capture, bounds=spectra_bounds, **kwargs)
            x0 = result.x
            # calculate glm
            result = least_squares(
                calculate_res, x0, args=(X, target_capture),
                bounds=spectra_bounds,
                max_nfev=1000*len(x0)*(len(x0)+1),
                **kwargs)
            print(result.cost, result.fun)
        else:
            result = lsq_linear(
                X, target_capture, bounds=spectra_bounds, **kwargs)

        result.residuals = np.dot(X, result.x) - target_capture
        # except ValueError:
        #     x0 = np.mean(spectra_bounds, axis=0)
        #     result = least_squares(
        #         calculate_residuals, x0, args=(X, target_capture),
        #         bounds=spectra_bounds,
        #         **kwargs)

        if not result.success:
            warnings.warn('capture fitting not a success')
            print(result.message)

        #else:
        #    print(result.message)

        return result
