from .steps import Steps
from itertools import product
import pandas as pd
import numpy as np
from collections import OrderedDict
import random
from sklearn.metrics.pairwise import euclidean_distances
import matplotlib.pyplot as plt

from ...utils import gaussian, read_json
from ...colorvision.spectrum import Spectrum, Spectra, Filter, Filters
class CaptureNoise(Steps):
    """create stimuli according to photon capture plane

    Parameters
    ----------
    ao_devices : devices.AoDevices
        the analog output device instance
    """

    def __init__(
        self,
        ao_devices,
        noise_duration=30,
        noise_generator='truncnorm',
        noise_mean=0,
        noise_std=1,
        time_constant=0,
        receptor_model='linear',
        update_rate=None,
        constant_correlation=None,
        background_spectrum=None,
        background_wavelengths=None,
        background_intensity=None,
        background=None,
        skip_r2_below=None,
        use_minimizer=None,
        opsin_weights=None,
        cholesky_whitening=False,
        **kwargs
    ):

        self.ao_devices = ao_devices

        self.bg_spectrum = None
        self.opsins = ao_devices.opsins
        self.bg_fit = None
        self.channel_names = ao_devices.channel_order #does not include trigger!
        self.led_spectra = ao_devices.led_spectra
        self.measurement_range = ao_devices.measurement_range
        self.use_minimizer = use_minimizer
        self.opsin_weights = opsin_weights
        self.noise_duration = noise_duration
        self.update_rate = update_rate
        self.constant_correlation = constant_correlation
        self.cholesky_whitening = cholesky_whitening
        self.noise_generator = noise_generator
        self.noise_mean = noise_mean
        self.noise_std = noise_std
        self.receptor_model = receptor_model
        self.time_constant = time_constant

        if self.opsins is None:
            raise ValueError('Must provide opsin data for spectra steps.')
        if self.led_spectra is None:
            raise ValueError('Must provide led spectra data for spectra steps.')

        #should contain all ao mappings
        #but set to None
        #problem if mapping name when setting to None
        #problem for dataframe
        #general purpose fitting
        #should be optimized to mimic background
        background, bg_intensities = self._get_background_values(
            background,
            background_spectrum,
            background_wavelengths,
            background_intensity
        )

        self.bg_q = self.opsins.capture(
            self.led_spectra,
            intensity=bg_intensities, combine=True)

        self.bg_ordered = np.array(
            pd.Series(background)[self.channel_names])

        print('bg q: {}'.format(self.bg_q))

        # need to calculate relative photon capture
        self.opsins_min = self.opsins.capture(
            self.led_spectra, self.measurement_range[:, 0], True
            )/self.bg_q
        self.opsins_max = self.opsins.capture(
            self.led_spectra, self.measurement_range[:, 1], True
            )/self.bg_q
        self.opsins_range = np.array(
            [self.opsins_min, self.opsins_max]).T

        #find single wavelength simulation intensities
        self.values_frame, self.target_qs = self._get_values_frame()

        #calculate r2 values for each target q vector
        self.values_frame = self._calculate_residuals(
            self.values_frame,
            self.target_qs)

        #skip certain simulations with bad r2 values
        # TODO work with combined
        if skip_r2_below is not None:
            self.values_frame = self.values_frame[
                (self.values_frame['r2_values'] > skip_r2_below)]

        # create values dict as holder
        values_dict = self.values_frame[self.channel_names].to_dict('list')

        #
        super().__init__(
            ao_devices, values_dict,
            background=background,
            randomize=False,
            pause_durations=[0],
            durations=[1/self.update_rate],
            repeat=1,
            **kwargs)

    def _get_values_frame(self):
        """
        """

    def _calculate_residuals(
        self, values_frame, target_qs,
        message='r2 value for noisy stimulus:'):
        """
        """

        #calculate r2 values
        residuals = np.array(values_frame['residuals'].apply(pd.Series))
        actual_qs = target_qs + residuals #look at lnlsq code
        mean_target = np.mean(target_qs, axis=0, keepdims=True)
        sum_total = np.sum((target_qs-mean_target)**2, axis=1)
        sum_res = np.sum(residuals**2, axis=1)
        r2_values = 1 - sum_res/sum_total

        # add r2 values
        values_frame['r2_values'] = r2_values
        values_frame['sum_total'] = sum_total
        values_frame['sum_res'] = sum_res

        print(message)
        print(values_frame['r2_values'].describe())

        # add target capture
        for name, target_q, actual_q in zip(
            self.opsins.names, target_qs.T, actual_qs.T):
            values_frame[f'target_q_{name}'] = target_q
            values_frame[f'actual_q_{name}'] = actual_q

        return values_frame

    def _create_values_frame(self):
        """create dataframe for values
        """
        return self.values_frame

    def generate_volts(self):

        volts, volts_log = super().generate_volts()

        volts_log.update({
            'update_rate': self.update_rate,
            'noise_duration': self.noise_duration,
            'constant_correlation': self.constant_correlation,
            'noise_generator': self.noise_generator,
            'noise_mean': self.noise_mean,
            'noise_std': self.noise_std
        })

        return volts, volts_log
