from .util import *
from .locallysparsenoise import LocallySparseNoise
import os
import numpy as np
import matplotlib.pyplot as plt
import random
import time
import h5py
import warnings
from itertools import product
from scipy.stats import truncnorm

from . import tools

try:
    import skimage.external.tifffile as tf
except ImportError:
    import tifffile as tf


class WhiteNoise(LocallySparseNoise):
    """
    """

    def __init__(
        self, monitor, indicator, min_distance=20., background=0., coordinate='degree',
        grid_space=(10., 10.), probe_size=(10., 10.), probe_orientation=0.,
        probe_frame_num=6, subregion=None, sign='ON-OFF', iteration=1, repeat=1,
        pregap_dur=2., postgap_dur=3., is_include_edge=True, minmax=(-1, 1),
        refresh_distance=None, pause_frame_num=0, seed=None,
        noise_duration=100,
        ):
        """
        """
        super().__init__(
            monitor, indicator, min_distance=min_distance,
            background=background, coordinate=coordinate,
            grid_space=grid_space, probe_size=probe_size,
            probe_orientation=probe_orientation,
            probe_frame_num=probe_frame_num,
            subregion=subregion, sign=sign,
            iteration=iteration, repeat=repeat,
            pregap_dur=pregap_dur, postgap_dur=postgap_dur,
            is_include_edge=is_include_edge, minmax=minmax,
            refresh_distance=refresh_distance,
            pause_frame_num=pause_frame_num, seed=seed, 
        )

        self.number_unique_frames = int(
            (noise_duration * monitor.refresh_rate)
            /self.probe_frame_num)

        print('Unique_Frames',self.number_unique_frames)


    def _generate_all_probes(self):
        """
        return all possible (grid location + sign) combinations within the subregion,
        return a list of probe parameters, each element in the list is
        [center_altitude, center_azimuth, sign]
        """
        grid_locs = self._get_grid_locations()
        self.grid_locs = grid_locs

        #grid_locs = list([list(gl) for gl in grid_locs])

        all_probes = np.zeros(
            (self.number_unique_frames, len(grid_locs), 3)
            )

        all_probes[:,:,-1] = truncnorm.rvs(
            self.minmax[0], self.minmax[1],
            size=(self.number_unique_frames, len(grid_locs))
        )

        all_probes[:, :, :-1] = np.array(
            [grid_locs]*self.number_unique_frames)

        # print(grid_locs)
        #
        # #number of unique frames
        # all_probes = []
        # # loop of number of unique frames
        # for n in range(self.number_unique_frames):
        #     # Create intensities for each location
        #     #
        #     #
        #
        #
        #
        #     intense = truncnorm.rvs(
        #         self.minmax[0], self.minmax[1],
        #         size=len(grid_locs))
        #
        #     #intense = np.random.normal(len(grid_locs), mean, std)
        #
        #     #intense[intense < self.minmax[0]] = self.minmax[0]
        #     #intense[intense > self.minmax[1]] = self.minmax[1]
        #
        #     #create probes
        #
        #     #Original attempt:
        #     # all_probes.append(
        #     #     [gl + [intensity]
        #     #     for gl, intensity in zip(grid_locs, intense)]
        #     # )
        #
        #     #New attempt:
        #     all_probes.append(
        #         [(gl[0], gl[1], intensity)
        #         for gl, intensity in zip(grid_locs,intense)]
        #     )
        #print(all_probes[0:10])

        return all_probes

    def _generate_probe_sequence_one_iteration(self, all_probes, is_redistribute=False):
        """
        given all probes to be displayed and minimum distance between any pair of two probes
        return frames of one iteration that ensure all probes will be present once

        parameters
        ----------
        all_probes : list
            all probes to be displayed, each element (center_alt, center_azi, sign). ideally
            outputs of self._generate_all_probes()
        is_redistribute : bool
            redistribute the probes among frames after initial generation or not.
            redistribute will use self._redistribute_probes() and try to minimize the difference
            of probe numbers among different frames

        returns
        -------
        frames : tuple
            each element of the frames tuple represent one display frame, the element itself
            is a tuple of the probes to be displayed in this particular frame
        """

        np.random.seed(self.seed)
        frames = list(all_probes)
        np.random.shuffle(frames)

        return frames
