"""
"""

import motyxia
import os
import json

# Grabs the folder where the script runs.
BASEDIR = os.path.abspath(os.path.dirname(__file__))
SETTINGSDIR = os.path.join(BASEDIR, 'settings')
MONITORFILENAME = 'monitorsettings.json'
INDICATORFILENAME = 'indicatorsettings.json'
STIMULIFILENAME = 'stimulisettings.json'
PROJECTORFILENAME = 'projectorsettings.json'
MONITORSETTINGS = os.path.join(SETTINGSDIR, MONITORFILENAME)
INDICATORSETTINGS = os.path.join(SETTINGSDIR, INDICATORFILENAME)
STIMULISETTINGS = os.path.join(SETTINGSDIR, STIMULIFILENAME)
PROJECTORSETTINGS = os.path.join(SETTINGSDIR, PROJECTORFILENAME)


if not os.path.exists(SETTINGSDIR):
    raise NameError('settings directory does not exist.')

if not os.path.exists(MONITORSETTINGS):
    with open(MONITORSETTINGS, 'w') as f:
        json.dump({}, f)

if not os.path.exists(INDICATORSETTINGS):
    with open(INDICATORSETTINGS, 'w') as f:
        json.dump({}, f)

if not os.path.exists(STIMULISETTINGS):
    with open(STIMULISETTINGS, 'w') as f:
        json.dump({}, f)

if not os.path.exists(PROJECTORSETTINGS):
    with open(PROJECTORSETTINGS, 'w') as f:
        json.dump({}, f)