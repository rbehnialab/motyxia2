"""
"""

import os
import json
import argparse

from motyxia.utils import read_json
from motyxia.projector import Window
from motyxia.stimuli_combiner import StimuliCombiner
from motyxia.animate import Animator

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-l", "--location", help="location of basedir", type=str)

    args = parser.parse_args()

    config = read_json(args.location)

    window = Window(
        monitor_params=config['monitorsettings'],
        indicator_params=config['indicatorsettings'],
        background_params=config['backgroundsettings'],
        **config['projectorsettings'],
        **config['coresettings'],
        **config['aosettings']
    )

    stimuli_combiner = StimuliCombiner(
        window=window,
        **config['stimulisettings']
    )

    animator = Animator(
        window=window,
        stimuli_combiner=stimuli_combiner,
        config=config,
        **config['recordingsettings']
    )

    print(f'Initiating {config["recordingsettings"]}...')

    animator.run()
        
    animator.run_background(run_projectors=False)

    print(f'Stimulus complete for {config["recordingsettings"]}.')

    #TODO disconnect failing
