"""App classes
"""

from flask import Flask

from . import SECRET_KEY, SETTINGS_LIST, NOT_REQUIRED_SETTINGS

class Motyxia(Flask):
    to_define = set([
        'basedir', 'measurementsdir', 'settingsdir',
        'monitorsettings_file',
        'indicatorsettings_file', 'stimulisettings_file',
        'projectorsettings_file', 'backgroundsettings_file',
        'measurementsettings_file'
        ])

    def run(self, *args, **kwargs):
        if self.to_define - set(self.config):
            raise NameError(
                f"Must provide {to_define} in config of app before "
                "running app.")

        for settings_name in SETTINGS_LIST:
            if settings_name in NOT_REQUIRED_SETTINGS:
                self.config[settings_name] = {'name':'null'}
            else:
                self.config[settings_name] = None
        #add measurement settings as well
        self.config['measurementsettings'] = None

        super().run(*args, **kwargs)

#intialize app
app = Motyxia(__name__)
app.secret_key = SECRET_KEY

from . import views
from . import errors
