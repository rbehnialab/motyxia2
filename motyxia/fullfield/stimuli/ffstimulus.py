"""Full-field LED stimuli
"""

from itertools import product
import pandas as pd
import numpy as np
from collections import OrderedDict
import random
from sklearn.metrics.pairwise import euclidean_distances
import matplotlib.pyplot as plt

from ...utils import gaussian, read_json
from ...colorvision.spectrum import Spectrum, Spectra, Filter, Filters


class FfStimulus:
    """
    Base class for full field stimuli.

    Parameters
    ----------
    ao_devices : AoDevices
    pregap_dur, postgap_dur : float (in seconds)
    background : dict (in measurement units)
    iteration : int
    """

    def __init__(
        self, ao_devices,
        background=None,
        pregap_dur=0,
        postgap_dur=0,
        iteration=1,
        background_spectrum=None,
        background_wavelengths=None,
        background_intensity=None,
        ):

        self.bg_spectrum = None
        self.ao_devices = ao_devices
        self.refresh_rate = ao_devices.refresh_rate
        self.pregap_dur = pregap_dur
        self.postgap_dur = postgap_dur
        self.channel_names = ao_devices.channel_order #does not include trigger!
        self.led_spectra = ao_devices.led_spectra

        if isinstance(background_spectrum, str):
            self._background_spectrum = background_spectrum
        else:
            self._background_spectrum = None

        background, bg_intensities = self._get_background_values(
            background,
            background_spectrum,
            background_wavelengths,
            background_intensity
        )

        if not hasattr(self, 'bg_fit'):
            self.bg_fit = None

        self.background = background
        self.iteration = iteration

    def _get_background_values(
        self,
        background,
        background_spectrum,
        background_wavelengths,
        background_intensity
    ):
        """
        """

        if background_spectrum is not None:
            background = self._calculate_background(
                background_spectrum, background_wavelengths,
            )

        bg_intensities = np.array(
            pd.Series(background)[self.channel_names]
        )
        if background_intensity is not None:
            self.bg_intensity = background_intensity
            bg_intensities *= background_intensity/np.sum(bg_intensities)
            background = pd.Series(
                bg_intensities, index=self.channel_names
            ).to_dict()
        else:
            self.bg_intensity = np.sum(bg_intensities)

        return background, bg_intensities

    def generate_volts_by_index(self):
        raise NotImplementedError('generate volts by index')

    def generate_volts(self):
        raise NotImplementedError('generating volts')

    def _iterations(self):
        _mapped_values = self.mapped_values.copy()
        _dataframe = self.dataframe.copy()
        _duration = float(self.duration)

        #iterate over n-1
        for n in range(self.iteration-1):
            _mapped_values['delay'] += _duration
            _mapped_values['end'] += _duration
            _dataframe['delay'] += _duration
            _dataframe['end'] += _duration

            self.duration += _duration
            # TODO change delay and end
            self.mapped_values = self.mapped_values.append(
                _mapped_values, ignore_index=True
            )
            self.dataframe = self.dataframe.append(
                _dataframe, ignore_index=True
            )

    @property
    def name(self):
        return self.__class__.__name__

    def _calculate_background(
        self, background_spectrum,
        background_wavelengths,
    ):
        """calculate intensities for background
        """
        if isinstance(background_spectrum, str):
            if background_spectrum.endswith('.json'):
                return read_json(background_spectrum)

        self.bg_spectrum = Spectrum(
            background_spectrum, background_wavelengths,
            norm_over_wl=True,
        )

        w_bg, res_bg, rank_bg, s_bg = \
            self.led_spectra.fit_spectrum(self.bg_spectrum)

        self.bg_fit = {
            'w_bg': w_bg.tolist(),
            'res_bg': res_bg.tolist(),
            'rank_bg': int(rank_bg),
            's_bg': s_bg.tolist()
        }

        background = pd.Series(w_bg, index=self.channel_names).to_dict()

        return background
