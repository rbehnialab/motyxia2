from .sparsenoise import SparseNoise
from .locallysparsenoise import LocallySparseNoise
from .uniformcontrast import UniformContrast
from .flashingcircle import FlashingCircle
from .whitenoise import WhiteNoise
from .driftinggratingcircle import DriftingGratingCircle
from .staticgratingcircle import StaticGratingCircle
from .staticimages import StaticImages
