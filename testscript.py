"""
"""

import os
import json
import argparse
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from motyxia.utils import read_json
from motyxia.projector import Window
from motyxia.stimuli_combiner import StimuliCombiner

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-l", "--location", help="location of basedir", type=str)

    args = parser.parse_args()

    config = read_json(args.location)

    window = Window(
        monitor_params=config['monitorsettings'],
        indicator_params=config['indicatorsettings'],
        background_params=config['backgroundsettings'],
        **config['projectorsettings'],
        **config['coresettings'],
        **config['aosettings']
    )


    if window.has_ao:

        try:
            plt.switch_backend('wxAgg')
            switched_backend = True
        except:
            switched_backend = False

        window.ao_devices.plot_measurement_mapping()

        sns.despine(offset=20, trim=True)

        if switched_backend:
            mng = plt.get_current_fig_manager()
            mng.frame.Maximize(True)

        plt.savefig('measurements/calibration.pdf')
        plt.show()



    stimuli_combiner = StimuliCombiner(
        window=window,
        **config['stimulisettings']
    )

    if window.has_ao:
        ao_sequence = stimuli_combiner.ao_sequence.T
        channel_order = window.ao_devices.channel_order
        refresh_rate = window.refresh_rate
        t = np.arange(ao_sequence.shape[1]) * 1/refresh_rate

        plt.figure(figsize=(5*int(np.max(t)), len(channel_order)))
        try:
            plt.switch_backend('wxAgg')
            switched_backend = True
        except:
            switched_backend = False

        for ao_seq, channel in zip(ao_sequence, channel_order):
            plt.plot(t, ao_seq, label=channel, alpha=0.5)

        plt.legend()
        plt.ylabel('voltage (V)')
        plt.xlabel('time (s)')
        plt.title(f'duration: {np.max(t)}s', loc='left')
        sns.despine(offset=20, trim=True)

        if switched_backend:
            mng = plt.get_current_fig_manager()
            mng.frame.Maximize(True)
        plt.show()

    if window.has_projectors:
        raise NotImplementedError('test displaying projector stimulus')
        stimuli_combiner.build_stimulus()




    #TODO disconnect failing
