from .ffstimulus import FfStimulus
from itertools import product
import pandas as pd
import numpy as np
from collections import OrderedDict
import random
from sklearn.metrics.pairwise import euclidean_distances
import matplotlib.pyplot as plt

from ...utils import gaussian, read_json
from ...colorvision.spectrum import Spectrum, Spectra, Filter, Filters
class Steps(FfStimulus):
    """
    Single step stimulus

    Parameters
    ----------
    values_dict : dict of lists
    durations : list
    pause_durations : list
    repeat : int
    randomize : bool
    """

    def __init__(
        self, ao_devices,
        values_dict,
        durations,
        pause_durations,
        repeat=1,
        randomize=True,
        mixing=False,
        **kwargs):
        """
        """
        super().__init__(ao_devices, **kwargs)

        _values_dict = {}
        for mapping_name, values in values_dict.items():
            if values is not None:
                _values_dict[mapping_name] = values
        values_dict = _values_dict

        # for mapping_name, value in self.background.items():
        #     if mapping_name not in values_dict:
        #         values_dict[mapping_name] = [value]
        # assert set(values_dict.keys()) == set(self.background.keys())

        self.values_dict = OrderedDict(values_dict)
        self.durations = durations
        self.pause_durations = pause_durations
        self.randomize = randomize
        self.repeat = repeat
        self.mixing = mixing

        self.mapping_names = list(self.values_dict.keys())

        #background intensities
        bg = []
        for mapping_name in self.mapping_names:
            bg.append(self.background[mapping_name])
        self.bg = tuple(bg)

        self._create_dataframe()
        self._calculate_delay()
        self._map_values()
        self._iterations()

    def _create_dataframe(self):
        """
        """
        values_frame = self._create_values_frame()

        if len(self.durations) == len(self.pause_durations):
            iterable = list(zip(self.durations, self.pause_durations))
        else:
            iterable = list(product(self.durations, self.pause_durations))

        dataframe = pd.DataFrame()
        for n in range(self.repeat):
            for dur, pause in iterable:
                values_frame['dur'] = dur
                values_frame['pause'] = pause
                dataframe = dataframe.append(
                    values_frame, ignore_index=True)

        if self.randomize:
            idcs_random = np.random.permutation(
                np.arange(len(dataframe)))
            dataframe = dataframe.iloc[idcs_random].reset_index(
                ).drop('index', axis=1)
            # columns = dataframe.columns
            # data = list(dataframe)
            # random.shuffle(data)
            # dataframe = pd.DataFrame(data, columns=columns)

        #add non-mentioned mapping names to dataframe
        for mapping_name, value in self.background.items():
            #if mapping_name not in self.mapping_names:
            if mapping_name not in dataframe.columns:
                dataframe[mapping_name] = value
                self.mapping_names.append(mapping_name)

        self.dataframe = dataframe

    def _calculate_delay(self):
        #account for delays
        delays = np.cumsum(
            np.array(self.dataframe[['dur', 'pause']]).sum(1)
        ) + self.pregap_dur
        self.duration = delays[-1] + self.postgap_dur
        delays = np.concatenate([
            [self.pregap_dur], delays[:-1]
        ])
        self.dataframe['delay'] = delays
        self.dataframe['end'] = delays + np.array(self.dataframe['dur'])

    def _map_values(self):
        _mapped_values = self.ao_devices.map(self.dataframe[self.mapping_names].to_dict('list'))
        self.mapped_values = pd.DataFrame(_mapped_values)
        self.mapped_values['delay'] = self.dataframe['delay']
        self.mapped_values['end'] = self.dataframe['end']
        _mapped_bg = self.ao_devices.map(self.background)
        self.mapped_bg = pd.Series(_mapped_bg)[self.mapping_names]

    def generate_volts(self):
        """
        """
        timestamps = np.arange(0, self.duration, 1/self.refresh_rate)
        self.iter_frame_num = timestamps.shape[0]

        #initialize volts
        volts = np.ones((self.iter_frame_num, len(self.mapping_names)))
        volts *= np.array(self.mapped_bg[self.mapping_names])[None, :]

        #change voltages for specified
        for index, row in self.mapped_values.iterrows():
            tbool = (timestamps >= row['delay']) & (timestamps < row['end'])
            volts[tbool] = np.array(row[self.mapping_names])[None, :]

        #order the array correctly
        volts = pd.DataFrame(
            volts, columns=self.mapping_names
            )[self.ao_devices.channel_order].values.astype(np.float64)

        for bg_key, bg_value in self.background.items():
            self.dataframe[f'{bg_key}_background'] = bg_value

        self.dataframe['stimulus_type'] = self.name
        self.dataframe['background_spectrum'] = self._background_spectrum

        volts_log = {
            'dataframe': self.dataframe.to_dict('list'),
            'mapped_values': self.mapped_values[
                self.mapping_names].to_dict('records'),
            'mapped_bg': self.mapped_bg.to_dict(),
            'refresh_rate': self.refresh_rate,
            'pregap_dur': self.pregap_dur,
            'postgap_dur': self.postgap_dur,
            'background': dict(self.background),
            'iteration': self.iteration,
            'duration': self.duration,
            'timestamps': timestamps.tolist(),
            'ao_devices': self.ao_devices.to_dict(),
            'bg_fit': self.bg_fit
        }

        return volts, volts_log

    def _create_values_frame(self):
        """
        """
        if self.mixing:
            values = list(product(*self.values_dict.values()))
        else:
            values = []
            n = 0
            for mapping_name, ivalues in self.values_dict.items():
                _values = np.array([self.bg]*len(ivalues))
                _values[:, n] = np.array(ivalues)
                values.extend(_values.tolist())
                n += 1
