"""Stimulus Combiner class
"""

import numpy as np
from pyglet import gl
from collections import defaultdict
import pandas as pd

from psychopy import visual
from psychopy.visual import globalVars

from . import STIMULITYPES
from .allen.stimuli import UniformContrast
from .fullfield.stimuli import FfStimulus


class StimuliCombiner:

    _exclude = set(['color_mode', 'idx'])

    def __init__(
        self, window, is_by_index=True, is_interpolate=True,
        name=None, **stimuli_params):
        """
        """
        #TODO pregap_dur and postgap_dur for whole stimulus
        self.name = name
        self.window = window
        if 'ao' in window.system:
            self.ao_devices = window.ao_devices
        if self.window.ao_params:
            if not "arduino_pin" in self.window.ao_params[0]:
                self.monitor = window.allen_monitor
                self.indicator = window.allen_indicator
        else:
            self.monitor = window.allen_monitor
            self.indicator = window.allen_indicator
            
        self.is_by_index = is_by_index
        self.is_interpolate = is_interpolate
        self.display_iter = 1

        if not self.window.has_projectors:
            pass
        elif window.dual:
            self.win_rgb = 0
            self.win_uv = 1
        else:
            self.win_idx = 0

        sort_list = []
        color_modes = []
        stimuli_list = []
        frame_count = []
        sequences = []
        seq_logs = []
        ao_sequences = []
        ao_seq_logs = []
        idcs_to_display = []

        for stimulustype, stimulus_params in stimuli_params.items():

            stimulusclass = STIMULITYPES[stimulustype]

            if stimulus_params is None:
                continue

            for single_params in stimulus_params:

                color_mode = single_params.pop('color_mode', None)
                idx = single_params.pop('idx')
                #TODO remove ao_mapping

                has_excluded = (self._exclude & set(single_params))
                if has_excluded:
                    raise KeyError(
                        f'need to pop excluded keys: {has_excluded}')

                if not issubclass(stimulusclass, FfStimulus):
                    stimulus = stimulusclass(
                        monitor=self.monitor,
                        indicator=self.indicator,
                        **single_params)
                else:
                    stimulus = stimulusclass(
                        ao_devices=self.ao_devices,
                        **single_params
                    )

                #get frames
                sequence, seq_log, ao_sequence, ao_seq_log, \
                    iter_frame_num, index_to_display = \
                    self.set_stimulus(stimulus)

                frame_count.append(iter_frame_num)
                sequences.append(sequence)
                seq_logs.append(seq_log)
                ao_sequences.append(ao_sequence)
                ao_seq_logs.append(ao_seq_log)
                idcs_to_display.append(index_to_display)
                sort_list.append(idx) #order of stimuli
                color_modes.append(color_mode)
                stimuli_list.append(stimulus)

        #params: sequence, color_mode, seq_log, iter_frame_num, index_to_display
        self.stimuli_list = np.array(stimuli_list) #TODO remove - in animate

        if not self.window.has_projectors and not self.window.has_ao:
            raise Exception('No projector and no analog output present.')

        if self.window.has_ao:
            argsort = np.argsort(sort_list)

            ao_sequence, ao_seq_log, iter_frame_num = \
                self._multiple_ff_stimuli(
                    argsort, frame_count, ao_sequences,
                    ao_seq_logs, sort_list
                )

            self.iter_frame_num = iter_frame_num
            self.ao_sequence = ao_sequence
            self.ao_seq_log = ao_seq_log

        else:
            self.ao_sequence = None
            self.ao_seq_log = None

        if self.window.has_projectors:

            if self.window.dual:

                stimuli_uv = np.array(color_modes) == 'uv'
                stimuli_rgb = np.array(color_modes) != 'uv'

                sequences_uv = list(np.array(sequences)[stimuli_uv])
                sequences_rgb = list(np.array(sequences)[stimuli_rgb])
                frame_count_uv = list(np.array(frame_count)[stimuli_uv])
                frame_count_rgb = list(np.array(frame_count)[stimuli_rgb])
                seq_logs_uv = list(np.array(seq_logs)[stimuli_uv])
                seq_logs_rgb = list(np.array(seq_logs)[stimuli_rgb])
                color_modes_uv = list(np.array(color_modes)[stimuli_uv])
                color_modes_rgb = list(np.array(color_modes)[stimuli_rgb])
                sort_list_uv = list(np.array(sort_list)[stimuli_uv])
                sort_list_rgb = list(np.array(sort_list)[stimuli_rgb])
                idcs_to_display_uv = list(np.array(idcs_to_display)[stimuli_uv])
                idcs_to_display_rgb = list(np.array(idcs_to_display)[stimuli_rgb])

                self._check_dual_stimuli(
                    color_mode='rgb',
                    sort_list_other=sort_list_uv,
                    frame_count_other=frame_count_uv,
                    sequences=sequences_rgb,
                    frame_count=frame_count_rgb,
                    seq_logs=seq_logs_rgb,
                    idcs_to_display=idcs_to_display_rgb,
                    sort_list=sort_list_rgb,
                    color_modes=color_modes_rgb
                    )

                self._check_dual_stimuli(
                    color_mode='uv',
                    sort_list_other=sort_list_rgb,
                    frame_count_other=frame_count_rgb,
                    sequences=sequences_uv,
                    frame_count=frame_count_uv,
                    seq_logs=seq_logs_uv,
                    idcs_to_display=idcs_to_display_uv,
                    sort_list=sort_list_uv,
                    color_modes=color_modes_uv
                    )

                argsort_uv = np.argsort(sort_list_uv)
                argsort_rgb = np.argsort(sort_list_rgb)

                if not np.all(argsort_uv == argsort_rgb):
                    raise ValueError('Error in aligning uv and rgb stimuli.')

                self.seq_log_uv, self.sequence_uv, self.index_to_display_uv, \
                    self.proj_color_change_uv, self.iter_frame_num_uv = \
                        self._multiple_stimuli(
                            argsort=argsort_uv,
                            frame_count=frame_count_uv,
                            sequences=sequences_uv,
                            seq_logs=seq_logs_uv,
                            idcs_to_display=idcs_to_display_uv,
                            sort_list=sort_list_uv,
                            color_modes=color_modes_uv
                        )

                self.seq_log_rgb, self.sequence_rgb, \
                    self.index_to_display_rgb, \
                    self.proj_color_change_rgb, self.iter_frame_num_rgb = \
                        self._multiple_stimuli(
                            argsort=argsort_rgb,
                            frame_count=frame_count_rgb,
                            sequences=sequences_rgb,
                            seq_logs=seq_logs_rgb,
                            idcs_to_display=idcs_to_display_rgb,
                            sort_list=sort_list_rgb,
                            color_modes=color_modes_rgb
                        )

                #TODO uv and rgb assertion checks
                #shortcuts
                self.index_to_display = self.index_to_display_rgb
                self.seq_log = self.seq_log_rgb
                self.proj_color_change = self.proj_color_change_rgb
                self.sequence = self.sequence_rgb
                self.iter_frame_num = self.iter_frame_num_rgb

                self.check_stimulus()

            else:

                argsort = np.argsort(sort_list)

                if len(sort_list) > 1:
                    self.seq_log, self.sequence, self.index_to_display, \
                        self.proj_color_change, self.iter_frame_num = \
                            self._multiple_stimuli(
                                argsort=argsort,
                                frame_count=frame_count,
                                sequences=sequences,
                                seq_logs=seq_logs,
                                idcs_to_display=idcs_to_display,
                                sort_list=sort_list,
                                color_modes=color_modes
                            )
                    self.check_stimulus()


                else:
                    #single
                    self.combined = stimuli_list[0]
                    color_mode = color_modes[0]
                    self.sequence = sequences[0]
                    self.seq_log = seq_logs[0]
                    self.iter_frame_num = frame_count[0]
                    self.index_to_display = idcs_to_display[0]
                    self.check_stimulus()
                    self.proj_color_change = [color_mode] + [
                        None for k in range(self.iter_frame_num-1)
                    ]

        else:
            self.index_to_display = None
            self.seq_log = None
            self.proj_color_change = None
            self.sequence = None

        #add trigger syncing channel to volts
        if self.window.has_ni_sync:

            trigger_output = np.zeros(self.iter_frame_num)
            trigger_length = int((
                self.ao_devices.refresh_rate
                / self.ao_devices.trigger_frequency
            ) // 2)
            trigger_points = np.arange(
                0, self.iter_frame_num, trigger_length
            ).astype(int)

            for n, idx in enumerate(trigger_points):
                #set trigger to 5Volts
                if (n % 2) == 0:
                    trigger_output[idx:idx+trigger_length] = 5

            #make sure trigger always ends at 0
            trigger_output[-1] = 0

            if self.window.has_ao:
                self.ao_sequence = np.hstack([
                    self.ao_sequence, trigger_output[:, None]
                ])
            else:
                self.ao_sequence = trigger_output[:, None]

        if self.ao_sequence is not None:
            # function does not group if single tasks
            self.grouped_ao_sequence = \
                self.ao_devices.group_values(self.ao_sequence)


    def _check_dual_stimuli(
        self, color_mode,
        sort_list_other, frame_count_other,
        sequences, frame_count,
        seq_logs, idcs_to_display,
        sort_list, color_modes,
        ):
        for queue_number, iter_frame_num_other \
            in zip(sort_list_other, frame_count_other):

            #check if queue number exists
            if queue_number not in sort_list:
                #set dummy stimulus
                stimulus = UniformContrast(
                    monitor=self.monitor,
                    indicator=self.indicator,
                    coordinate='degree',
                    background=-1,
                    color=-1,#TODO change to standard background
                    pregap_dur=0,
                    postgap_dur=0,
                    duration=(
                        iter_frame_num_other/self.monitor.refresh_rate)
                )
                #TODO what to do with ao_sequence and ao_seq_log here?
                sequence, seq_log, ao_sequence, ao_seq_log, \
                    iter_frame_num, index_to_display = \
                    self.set_stimulus(stimulus)

                if not iter_frame_num_other == iter_frame_num:
                    raise NotImplementedError(
                        'problem with setting up dummy '
                        'stimulus in dual projection.')

                sequences.append(sequence)
                frame_count.append(iter_frame_num)
                seq_logs.append(seq_log)
                idcs_to_display.append(index_to_display)
                sort_list.append(queue_number)
                color_modes.append(color_mode)

    @staticmethod
    def _multiple_ff_stimuli(
        argsort, frame_count, ao_sequences,
        ao_seq_logs, sort_list
    ):
        if len(np.unique(sort_list)) != len(sort_list):
            raise NotImplementedError('overlapping queue numbers.')

        ao_sequence = []
        dataframe = pd.DataFrame()
        ao_seq_log = defaultdict(list)

        duration = 0
        for index in argsort:
            ao_sequence.extend(ao_sequences[index].tolist())
            _ao_seq_log = ao_seq_logs[index]
            _df = pd.DataFrame(_ao_seq_log['dataframe'])
            _df['delay'] += duration
            _df['end'] += duration
            _df['queue_number'] = sort_list[index]
            dataframe = dataframe.append(_df, ignore_index=True, sort=True)
            for key, value in _ao_seq_log.items():
                if key == 'timestamps':
                    value = (np.array(value) + duration).tolist()
                ao_seq_log[key].append(value)
            duration += _ao_seq_log['duration']

        iter_frame_num = np.sum(frame_count)
        #Making sure sequence is numpy array and not list
        ao_sequence = np.array(ao_sequence)
        ao_seq_log = dict(ao_seq_log)
        ao_seq_log['dataframe'] = dataframe.to_dict('list')
        ao_seq_log['timestamps'] = np.concatenate(
            ao_seq_log['timestamps']).tolist()
        ao_seq_log['duration'] = duration
        ao_seq_log['mapped_values'] = np.concatenate(
            ao_seq_log['mapped_values']).tolist()
        #These values should be the same across
        ao_seq_log['ao_devices'] = _ao_seq_log['ao_devices']
        ao_seq_log['refresh_rate'] = _ao_seq_log['refresh_rate']

        return ao_sequence, ao_seq_log, iter_frame_num

    @staticmethod
    def _multiple_stimuli(
        argsort, frame_count, sequences, seq_logs,
        idcs_to_display, sort_list, color_modes):
        """setting multiple stimuli
        """
        if len(np.unique(sort_list)) != len(sort_list):
            raise NotImplementedError('overlapping queue numbers.')

        max_index = -1
        prev_color = None #TODO maybe substitute with window.color
        proj_color_change = []
        keys = np.unique(np.concatenate(
            [list(seq_log['stimulation'].keys()) for seq_log in seq_logs]))
        stimulation_log = {
            key : []
            for key in keys
        }
        index_to_display = []

        for index in argsort:
            #change to correct indices
            _index_to_display = \
                np.array(idcs_to_display[index]) + max_index + 1
            max_index = np.max(_index_to_display)
            index_to_display.append(_index_to_display)

            #select correct color switches
            iter_frame_num = frame_count[index]
            color_mode = color_modes[index]
            if color_mode != prev_color:
                proj_color_change.extend(
                    [color_mode] + [
                        None for k in range(iter_frame_num-1)
                    ]
                )
                prev_color = color_mode
            else:
                proj_color_change.extend(
                    [None for k in range(iter_frame_num)]
                )

            #combine all stimulatin logs
            #TODO improve combining frames_unique, frames, frame_config,
            _stimulation_log = seq_logs[index]['stimulation']
            for key in keys:
                stimulation_log[key].append(
                    _stimulation_log.get(key, None))

        #TODO test if seq log not too deep
        #set return values
        seq_log = {
            'monitor':seq_logs[0]['monitor'],
            'indicator':seq_logs[0]['indicator'],
            'stimulation':stimulation_log
        }
        index_to_display = np.concatenate(index_to_display)
        seq_log['stimulation']['index_to_display'] = \
            index_to_display
        sequence = np.concatenate(np.array(sequences)[argsort], axis=0)
        proj_color_change = proj_color_change
        iter_frame_num = np.sum(frame_count)

        print(seq_log['stimulation'].keys())

        return seq_log, sequence, index_to_display, \
            proj_color_change, iter_frame_num

    def check_stimulus(self):
        """
        Check if stimulus length is correct
        """
        if self.is_by_index:
            max_index = np.max(self.index_to_display)
            min_index = np.min(self.index_to_display)
            if max_index >= self.sequence.shape[0] or min_index < 0:
                raise ValueError('Max display index range: {} is out of self.sequence frame range: {}.'
                                 .format((min_index, max_index), (0, self.sequence.shape[0] - 1)))
            if 'frames_unique' not in self.seq_log['stimulation'].keys():
                raise LookupError('"frames_unique" is not found in self.seq_log["stimulation"]. This'
                                  'is required when display by index.')
        else:
            if 'frames' not in self.seq_log['stimulation'].keys():
                raise LookupError('"frames" is not found in self.seq_log["stimulation"]. This'
                                  'is required when display by full sequence.')

        # calculate expected display time
        if self.is_by_index:
            display_time = (float(len(self.index_to_display)) *
                            self.display_iter / self.window.refresh_rate)
        else:
            display_time = (float(self.sequence.shape[0]) *
                            self.display_iter / self.window.refresh_rate)
        print('\nExpected display time: {} seconds.\n'.format(display_time))

    def set_stimulus(self, stimulus=None):
        """
        Calls the `generate_movie` method of the respective stim object and
        populates the attributes `self.sequence` and `self.seq_log`
        """
        if stimulus is None:
            stimulus = self.combined

        if self.window.has_ao and self.window.has_projectors:
            raise NotImplementedError('Full-field stimuli and projectors.')
            #TODO BUILD fake ao output if present
            #TODO Build uniformaContrast fake for sequence, seq_log,
            #index_to_display, and iter_frame_num

        if isinstance(stimulus, FfStimulus):
            ao_sequence, ao_seq_log = stimulus.generate_volts()
            iter_frame_num = stimulus.iter_frame_num
            index_to_display = None
            sequence = None
            seq_log = None

        elif self.is_by_index:
            if stimulus.stim_name in ['KSstim', 'KSstimAllDir']:
                raise LookupError(
                    'Stimulus {} does not support '
                    'indexed display.'.format(stimulus.stim_name))

            sequence, seq_log = stimulus.generate_movie_by_index()
            index_to_display = seq_log['stimulation']['index_to_display']
            iter_frame_num = len(index_to_display)
            ao_sequence = None
            ao_seq_log = None

        else:
            if stimulus.stim_name in [
                'LocallySparseNoise', 'StaticGratingCircle', 'NaturalScene']:
                raise LookupError(
                    f'Stimulus {stimulus.stim_name} does not support full '
                    'sequence display. Please use '
                    'indexed display instead (set self.is_by_index = True).')

            sequence, seq_log = stimulus.generate_movie()
            iter_frame_num = sequence.shape[0]
            index_to_display = range(iter_frame_num)
            ao_sequence = None
            ao_seq_log = None

        return sequence, seq_log, ao_sequence, ao_seq_log, \
            iter_frame_num, index_to_display

    def build_stimulus(self):
        """Creates instance of psychopy stim object.
        """

        if not self.window.has_projectors:
            pass

        elif self.window.dual:

            w_uv = self.window.wins[self.win_uv]
            self.stim_uv = visual.ImageStim(
                w_uv,
                pos=[0,0], #TODO make parameter
                autoLog=False,
                texRes=2**10,
                units='pix',
                size=self.monitor.resolution,
                interpolate=self.is_interpolate
            )

            w_rgb = self.window.wins[self.win_rgb]
            self.stim_rgb = visual.ImageStim(
                w_rgb,
                pos=[0,0], #TODO make parameter
                autoLog=False,
                texRes=2**10,
                units='pix',
                size=self.monitor.resolution,
                interpolate=self.is_interpolate
            )

        else:
            w = self.window.wins[self.win_idx]
            self.stim = visual.ImageStim(
                w,
                pos=[0,0], #TODO make parameter
                autoLog=False,
                texRes=2**10,
                units='pix',
                size=self.monitor.resolution,
                interpolate=self.is_interpolate
            )

        return self

    def animate(self, frame):
        """
        Method for drawing stim objects to back buffer. Checks if object
        should be drawn. Back buffer is brought to front with calls to flip()
        on the window.

        Parameters
        ----------
        frame : int
            current frame number
        """
        # get frame index


        if self.window.has_ao or self.window.has_ni_sync:
            # TODO think about what to do first ?
            if self.ao_devices.indicator == 'NI':
                self.ao_devices.send(self.grouped_ao_sequence[frame])
            elif self.ao_devices.indicator == 'ARDUINO':
                raise Exception('NO NEED TO ANIMATE THE STIMULI WITH AN ARDUINO')
            #raise NotImplementedError('Analog output')

        if self.window.has_projectors:
            frame_index = self.index_to_display[frame]

            # check if two projectors
            if self.window.dual:
                #
                w_uv = self.window.wins[self.win_uv]
                #set image
                self.stim_uv.setImage(self.sequence_uv[frame_index][::-1])
                #draw to back buffer
                self.stim_uv.draw(w_uv)

                w_uv.winHandle.switch_to()
                globalVars.currWindow = w_uv
                gl.glBindFramebufferEXT(
                    gl.GL_FRAMEBUFFER_EXT, w_uv.frameBuffer)

                w_rgb = self.window.wins[self.win_rgb]
                #set image
                self.stim_rgb.setImage(self.sequence_rgb[frame_index][::-1])
                # draw to back buffer
                self.stim_rgb.draw(w_rgb)

                w_rgb.winHandle.switch_to()
                globalVars.currWindow = w_rgb
                gl.glBindFramebufferEXT(
                    gl.GL_FRAMEBUFFER_EXT, w_rgb.frameBuffer)

            else:
                w = self.window.wins[self.win_idx]
                #set image
                self.stim.setImage(self.sequence[frame_index][::-1])
                # draw to back buffer
                self.stim.draw(w)
