from .ffstimulus import FfStimulus
from itertools import product
import pandas as pd
import numpy as np
from collections import OrderedDict
import random
from sklearn.metrics.pairwise import euclidean_distances
import matplotlib.pyplot as plt

from ...utils import gaussian, read_json
from ...colorvision.spectrum import Spectrum, Spectra, Filter, Filters
class ConstantBackground(FfStimulus):
    """
    Constant background stimulus
    """

    def __init__(
        self, ao_devices,
        duration,
        **kwargs
    ):
        super().__init__(ao_devices, **kwargs)
        self.duration = duration + self.pregap_dur + self.postgap_dur
        self.mapping_names = list(self.background.keys())

        self.dataframe = pd.DataFrame([self.background])
        self.dataframe['delay'] = 0
        self.dataframe['end'] = self.duration

        self._map_values()
        self._iterations()

    def _map_values(self):
        _mapped_bg = self.ao_devices.map(self.background)
        self.mapped_bg = pd.Series(_mapped_bg)[self.mapping_names]
        self.mapped_values = pd.DataFrame([_mapped_bg])[self.mapping_names]
        self.mapped_values['delay'] = self.dataframe['delay']
        self.mapped_values['end'] = self.dataframe['end']

    def generate_volts(self):
        """
        """
        timestamps = np.arange(0, self.duration, 1/self.refresh_rate)
        self.iter_frame_num = timestamps.shape[0]

        #initialize volts
        volts = np.ones((self.iter_frame_num, len(self.mapping_names)))
        volts *= np.array(self.mapped_bg[self.mapping_names])[None, :]

        #order the array correctly
        volts = pd.DataFrame(
            volts, columns=self.mapping_names
            )[self.ao_devices.channel_order].values.astype(np.float64)

        self.dataframe['stimulus_type'] = self.name
        self.dataframe['background_spectrum'] = self._background_spectrum

        volts_log = {
            'dataframe': self.dataframe.to_dict('list'),
            'mapped_values': self.mapped_values[
                self.mapping_names].to_dict('records'),
            'mapped_bg': self.mapped_bg.to_dict(),
            'refresh_rate': self.refresh_rate,
            'pregap_dur': self.pregap_dur,
            'postgap_dur': self.postgap_dur,
            'background': dict(self.background),
            'iteration': self.iteration,
            'duration': self.duration,
            'timestamps': timestamps.tolist(),
            'ao_devices': self.ao_devices.to_dict(),
            'bg_fit': self.bg_fit
        }

        return volts, volts_log
