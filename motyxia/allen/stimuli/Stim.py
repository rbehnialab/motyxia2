"""
Contains various stimulus routines
"""

import os
from ...utils import read_json,write_json
from .util import *
import numpy as np
import matplotlib.pyplot as plt
import random
import time
import h5py
import warnings
from itertools import product
from scipy.stats import truncnorm
import pickle
import numpy as np
import uuid
from abc import ABC, abstractmethod

from . import tools

try:
    import skimage.external.tifffile as tf
except ImportError:
    import tifffile as tf


class Stim(ABC):
    """
    generic class for visual stimulation. parent class for individual
    stimulus routines.

    Parameters
    ----------
    monitor : monitor object
         the monitor used to display stimulus in the experiment
    indicator : indicator object
         the indicator used during stimulus
    background : float, optional
        background color of the monitor screen when stimulus is not being
        presented, takes values in [-1,1] and defaults to `0.` (grey)
    coordinate : str {'degree', 'linear'}, optional
        determines the representation of pixel coordinates on monitor,
        defaults to 'degree'
    pregap_dur : float, optional
        duration of gap period before stimulus, measured in seconds, defaults
        to `2.`
    postgap_dur : float, optional
        duration of gap period after stimulus, measured in seconds, defaults
        to `3.`
    """

    def __init__(self, monitor, indicator, background=0., coordinate='degree',
                 pregap_dur=2., postgap_dur=3., seed=None):
        """
        Initialize visual stimulus object
        """
        ''' Initialize this seed to make stimuli constant'''
        self.seed = seed
        self.monitor = monitor
        self.indicator = indicator

        if background < -1. or background > 1.:
            raise ValueError('parameter "background" should be a float within [-1., 1.].')
        else:
            self.background = float(background)

        if coordinate not in ['degree', 'linear']:
            raise ValueError('parameter "coordinate" should be either "degree" or "linear".')
        else:
            self.coordinate = coordinate

        if pregap_dur >= 0.:
            self.pregap_dur = float(pregap_dur)
        else:
            raise ValueError('pregap_dur should be no less than 0.')

        if postgap_dur >= 0.:
            self.postgap_dur = float(postgap_dur)
        else:
            raise ValueError('postgap_dur should be no less than 0.')

        self.clear()

    @property
    def pregap_frame_num(self):
        return int(self.pregap_dur * self.monitor.refresh_rate)

    @property
    def postgap_frame_num(self):
        return int(self.postgap_dur * self.monitor.refresh_rate)

    def generate_frames(self):
        """
        place holder of function "generate_frames" for each specific stimulus
        """
        print('Nothing executed! This is a place holder function. \n'
              'See documentation in the respective stimulus.')

    def generate_movie(self):
        """
        place holder of function 'generate_movie' for each specific stimulus
        """
        print('Nothing executed! This is a place holder function. '
              'See documentation in the respective stimulus. \n'
              'It is possible that full sequence generation is not'
              'implemented in this particular stimulus. Try '
              'generate_movie_by_index() function to see if indexed '
              'sequence generation is implemented.')

    def _generate_frames_for_index_display(self):
        """
        place holder of function _generate_frames_for_index_display()
        for each specific stimulus
        """
        print('Nothing executed! This is a place holder function. \n'
              'See documentation in the respective stimulus.')

    @abstractmethod
    def _generate_display_index(self):
        """
        place holder of function _generate_display_index()
        for each specific stimulus
        """
        pass

    @abstractmethod
    def generate_movie_by_index(self):
        """
        place holder of function generate_movie_by_index()
        for each specific stimulus
        """
        pass

    def clear(self):
        if hasattr(self, 'frames'):
            del self.frames
        if hasattr(self, 'frames_unique'):
            del self.frames_unique
        if hasattr(self, 'index_to_display'):
            del self.index_to_display

        # for StaticImages
        if hasattr(self, 'images_wrapped'):
            del self.images_wrapped
        if hasattr(self, 'images_dewrapped'):
            del self.images_dewrapped
        if hasattr(self, 'altitude_wrapped'):
            del self.altitude_wrapped
        if hasattr(self, 'azimuth_wrapped'):
            del self.azimuth_wrapped
        if hasattr(self, 'altitude_dewrapped'):
            del self.altitude_dewrapped
        if hasattr(self, 'azimuth_dewrapped'):
            del self.azimuth_dewrapped

    def set_monitor(self, monitor):
        self.monitor = monitor
        self.clear()

    def set_indicator(self, indicator):
        self.indicator = indicator
        self.clear()

    def set_pregap_dur(self, pregap_dur):
        if pregap_dur >= 0.:
            self.pregap_dur = float(pregap_dur)
        else:
            raise ValueError('pregap_dur should be no less than 0.')
        self.clear()

    def set_postgap_dur(self, postgap_dur):
        if postgap_dur >= 0.:
            self.postgap_dur = float(postgap_dur)
        else:
            raise ValueError('postgap_dur should be no less than 0.')

    def set_background(self, background):
        if background < -1. or background > 1.:
            raise ValueError('parameter "background" should be a float within [-1., 1.].')
        else:
            self.background = float(background)
        self.clear()

    def set_coordinate(self, coordinate):
        if coordinate not in ['degree', 'linear']:
            raise ValueError('parameter "coordinate" should be either "degree" or "linear".')
        self.coordinate = coordinate

    @property
    def stim_name(self):
        return self.__class__.__name__

    def stored_filename(self):
        """DESCRIPTION
        """
        # TODO use inspect of __init__ instead of dict
        filename = self.stim_name

        parameters = self.__dict__
        parameters = sorted(parameters)
        #print(parameters)
        parameters.remove('monitor')
        parameters.remove('indicator')

        if 'smooth_func'in parameters:
            parameters.remove('smooth_func')

        for par in parameters:
            string="{}-{}".format(par,getattr(self,par))
            filename=filename+"_"+string

        return filename

    def check_is_caching_active(self,filename):
        """DESCRIPTION
        """

        filename_pkl='seq_'+filename
        filename_np='np_'+filename
        fmp=None
        path=os.getcwd()
        dirpath=os.path.join(path, "motyxia", "allen","stimuli", "")
        #dirpath = os.getcwd()+"/motyxia"+"/allen"+"/stimuli"
        fmp=dict(read_json(os.path.join(dirpath, "data","fmp.json")))
        '''with open(os.path.join(dirpath, "data","fmp.pkl"),'rb') as handle:
            fmp= pickle.load(handle)
        '''
        endpath_np = os.path.join(dirpath,"data",str(filename_np+'.npy'))
        endpath_pkl = os.path.join(dirpath,"data",str(filename_pkl+'.pkl'))
        endpath_pkl_short = None
        endpath_np_short = None
        pkl_present = False
        np_present = False
        if endpath_np in fmp:
            endpath_np_short = fmp[endpath_np]
            np_present = True
        if endpath_pkl in fmp:
            endpath_pkl_short = fmp[endpath_pkl]
            pkl_present = True
        #print("fmp ",fmp)
        print("While reading ")
        print("longer path: ",endpath_pkl)
        print("shorter path: ",endpath_pkl_short)


        all_files=os.listdir(os.path.join(dirpath,"data"))
        #print("all_files ********",all_files)
        if (self.seed is not None
            and np_present
            and pkl_present
            and endpath_pkl_short in all_files
            and endpath_np_short in all_files):

            full_pkl = None
            with open(os.path.join(dirpath,"data",endpath_pkl_short), 'rb') as handle:
                full_pkl= pickle.load(handle)
            full_np=np.load(os.path.join(dirpath,"data",endpath_np_short),allow_pickle=True)
            print("*** Loaded cached sequences **** ")
            return (full_np, full_pkl)
        else:
            return (dirpath,endpath_np_short,endpath_pkl_short,fmp,endpath_pkl,endpath_np)

    def write_seq_to_cache(
        self,dirpath, endpath_np_short, full_np, endpath_pkl_short,
        full_pkl, fmp, endpath_pkl, endpath_np):
        """DESCRIPTION
        """

        #TODO check that seed is not None
        if self.seed is not None:
            str_uuid=str(uuid.uuid4())
            endpath_np_short=str_uuid+'.npy'
            endpath_pkl_short=str_uuid+'.pkl'
            with open(os.path.join(dirpath,"data",endpath_pkl_short), 'wb') as handle:
                pickle.dump(full_pkl , handle, protocol=pickle.HIGHEST_PROTOCOL)
            np.save(os.path.join(dirpath,"data",endpath_np_short), full_np)
            fmp[endpath_pkl]=endpath_pkl_short
            fmp[endpath_np]=endpath_np_short
            #fmp=dict(read_json(os.path.join(dirpath, "data","fmp.json")))
            write_json(os.path.join(dirpath, "data","fmp.json"), fmp)
            '''with open(os.path.join(dirpath, "data","fmp.pkl"), 'wb') as handle:
                pickle.dump(fmp , handle, protocol=pickle.HIGHEST_PROTOCOL)
            '''

        print("***** No caching activated******")
        print("While writing ")
        print("longer path: ",endpath_pkl)
        print("shorter path: ",endpath_pkl_short)
        return (full_np, full_pkl)
