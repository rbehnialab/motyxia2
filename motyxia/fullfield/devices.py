"""Class for handling DAC devices
"""

import os
import warnings
import time
import datetime

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.interpolate import interp1d, UnivariateSpline
from collections import OrderedDict

try:
    import nidaqmx
    import nidaqmx.system
    from nidaqmx.stream_writers import \
        AnalogMultiChannelWriter, AnalogSingleChannelWriter
    NI = True
except ImportError:
    print('Could not import nidaqmx.')
    NI = False

try:
    ARDUINO = False
    import serial
    import serial.tools.list_ports # To see the list of available Arduinos
    ports = list(serial.tools.list_ports.comports())
    for p in ports:
        if "Arduino" in p.description:
            ARDUINO = True
            break
except ImportError:
    ARDUINO = False

try:
    import seabreeze.spectrometers as sb
    SB = True
except ImportError:
    SB = False

from .. import utils
from ..colorvision import butter_lowpass_filter, calculate_photonflux
from ..colorvision.photoreceptors import Opsins
from ..colorvision.spectrum import Spectrum, concat_spectra

MAX_PHOTON_COUNT = 55000 #max photon count for spectrophotometer
PHOTON_COUNT_UPPER_BOUND = 52000
PHOTON_COUNT_LOWER_BOUND = 48000


class System:
    """NI DAQ system.
    Performs necessary checks upon initialization.
    """
    __slots__ = 'system', 'device_names', 'ao_chan_names'

    def __init__(self):
        self.system = nidaqmx.system.System.local()
        devices = [
            device
            for device in self.system.devices
        ]
        self.device_names = [
            device.name
            for device in devices
        ]
        self.ao_chan_names = [
            chan
            for device in devices
            for chan in device.ao_physical_chans
        ]


class BaseMeasurement:
    """
    Class to define the functions used for NI and arduino mappings
    """
    def __init__(self):
        pass

    def save_measurement(
        self, filename, volts,
        absolute_intensity, intensities, wavelengths,
        #baselines,
        settings, photonfluxes, absolute_photonflux,
        **kwargs
    ):
        """
        """
        if os.path.exists(filename):
            raise NameError(
                f"measurement file {filename} "
                "already exists.")
        else:
            os.makedirs(filename)

        np.save(
            os.path.join(filename, 'volts.npy'),
            volts
        )
        np.save(
            os.path.join(filename, 'absolute_intensity.npy'),
            absolute_intensity
        )
        np.save(
            os.path.join(filename, 'intensities.npy'),
            intensities
        )
        np.save(
            os.path.join(filename, 'wavelengths.npy'),
            wavelengths
        )
        #save arbitrary stuff
        for key, value in kwargs.items():
            np.save(
                os.path.join(filename, key+'.npy'),
                value
            )

        if photonfluxes is not None:
            np.save(
                os.path.join(filename, 'photonfluxes.npy'),
                photonfluxes
            )
            np.save(
                os.path.join(filename, 'absolute_photonflux.npy'),
                absolute_photonflux
            )
        utils.write_json(
            os.path.join(filename, 'settings.json'),
            settings
        )

    def plot(self, ax):
            """plot absolute intensity,
            """
            raise NotImplementedError('plotting')

    def read_calibration_file(self, filename):
        """read lamp calibration dat
        """
        if filename.endswith('.cal'):
            area_text = 'Collection-area (cm^2)'
            integration_time_text = 'Integration Time (sec):'
        elif filename.endswith('.IRRADCAL'):
            area_text = 'Fiber (micron)'
            integration_time_text = 'Int. Time(usec)'
        else:
            raise NameError(filename)

        area = None
        integration_time = None
        cal_data = np.loadtxt(filename, skiprows=9)

        with open(filename, 'r') as f:
            for n in range(9):
                line = next(f)
                line = line.rstrip()
                if line.startswith(area_text):
                    area = eval(
                        line.replace(area_text, ''))
                elif line.startswith(integration_time_text):
                    integration_time = eval(
                        line.replace(integration_time_text, ''))

        if (area is None) or (integration_time is None):
            raise ValueError(
                'Could not find area or'
                ' integration time in lamp file.')

        if filename.endswith('.IRRADCAL'):
            integration_time /= 10**6
            area = np.pi * (area/2 * 10**-4)**2

        return cal_data[:, 0], cal_data[:, 1], area, integration_time

    def measure(
        self, sb_device, integration_time=0.1,
        number_of_steps=100, scale='log',
        wavelength_min=300,
        wavelength_max=700,
        baseline_interval=None,
        correct_dark_counts=True,
        correct_nonlinearity=True,
        return_value=None,
        calibration_file=None,
        average=20,
        max_integration_time=15,
        min_integration_time=0.001
        ):
        """
        """
        settings = dict(
            integration_time=integration_time,
            number_of_steps=number_of_steps,
            scale=scale,
            wavelength_min=wavelength_min,
            wavelength_max=wavelength_max,
            baseline_interval=None,
            return_value=return_value,
            correct_dark_counts=correct_dark_counts,
            correct_nonlinearity=correct_nonlinearity,
            calibration_file=calibration_file,
            average=average,
            max_integration_time=max_integration_time,
            min_integration_time=min_integration_time
        )
        print(f'starting measurement for {self.name}')

        if not SB:
            raise ImportError('seabreeze not imported for measurements')

        # get calibration file
        if calibration_file is not None:
            cal_wls, cal_data, area, cal_integration_time = \
                self.read_calibration_file(calibration_file)

        # initialize spectrophotometer
        spec = sb.Spectrometer(sb_device)
        # set integration time
        spec.integration_time_micros(integration_time*10**6)

        #new wavelengths
        _wavelengths = spec.wavelengths()
        _wl_diff = np.diff(_wavelengths).mean()
        _wl_diff_cal = np.diff(cal_wls).mean()

        # TODO add correct interpolation with wl_diff
        print(f'dLp for measurement {_wl_diff} and calibration {_wl_diff_cal}.')

        wl_diff = np.mean([_wl_diff, _wl_diff_cal])

        print(f'new dLp {wl_diff}.')

        # relevant wavelength range
        new_wavelengths = np.arange(wavelength_min, wavelength_max, wl_diff)

        #interpolate calibration data
        cal_data = interp1d(cal_wls, cal_data)(new_wavelengths)

        # set return value
        if return_value is None:
            return_value = self.zero_intensity
        #automatic selection of range
        if 'log' in scale:
            if self.vmin < 0:
                raise ValueError('vmin cannot be smaller than 0 for log scale')
            elif self.vmin == 0:
                vmin = 0.0001
            else:
                vmin = self.vmin
            if self.indicator == 'NI':
                volts = np.logspace(
                    np.log10(vmin), np.log10(self.vmax),
                    number_of_steps
                )
            elif self.indicator == 'ARDUINO':
                volts = np.unique(np.floor(np.logspace(
                    np.log10(vmin), np.log10(self.vmax),
                    number_of_steps
                )))
            if scale == 'reverselog':
                volts = self.vmax - (volts - vmin)

        elif scale == 'linear':
            volts = np.linspace(
                self.vmin, self.vmax,
                number_of_steps
            )
        else:
            raise NameError(
                f"scale {scale} not defined; set to linear or log.")

        #initialize wavelengths and intensities
        intensities = []
        integration_times = []

        if self.indicator == "NI":
            with nidaqmx.Task() as task:
                task.ao_channels.add_ao_voltage_chan(
                    self.channel_name,
                    max_val=self.vmax, min_val=self.vmin)

                writer = AnalogSingleChannelWriter(task.out_stream)

                (intensities,integration_times,new_wavelengths) = self.processing(volts,
                   integration_time, spec, correct_dark_counts, correct_nonlinearity,
                   max_integration_time, min_integration_time, baseline_interval,
                   calibration_file, cal_data, intensities, integration_times,
                   new_wavelengths, average, area, writer=writer
                   )

                writer.write_one_sample(return_value)

        elif self.indicator == "ARDUINO":
            arduino_pin = self.arduino_pin
            port = self.device
            with serial.Serial(port,9600) as ser:
                time.sleep(1)
                (intensities,integration_times,new_wavelengths) = self.processing(volts,
                       integration_time, spec, correct_dark_counts, correct_nonlinearity,
                       max_integration_time, min_integration_time, baseline_interval,
                       calibration_file, cal_data, intensities, integration_times,
                       new_wavelengths, average, area, arduino_pin=arduino_pin, ser=ser
                       )
                self.SendArduino(return_value,arduino_pin,ser)


        absolute_intensity = np.array([
            np.sum(intensity)
            for intensity in intensities
        ])

        self.volts = volts
        self.intensities = np.array(intensities)
        self.absolute_intensity = absolute_intensity
        self.wavelengths = np.array(new_wavelengths)
        self.integration_times = np.array(integration_times)

        if calibration_file is not None:
            self.photonfluxes = calculate_photonflux(
                self.intensities, self.wavelengths
            )
            self.absolute_photonflux = np.sum(self.photonfluxes, 1)
        else:
            self.photonfluxes = None
            self.absolute_photonflux = None

        self.save_function(
            self.measurement_file,
            volts=volts,
            absolute_intensity=absolute_intensity,
            intensities=intensities,
            wavelengths=self.wavelengths,
            settings=settings,
            photonfluxes=self.photonfluxes,
            absolute_photonflux=self.absolute_photonflux,
            integration_times=self.integration_times
            )

        spec.close()

        print(f'{self.name} measurement complete.')

        time.sleep(1)

    def processing(self,volts, integration_time,
               spec, correct_dark_counts, correct_nonlinearity,
               max_integration_time, min_integration_time,
               baseline_interval, calibration_file,
               cal_data, intensities, integration_times,
               new_wavelengths, average, area,
               arduino_pin=None, writer=None, ser=None):
        for value in volts:

            print(value)

            if self.indicator == 'NI':
                writer.write_one_sample(value)
            elif self.indicator == 'ARDUINO':
                self.SendArduino(value,arduino_pin,ser)
                time.sleep(0.01) #if we go to fast the arduino can't keep up


            time.sleep(integration_time)

            # set integration time
            spec.integration_time_micros(integration_time*10**6)
            new_integration_time = integration_time

            _wavelengths = spec.wavelengths()

            # first test
            _test_int = spec.intensities(
                correct_dark_counts, correct_nonlinearity)
            int_max = np.max(_test_int)

            if (int_max < PHOTON_COUNT_UPPER_BOUND) \
                & (int_max > PHOTON_COUNT_LOWER_BOUND):
                found_integration_time = True
            else:
                found_integration_time = False

            while not found_integration_time:

                factor = (
                    (PHOTON_COUNT_UPPER_BOUND+PHOTON_COUNT_LOWER_BOUND)/2
                    / int_max
                )

                new_integration_time *= factor

                if new_integration_time > max_integration_time:
                    new_integration_time = max_integration_time
                    # set integration time
                    spec.integration_time_micros(new_integration_time*10**6)

                    _test_int = spec.intensities(
                        correct_dark_counts, correct_nonlinearity)
                    int_max = np.max(_test_int)

                    if (int_max < PHOTON_COUNT_UPPER_BOUND):
                        found_integration_time = True

                elif new_integration_time < min_integration_time:
                    new_integration_time = min_integration_time
                    found_integration_time = True
                    # set integration time
                    spec.integration_time_micros(new_integration_time*10**6)

                    _test_int = spec.intensities(
                        correct_dark_counts, correct_nonlinearity)
                    int_max = np.max(_test_int)

                    if (int_max < PHOTON_COUNT_UPPER_BOUND) \
                        & (int_max > PHOTON_COUNT_LOWER_BOUND):
                        found_integration_time = True
                    elif int_max > PHOTON_COUNT_UPPER_BOUND:
                    	raise ValueError(
                    		'Must just smaller minimum integration time.')
                else:
                    # set integration time
                    spec.integration_time_micros(new_integration_time*10**6)

                    _test_int = spec.intensities(
                        correct_dark_counts, correct_nonlinearity)
                    int_max = np.max(_test_int)

                    if (int_max < PHOTON_COUNT_UPPER_BOUND) \
                        & (int_max > PHOTON_COUNT_LOWER_BOUND):
                        found_integration_time = True

            #for paranoia
            spec.integration_time_micros(new_integration_time*10**6)

            _intensities = np.zeros(_wavelengths.shape)
            #average over multiple iterations
            for i in range(average):
                _intensities += spec.intensities(
                    correct_dark_counts, correct_nonlinearity)
                time.sleep(integration_time)
            #divide to average intensities
            _intensities /= average

            _intensities = interp1d(
            	_wavelengths, _intensities
            )(new_wavelengths)

            if baseline_interval is not None:
                _intensities -= np.mean(
                    _intensities[
                        (new_wavelengths > baseline_interval[0])
                        & (new_wavelengths < baseline_interval[1])
                    ]
                )

            # convert intensities if not calibration file present
            if calibration_file is not None:
                if not correct_dark_counts:
                    raise ValueError(
                        'need to correct dark counts for '
                        'intensity conversion with calibration file')

                dLp = np.mean(np.diff(new_wavelengths))

                _intensities = (
                    cal_data
                    * (_intensities/(new_integration_time*area*dLp))
                    ) #uW/cm2

                _intensities /= 100 #W/m2

            intensities.append(_intensities)
            integration_times.append(new_integration_time)

            time.sleep(integration_time)
        return(intensities,integration_times,new_wavelengths)

    def interp(self, x):
        """
        """

        is_number = False
        if isinstance(x, (int, float)):
            is_number = True
            x = np.array([x])
        else:
            x = np.array(x)


        interp_y = np.clip(
            self.interp_func(x), self.vmin, self.vmax
        )
        #set to zero intensity where it is
        interp_y[x == 0] = self.zero_intensity

        if is_number:
            return interp_y[0]
        else:
            return interp_y


    def calculate_absolute(
        self, measurement,
        measurement_unit,
        wavelengths,
        butter_cutoff=0.4,
        rm_wls=0,
        peak_reset=60
        ):
        """
        """
        fs = 1/np.diff(wavelengths).mean()
        if butter_cutoff:
            measurement = np.apply_along_axis(
                butter_lowpass_filter, 1, measurement,
                cutoff=butter_cutoff, fs=fs
            )
        if rm_wls:
            rm_idcs = int(rm_wls*fs)
            measurement = measurement[:,rm_idcs:-rm_idcs]
            wavelengths = wavelengths[rm_idcs:-rm_idcs]

        spectrum = measurement[
            np.argmax(np.max(measurement, 1))
        ]

        if peak_reset:
            print(f'setting everything beyond +- '
                  f'{peak_reset}nm from the peak to zero.')

            max_idx = np.argmax(spectrum)
            wiggle_idx = int(peak_reset*fs)
            start = np.max([max_idx - wiggle_idx, 0])
            end = max_idx + wiggle_idx
            measurement[:, :start] = 0
            measurement[:, end:] = 0

        self.wavelengths = wavelengths

        self.spectrum = Spectrum(
            spectrum, wavelengths,
            intensity=1, name=self.name)

        absolute_intensity = np.sum(measurement, axis=1)

        if measurement_unit == 'uE':
            absolute_intensity *= 10**6
        elif measurement_unit == 'mE':
            absolute_intensity *= 10**3
        elif measurement_unit == 'nE':
            absolute_intensity *= 10**9
        elif measurement_unit == 'nW/mm2':
            absolute_intensity *= 1000
        elif measurement_unit == 'uW/cm2':
            absolute_intensity *= 100

        return absolute_intensity


    def load_measurement(self, filename, measurement_unit, **kwargs):
        """load measurements from numpy files and convert to corret units
        """

        volts = np.load(os.path.join(filename, 'volts.npy'))
        wavelengths = np.load(os.path.join(filename, 'wavelengths.npy'))

        if measurement_unit not in \
            ['E', 'uE', 'mE', 'nE', 'nW/mm2', 'uW/cm2', None, 'W/m2']:

            raise NameError(
                f'measurement unit name {measurement_unit} not valid')

        if measurement_unit in ['E', 'uE', 'mE', 'nE']:
            measurement = np.load(
                os.path.join(filename, 'photonfluxes.npy')
            )

        else:
            measurement = np.load(
                os.path.join(filename, 'intensities.npy')
            )

        absolute_intensity = self.calculate_absolute(
            measurement, measurement_unit, wavelengths, **kwargs
        )

        return volts, absolute_intensity

    def plot_measurement(self, ax1, ax2=None, color='black'):
        """
        """
        ax1.plot(self.volts, self.measurement, linestyle='--', color=color)
        ax1.plot(
            self.interp(self.measurement),
            self.measurement, linestyle='-', color=color,
            label=self.name)
        if ax2 is not None and hasattr(self, 'spectrum'):
            self.spectrum.plot(ax2)

    def map(self, values, bound_values=False):
            """Map power or photon flux values to volts
            """
            if isinstance(values, (list, tuple)):
            	values = np.array(values)

            #print(np.all((values >= self.mmin)))
            #print(np.all((values <= self.mmax)))

            if not (np.all((values >= self.mmin)) & np.all((values <= self.mmax))):
                raise ValueError(
                    f"measurement values not within "
                    f"{self.mmin} and {self.mmax}.\n"
                    f"values smaller: {values[values < self.mmin]}\n"
                    f"values bigger: {values[values > self.mmax]}"
                    )

            if isinstance(values, (int, float)):
                volts = self.interp([values])[0]
            else:
                volts = self.interp(values)

            if bound_values:
                if isinstance(volts, (int, float)):
                    if volts < self.vmin:
                        volts = self.vmin
                    elif volts > self.vmax:
                        volts = self.vmax
                else:
                    volts[volts < self.vmin] = self.vmin
                    volts[volts > self.vmax] = self.vmax

            if np.any(volts < self.vmin) or np.any(volts > self.vmax):
                raise ValueError(
                    f"voltage values not within"
                    f" {self.vmin} and {self.vmax}.")

            return volts


class AoMeasurement(BaseMeasurement):
    """Class to measure power output of NI DAQs
    """

    def __init__(
        self, device,
        mapping_name,
        output=None,
        measurement_file=None,
        measurementsdir=None,
        vmin=0, vmax=5,
        save_function=None,
        zero_intensity=None,
        indicator='NI'
    ):
        if output is None:
            self.device = device.split('/')[0]
            self.output = device.split('/')[1]
            self.channel_name = device
        else:
            self.device = device
            self.output = output
            self.channel_name = f"{device}/{output}"
        self.name = mapping_name
        self.vmin = vmin
        self.vmax = vmax
        self.indicator = indicator

        if zero_intensity is None:
            self.zero_intensity = vmin
        else:
            self.zero_intensity = zero_intensity

        if measurement_file is None:
            measurement_file = (
                f"{mapping_name}_"
                f"{datetime.datetime.now().strftime('%y%m%dT%H%M')}")

        if measurementsdir is not None:
            measurement_file = os.path.join(
                measurementsdir, measurement_file
            )
        self.measurement_file = measurement_file

        if isinstance(save_function, str):
            save_function = getattr(utils, save_function)
        elif save_function is None:
            save_function = self.save_measurement

        self.save_function = save_function


class ArduinoMeasurment(BaseMeasurement):
    """Class to measure power output of NI DAQs
    """

    def __init__(
        self, device,arduino_pin,mapping_name,
        measurement_file=None,
        measurementsdir=None,
        vmin=0, vmax=5,
        save_function=None,
        zero_intensity=None,
        indicator='ARDUINO'
    ):

        self.device = device
        self.arduino_pin = arduino_pin
        self.name = mapping_name
        self.vmin = vmin
        self.vmax = vmax
        self.indicator = indicator

        if zero_intensity is None:
            self.zero_intensity = vmin
        else:
            self.zero_intensity = zero_intensity

        if measurement_file is None:
            measurement_file = (
                f"{mapping_name}_"
                f"{datetime.datetime.now().strftime('%y%m%dT%H%M')}")

        if measurementsdir is not None:
            measurement_file = os.path.join(
                measurementsdir, measurement_file
            )
        self.measurement_file = measurement_file

        if isinstance(save_function, str):
            save_function = getattr(utils, save_function)
        elif save_function is None:
            save_function = self.save_measurement

        self.save_function = save_function


class AoMapping(AoMeasurement):
    """Analog output NI DAQ device mapping

    Parameters
    ----------
    device : str
        Name of DAQ device
    output : str
        Abbreviated name of analog output channel
    mapping_name :  str
        Common name to assign to analog output channel (e.g. duv_led1)
    measurement_file : str
        File name containing LED measurements.
    load_function : str or callable
        function to load measurement file. Must return two numpy arrays
        with the same length for the first axis. The first array corresponds
        to applied voltages and the second array corresponds to the
        measurement (in watts or photon flux normally).
    vmin, vmax : float, optional
        The maximum and minimum allowed voltage for this analog output.
        Defaults to 0 and 5 respectively.
    measurementsdir : str, optional
        Folder that contains measurement file.
    measurement_unit : str, optional
        Unit to convert to (e.g. W/m2, E, nW/mm2, uE).
    """

    def __init__(
        self, device, mapping_name,
        output=None,
        measurement_file=None,
        load_function=None,
        vmin=0, vmax=5,
        measurementsdir=None,
        measurement_unit=None,
        polyfit_degree=0,
        zero_intensity=None,
        preinterp_method='monotonic',
        indicator='NI',
        # TODO apply log to measurement etc?
        #measurement_unit='flux',
    ):
        super().__init__(
            device=device,
            output=output,
            mapping_name=mapping_name,
            measurement_file=measurement_file,
            measurementsdir=measurementsdir,
            vmin=vmin, vmax=vmax,
            zero_intensity=zero_intensity,
            indicator=indicator
        )
        self.spectrum = None

        self.measurement_unit = measurement_unit
        self.indicator = indicator

        if isinstance(load_function, str):
            load_function = getattr(utils, load_function)
        elif load_function is None:
            load_function = self.load_measurement

        if measurement_file is None:
            warnings.warn(
                f"Measurement file is None: Using unity mapping for now"
            )
            self.volts = np.array([vmin, vmax])
            self.measurement = self.volts

        elif os.path.exists(self.measurement_file):
            self.volts, self.measurement = load_function(
                self.measurement_file, measurement_unit)

        else:
            raise NameError(
                f"no LED measurement file found: {self.measurement_file}"
                f" either make a measurement or change the file."
            )

        self.mmax = np.max(self.measurement)
        self.mmin = np.min(self.measurement)

        if zero_intensity is not None:
            self.mmin = 0

        #preinterp method
        #smoothing out the noise, assuming monotonicity or other
        #enforce monotonic function
        #spline interpolation etc.
        if preinterp_method == 'spline':
            new_measurement = UnivariateSpline(
                self.volts, self.measurement, k=4,
            )(self.volts)
        elif preinterp_method == 'monotonic':
            mdiff_sign = np.sign(np.diff(self.measurement))
            sign_slope = np.sign(mdiff_sign.sum())

            if sign_slope == 0:
                raise ValueError('cannot enforce monotonicity.')

            mdiff_sign[mdiff_sign == 0] = sign_slope
            mbool = mdiff_sign == sign_slope

            if sign_slope == 1:
                #increasing
                mbool = np.concatenate([[True], mbool])
            else:
                #decreasing
                mbool = np.concatenate([mbool, [True]])

            new_measurement = interp1d(
                self.volts[mbool], self.measurement[mbool],
                bounds_error=False,
                fill_value='extrapolate'
            )(self.volts)
        else:
            new_measurement = self.measurement.copy()

        if polyfit_degree:
            #use polyfit
            argsort = np.argsort(new_measurement)
            z = np.polyfit(
                new_measurement[argsort],
                self.volts[argsort], polyfit_degree)
            interp = np.poly1d(z)

        else:
            argsort = np.argsort(new_measurement)

            interp = interp1d(
                new_measurement[argsort], self.volts[argsort],
                bounds_error=False,
                fill_value="extrapolate"
            )

        self.interp_func = interp

        # self.interp = lambda x: np.clip(
        #     interp(x),
        #     self.vmin, self.vmax
        #     #np.min(self.volts), np.max(self.volts)
        # )



    def __eq__(self, other):
        if not isinstance(other, AoMapping):
            raise TypeError(
                "__eq__ comparison only works with"
                "other AoMapping instance")

        # compare instances
        #TODO check led measurement
        output_same = self.channel_name == other.channel_name
        name_same = self.name == other.name

        if output_same and name_same:
            return True
        elif output_same and not name_same:
            raise NameError(
                f"Different names for same analog output:"
                f"{self.name} and {other.name}"
            )
        else:
            return False

    def channel_exists(self):
        """Check if DAQ channel exists
        """
        system = System()
        channel_names = [chan.name for chan in system.ao_chan_names]
        return self.channel_name in channel_names

class ArduinoMapping(ArduinoMeasurment):
    """Analog output NI DAQ device mapping

    Parameters
    ----------
    device : str
        Name of DAQ device
    output : str
        Abbreviated name of analog output channel
    mapping_name :  str
        Common name to assign to analog output channel (e.g. duv_led1)
    measurement_file : str
        File name containing LED measurements.
    load_function : str or callable
        function to load measurement file. Must return two numpy arrays
        with the same length for the first axis. The first array corresponds
        to applied voltages and the second array corresponds to the
        measurement (in watts or photon flux normally).
    vmin, vmax : float, optional
        The maximum and minimum allowed voltage for this analog output.
        Defaults to 0 and 5 respectively.
    measurementsdir : str, optional
        Folder that contains measurement file.
    measurement_unit : str, optional
        Unit to convert to (e.g. W/m2, E, nW/mm2, uE).
    """

    def __init__(
        self, device, arduino_pin,mapping_name,
        measurement_file=None,
        load_function=None,
        vmin=0, vmax=5,
        measurementsdir=None,
        measurement_unit=None,
        polyfit_degree=0,
        zero_intensity=None,
        preinterp_method='monotonic',
        indicator='ARDUINO',
        # TODO apply log to measurement etc?
        #measurement_unit='flux',
    ):
        super().__init__(
            device, arduino_pin,
            mapping_name=mapping_name,
            measurement_file=measurement_file,
            measurementsdir=measurementsdir,
            vmin=vmin, vmax=vmax,
            zero_intensity=zero_intensity,
            indicator=indicator
        )
        self.spectrum = None

        self.measurement_unit = measurement_unit
        self.indicator = indicator

        if isinstance(load_function, str):
            load_function = getattr(utils, load_function)
        else:
            load_function = self.load_measurement

        if measurement_file is None:
            warnings.warn(
                f"Measurement file is None: Using unity mapping for now"
            )
            self.volts = np.array([vmin, vmax])
            self.measurement = self.volts

        elif os.path.exists(self.measurement_file):
            self.volts, self.measurement = load_function(
                self.measurement_file, measurement_unit)

        else:
            raise NameError(
                f"no LED measurement file found: {self.measurement_file}"
                f" either make a measurement or change the file."
            )

        self.mmax = np.max(self.measurement)
        self.mmin = np.min(self.measurement)

        if zero_intensity is not None:
            self.mmin = 0

        #preinterp method
        #smoothing out the noise, assuming monotonicity or other
        #enforce monotonic function
        #spline interpolation etc.
        if preinterp_method == 'spline':
            new_measurement = UnivariateSpline(
                self.volts, self.measurement, k=4,
            )(self.volts)
        elif preinterp_method == 'monotonic':
            mdiff_sign = np.sign(np.diff(self.measurement))
            sign_slope = np.sign(mdiff_sign.sum())

            if sign_slope == 0:
                raise ValueError('cannot enforce monotonicity.')

            mdiff_sign[mdiff_sign == 0] = sign_slope
            mbool = mdiff_sign == sign_slope

            if sign_slope == 1:
                #increasing
                mbool = np.concatenate([[True], mbool])
            else:
                #decreasing
                mbool = np.concatenate([mbool, [True]])

            new_measurement = interp1d(
                self.volts[mbool], self.measurement[mbool],
                bounds_error=False,
                fill_value='extrapolate'
            )(self.volts)
        else:
            new_measurement = self.measurement.copy()

        if polyfit_degree:
            #use polyfit
            argsort = np.argsort(new_measurement)
            z = np.polyfit(
                new_measurement[argsort],
                self.volts[argsort], polyfit_degree)
            interp = np.poly1d(z)

        else:
            argsort = np.argsort(new_measurement)

            interp = interp1d(
                new_measurement[argsort], self.volts[argsort],
                bounds_error=False,
                fill_value="extrapolate"
            )

        self.interp_func = interp

        # self.interp = lambda x: np.clip(
        #     interp(x),
        #     self.vmin, self.vmax
        #     #np.min(self.volts), np.max(self.volts)
        # )



    def __eq__(self, other):
        if not isinstance(other, ArduinoMapping):
            raise TypeError(
                "__eq__ comparison only works with"
                "other AoMapping instance")

        # compare instances
        #TODO check led measurement
        pin_same = self.arduino_pin == other.arduino_pin
        name_same = self.name == other.name
        device_same = self.device == other.device

        if pin_same and name_same and device_same:
            return True
        elif (pin_same and device_same) and not name_same :
            raise NameError(
                f"Different names for same arduino and same pin:"
                f"{self.name} and {other.name}"
            )
        else:
            return False

    def SendArduino(self,value,arduino_pin,ser):

        """
        Method to send a pwm value to a given LED of the Arduino

        Args:
            value (int): pwm value of the LED
            arduino_pin (int): pin the LED is connected to
            ser: the serial object (here the Arduino)

        Returns: Nothing, it just send the appropriate data to the arduino

        We are going to use the same method as the author of MARGO:
        we convert the pwm value into binary (12 bits), then split it
        into two bytes, then convert them to decimal and send them to the
        arduino as bytes

        """
        value = np.uint32(value)
        byte1 = value >> 8 & 0b1111 #Getting the first four left bits of value
        byte2 = value & 0b11111111 #Getting the 8 right bits of value
        board = int(arduino_pin//24) #Pin 11 is on board 0, pin 25 is on board 1 and so on
        LED = int(arduino_pin%24) #Pin 25 is LED 1 of board 2, pin 18 is LED 18 of board 1 and so on
        ser.write(bytearray([byte1,byte2,board,LED]))


class BaseDevices:

    """
    Class to define the functions used for NI and arduino device
    """
    def __init__(self):
        pass

    def plot_measurement_mapping(self, ax1=None, ax2=None):

        if ax1 is None:
            fig, (ax1, ax2) = plt.subplots(nrows=2, figsize=(5, 10))

        if self.indicator == "NI":
            colors = sns.color_palette('rainbow', len(self.ao_mappings))
            n = 0

            for name, ao_mapping in self.ao_mappings.items():

                ao_mapping.plot_measurement(ax1, ax2, color=colors[n])
                n += 1

        elif self.indicator == "ARDUINO":
            colors = sns.color_palette('rainbow', len(self.arduino_mappings))
            n = 0

            for name, arduino_mapping in self.arduino_mappings.items():

                arduino_mapping.plot_measurement(ax1, ax2, color=colors[n])
                n += 1

        ax1.legend()
        ax2.legend()
        ax1.set_xlabel('volts (V)')
        ax1.set_ylabel('measurement unit')
        ax1.set_title('output range')
        ax2.set_xlabel('wavelength (nm)')
        ax2.set_ylabel('relative intensity')
        ax2.set_title('output spectrum')

    def name_check(self, dictionary):
        """assert correct name
        """
        if self.indicator == 'NI':
            assert \
                set(dictionary.keys()) == set(self.ao_mappings.keys()), \
                (f"mapping names and dictionary keys do not match: "
                f"{self.ao_mappings.keys()} != {self.dictionary.keys()}")
        elif self.indicator == 'ARDUINO':
            assert \
                set(dictionary.keys()) == set(self.arduino_mappings.keys()), \
                (f"mapping names and dictionary keys do not match: "
                f"{self.arduino_mappings.keys()} != {self.dictionary.keys()}")

    def map(self, values_dict, bound_values=False):
        """
        map measurement values to volts
        """
        #TODO photon capture conversion mapping
        self.name_check(values_dict)
        # simple mapping function of measurement units to volts
        volts = {}
        if self.indicator == 'NI':
            for mapping_name, values in values_dict.items():
                volts[mapping_name] = self.ao_mappings[mapping_name].map(
                    values, bound_values)
        elif self.indicator == 'ARDUINO':
            for mapping_name, values in values_dict.items():
                volts[mapping_name] = self.arduino_mappings[mapping_name].map(
                    values, bound_values)

        return volts

    def to_dict(self):
        if self.indicator == 'NI':
            args = {
                mapping_name:{
                    'channel_name': ao_mapping.channel_name,
                    'measurement_file': ao_mapping.measurement_file,
                    'measurement': ao_mapping.measurement.tolist(),
                    'volts': ao_mapping.volts.tolist(),
                    'measurement_unit': ao_mapping.measurement_unit,
                }
                for mapping_name, ao_mapping in self.ao_mappings.items()
            }

            return {
                "use_photon_capture": self.use_photon_capture,
                "trigger_device": self.trigger_device,
                "trigger_frequency": self.trigger_frequency,
                "channel_order": self.channel_order,
                "opsin_sensitivities": self.opsin_sensitivities,
                "opsin_wavelengths": self.opsin_wavelengths,
                "led_spectra" : (
                    None
                    if self.led_spectra is None
                    else {
                        "spectra": self.led_spectra.s.tolist(),
                        "wl": self.led_spectra.wl.tolist()
                    }
                ),
                **args
            }

        elif self.indicator == 'ARDUINO':
            args = {
                mapping_name:{
                    'measurement_file': arduino_mapping.measurement_file,
                    'measurement': arduino_mapping.measurement.tolist(),
                    'volts': arduino_mapping.volts.tolist(),
                    'measurement_unit': arduino_mapping.measurement_unit,
                }
                for mapping_name, arduino_mapping in self.arduino_mappings.items()
            }

            return {
                "use_photon_capture": self.use_photon_capture,
                "channel_order": self.channel_order,
                "opsin_sensitivities": self.opsin_sensitivities,
                "opsin_wavelengths": self.opsin_wavelengths,
                "led_spectra" : (
                    None
                    if self.led_spectra is None
                    else {
                        "spectra": self.led_spectra.s.tolist(),
                        "wl": self.led_spectra.wl.tolist()
                    }
                ),
                **args
            }

    def group_values(self, values, skip_trigger=False):
        """group volt values to correct devices
        """
        #not need to group if single tasks
        if self.single_tasks:
            return values

        assert values.ndim == 2, "values must be two dimensional"

        grouped_values = pd.DataFrame()

        if skip_trigger:
        	device_order = self.device_order[:-1]
        else:
        	device_order = self.device_order

        for n, unique_device in enumerate(self.unique_devices):

            _values = list(values[:, device_order == unique_device])

            _values = [v.astype(np.float64) for v in _values]

            grouped_values[unique_device] = _values

        return np.array(grouped_values[self.unique_devices])


class AoDevices(BaseDevices):
    """Handling analog output of NI DAQ devices.

    Parameter
    ---------
    ao_mappings : args
        list of AoMapping instances
    """

    def __init__(
        self,
        *ao_mappings,
        refresh_rate=180,
        trigger_device=None,
        trigger_frequency=0,
        single_tasks=True,
        use_photon_capture=False,
        opsin_sensitivities=None,
        opsin_wavelengths=None,
        name='test',
        indicator='NI',
        ):
        """
        """
        self.refresh_rate = refresh_rate
        self.trigger_device = trigger_device
        self.single_tasks = single_tasks
        self.use_photon_capture = use_photon_capture
        self.trigger_frequency = trigger_frequency
        self.channel_names = {}
        self.ao_mappings = {}
        self.name = name
        self.unique_devices = []
        self.channel_order = []

        if opsin_sensitivities is None:
            self.opsins = None
            self.opsin_sensitivities = opsin_sensitivities
            self.opsin_wavelengths = opsin_wavelengths
        else:
            self.opsins = Opsins(
                opsin_sensitivities, opsin_wavelengths
            )
            self.opsin_sensitivities = pd.DataFrame(
                self.opsins.s, columns=self.opsins.names
                ).to_dict('list')
            self.opsin_wavelengths = self.opsins.wl.tolist()

        # print(opsin_sensitivities)
        # print(self.opsins)

        #add all spectra together
        spectra = []
        measurement_range = []
        has_spectrum = True
        for ao_mapping in ao_mappings:
            # check that ao_mapping is the right type
            if isinstance(ao_mapping, dict):
                try:
                    ao_mapping = AoMapping(**ao_mapping)
                except TypeError as e:
                    raise KeyError(
                        f"ao mapping dict incorrect: {e}")
            elif not isinstance(ao_mapping, AoMapping):
                raise TypeError(
                    f"ao mapping wrong type: {type(ao_mapping)}")

            # does channel exist
            if not ao_mapping.channel_exists():
                raise NameError(
                    f"Channel {ao_mapping.channel_name} does not exist."
                )


            self.channel_names[ao_mapping.name] = ao_mapping.channel_name
            self.ao_mappings[ao_mapping.name] = ao_mapping
            self.channel_order.append(ao_mapping.name)
            self.unique_devices.append(ao_mapping.device)

            print(
                f"{ao_mapping.name} range: {ao_mapping.mmin} to "
                f"{ao_mapping.mmax} {ao_mapping.measurement_unit}"
            )

            measurement_range.append(
                (ao_mapping.mmin, ao_mapping.mmax)
            )

            if ao_mapping.spectrum is None:
                has_spectrum = False
            else:
                spectra.append(ao_mapping.spectrum)

        self.measurement_range = np.array(measurement_range)

        if has_spectrum and len(spectra) > 0:
            self.led_spectra = concat_spectra(spectra)
        else:
            self.led_spectra = None

        if (self.trigger_device is not None) and (self.trigger_frequency > 0):
            self.device_order = np.array(
                self.unique_devices + [self.trigger_device.split('/')[0]])
        else:
            self.device_order = np.array(self.unique_devices)

        self.unique_devices = np.unique(self.unique_devices)


    def create(self, skip_trigger=False):
        """Create task and writer
        """
        if not self.single_tasks:
            self.tasks = OrderedDict()

            #create tasks for each unique device
            for unique_device in self.unique_devices:
                self.tasks[unique_device] = nidaqmx.Task(unique_device)

            for mapping_name in self.channel_order:
                self.tasks[
                    self.ao_mappings[mapping_name].device
                ].ao_channels.add_ao_voltage_chan(
                    self.channel_names[mapping_name],
                    max_val=self.ao_mappings[mapping_name].vmax,
                    min_val=self.ao_mappings[mapping_name].vmin
                )

            if (self.trigger_device is not None) \
                and (self.trigger_frequency > 0) and not skip_trigger:

                unique_device = self.trigger_device.split('/')[0]

                if unique_device not in self.tasks:
                    self.tasks[unique_device] = nidaqmx.Task(unique_device)

                self.tasks[
                    unique_device
                ].ao_channels.add_ao_voltage_chan(
                    self.trigger_device,
                    max_val=5, min_val=0
                )

            self.writers = OrderedDict()
            for unique_device, task in self.tasks.items():
                self.writers[unique_device] = (
                    AnalogMultiChannelWriter(task.out_stream)
                )

            unique_device_order = list(self.tasks.keys())
            self.writers = list(self.writers.values())
            self.tasks = list(self.tasks.values())

        else:

            self.tasks = []
            #add ao channels
            for n, mapping_name in enumerate(self.channel_order):
                #TODO test this. Does this append or replace channels?
                self.tasks.append(nidaqmx.Task(mapping_name))
                self.tasks[n].ao_channels.add_ao_voltage_chan(
                    self.channel_names[mapping_name],
                    max_val=self.ao_mappings[mapping_name].vmax,
                    min_val=self.ao_mappings[mapping_name].vmin
                )
            #add trigger device if it exists
            if (self.trigger_device is not None) \
                and (self.trigger_frequency > 0) and not skip_trigger:
                self.tasks.append(nidaqmx.Task('syncing_trigger_output'))
                self.tasks[-1].ao_channels.add_ao_voltage_chan(
                    self.trigger_device,
                    max_val=5, min_val=0
                )
            #
            self.writers = []
            for task in self.tasks:
                self.writers.append(
                    AnalogSingleChannelWriter(task.out_stream)
                )


    def send(self, values):
        """send a specified voltage to devices
        """
        for writer, value in zip(self.writers, values):
            writer.write_one_sample(value)
        # for n, value in enumerate(values):
        #     self.writers[n].write_one_sample(value)
        # self.writer.write_one_sample(values)

    def close(self):
        for task in self.tasks:
            task.close()
        # self.task.close()

    def send_values(self, values_dict):
        """create task and send single mapped voltage value
        """

        self.create(skip_trigger=True)

        volts = pd.DataFrame(self.map(values_dict))[self.channel_order]
        volts = np.array(volts)
        volts = self.group_values(volts, skip_trigger=True)

        for volt in volts:
            self.send(volt)

        self.close()

class ArduinoDevices(BaseDevices):
    """Handling analog output of arduino devices.

    Parameter
    ---------
    arduino_mappings : args
        list of ArduinoMapping instances
    """

    def __init__(
        self,
        *arduino_mappings,
        refresh_rate=180,
        use_photon_capture=False,
        opsin_sensitivities=None,
        opsin_wavelengths=None,
        name='test',
        indicator='ARDUINO',
        single_tasks=True,
        ):
        """
        """
        self.refresh_rate = refresh_rate
        self.use_photon_capture = use_photon_capture
        self.arduino_mappings = {}
        self.name = name
        self.indicator = indicator
        self.single_tasks = single_tasks
        self.channel_order = []

        if opsin_sensitivities is None:
            self.opsins = None
            self.opsin_sensitivities = opsin_sensitivities
            self.opsin_wavelengths = opsin_wavelengths
        else:
            self.opsins = Opsins(
                opsin_sensitivities, opsin_wavelengths
            )
            self.opsin_sensitivities = pd.DataFrame(
                self.opsins.s, columns=self.opsins.names
                ).to_dict('list')
            self.opsin_wavelengths = self.opsins.wl.tolist()

        # print(opsin_sensitivities)
        # print(self.opsins)

        #add all spectra together
        spectra = []
        measurement_range = []
        has_spectrum = True
        for arduino_mapping in arduino_mappings:
            # check that ao_mapping is the right type
            if isinstance(arduino_mapping, dict):
                try:
                    arduino_mapping = ArduinoMapping(**arduino_mapping)
                except TypeError as e:
                    raise KeyError(
                        f"arduino mapping dict incorrect: {e}")
            elif not isinstance(arduino_mapping, ArduinoMapping):
                raise TypeError(
                    f"arduino mapping wrong type: {type(arduino_mapping)}")


            self.arduino_mappings[arduino_mapping.name] = arduino_mapping
            self.channel_order.append(arduino_mapping.name)

            print(
                f"{arduino_mapping.name} range: {arduino_mapping.mmin} to "
                f"{arduino_mapping.mmax} {arduino_mapping.measurement_unit}"
            )

            measurement_range.append(
                (arduino_mapping.mmin, arduino_mapping.mmax)
            )

            if arduino_mapping.spectrum is None:
                has_spectrum = False
            else:
                spectra.append(arduino_mapping.spectrum)

        self.measurement_range = np.array(measurement_range)

        if has_spectrum and len(spectra) > 0:
            self.led_spectra = concat_spectra(spectra)
        else:
            self.led_spectra = None
