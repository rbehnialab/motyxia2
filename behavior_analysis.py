#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 21 13:00:26 2019

@author: samimollard
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
import os
from motyxia.utils import read_json
plt.style.use('dark_background')




def show_results(file):
    centroid = pd.read_excel(file + '/centroid.xlsx',names = ["x","y"])
    droppedFrame = pd.read_excel(file + '/dropped_frames.xlsx')
    frame_to_remove = list(np.where(droppedFrame == True))
    abherant_point_outside_ROI = list(np.where(centroid['x'] > 65000))
    point_to_remove = np.union1d(abherant_point_outside_ROI[0], frame_to_remove[0])
    
    print("{} point to remove out of {} points ({:.2}% of total points)".format(len(point_to_remove),len(centroid),100 * len(point_to_remove)/len(centroid)))
    
    centroid = centroid.drop(point_to_remove)
    
    centroid = centroid.values
    x_coord = centroid[:,0]
    y_coord = centroid[:,1]
    
    #We dont just want to remove the time point corresponding to missing data, we want to add them 
    time = pd.read_excel(file + '/time.xlsx',names = ['time'])
    time_to_remove = []
    sumtime = time['time'][point_to_remove[0]]
    index = point_to_remove[0]
    time_lost = 0
    for i in range(1,len(point_to_remove)):
        if point_to_remove[i] == point_to_remove[i-1]+1:
            sumtime += time['time'][point_to_remove[i]]
            time_lost += time['time'][point_to_remove[i]] #not very optimised
        else:
            time_to_remove.append([sumtime,index])
            index = point_to_remove[i]
            sumtime = time['time'][point_to_remove[i]]
            time_lost += time['time'][point_to_remove[i]]
        if i == len(point_to_remove)-1:
            time_to_remove.append([sumtime,index])

    for i in range(len(time_to_remove)):
        time['time'][time_to_remove[i][1]-1] += time_to_remove[i][0]
        #what if the first frame is missed ?
    
    print('{} minutes and {:.2} seconds were lost'.format(time_lost//60,time_lost%60))
    
    time = time.drop(point_to_remove)  
    time = time.values
    
    light = []
    lights_point = [[700,500],[750,375],[819,475]] #6500 for when the lights are off at the beggining
    if os.path.exists(file + '/LEDs.xlsx'):
        LEDs = pd.read_excel(file + '/LEDs.xlsx')
        LEDs = LEDs.drop(point_to_remove)
        LEDs = LEDs.values
        for i in range(len(LEDs)):
            if 3 in LEDs[i]:
                light.append(lights_point[np.where(LEDs[i]==3)[0][0]])
            else:
                light.append([6500,6500])
    
    lightChoice = pd.read_excel(file + '/LightChoice.xlsx', names = ['choice'])
    lightChoice = lightChoice.drop(point_to_remove)
    
    configfile = file + 'config.json'
    config = read_json(configfile)
    first_choice = config["stimuli_setting"]['wls'][0]
    second_choice = config["stimuli_setting"]['wls'][1]
    
    lightChoice.loc[lightChoice['choice'] == 2, 'choice'] = first_choice
    lightChoice.loc[lightChoice['choice'] == 3, 'choice']= second_choice
    
    lightChoiceList = lightChoice.values
    
    lightChoice = lightChoice[lightChoice.choice != 0]
    
    print('The fly made ' + str(len(lightChoice)) + ' choices')
    
    first_non_zero = lightChoiceList[lightChoiceList.nonzero()[0][0]][0]
    lightChoiceList[0]=first_non_zero
    for i in range(1,len(lightChoiceList)):
        if lightChoiceList[i] == 0:
            lightChoiceList[i] = lightChoiceList[i-1]
    
    
    im = plt.imread(file + '/maze.jpg')
        
    fig, ax = plt.subplots()
    ax.imshow(im,cmap='gray', vmin=0, vmax=255)
    line, = plt.plot([], [], 'r', animated=True)
    flyDot, = plt.plot([], [], 'go')
    lightDot, = plt.plot([], [], 'bo',markersize = 20)
    time_stamp = plt.text(0.02, 0.9, '', transform=ax.transAxes,color = 'red')
    ax.set_xlim(left = min(x_coord) - 50, right =max(x_coord) + 50)
    ax.set_ylim(top = min(y_coord) - 50, bottom = max(y_coord) + 50)
    
    xdata, ydata = [],[]
    
    def init():
        line.set_data(xdata, ydata)
        flyDot.set_data([],[])
        lightDot.set_data([],[])
        time_stamp.set_text('')
        return line, time_stamp, flyDot,lightDot

    def animate(i):
        x = x_coord[i]
        y = y_coord[i]
        xdata.append(x)
        ydata.append(y)
        line.set_data(xdata[(i-20):], ydata[(i-20):])
        flyDot.set_data(x,y)
        timesec = np.cumsum(time)[i]
        minutes = timesec//60
        seconds = timesec%60
        light_chosen = lightChoiceList[i][0]
        time_stamp.set_text('time = %d minutes %.1f seconds \n Light chosen = %s' % (minutes,seconds,light_chosen))
        if os.path.exists(file + '/LEDs.xlsx'):
            lightDot.set_data(light[i][0],light[i][1])
            return time_stamp, line, flyDot, lightDot
        else:
             return time_stamp, line, flyDot

    
    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                  frames=len(x_coord), interval=1, blit=True,repeat = False)
    plt.show()
    
    
    
    
    plt.figure(2)
    count_choice = lightChoice.choice.value_counts()
    count_choice = pd.DataFrame({"choice":count_choice})
    count_choice['normalised'] = count_choice/count_choice.sum()
    count_choice['normalised'].plot(kind = 'bar',color = ['g','b'],rot = 0,title = "Normalised light Preference of the fly \n %d choices" % (len(lightChoice)))
    
    #plotting the whole trace to see incohrent points
    fig2, ax2 = plt.subplots()
    ax2.imshow(im,cmap='gray', vmin=0, vmax=255)
    ax2.set_xlim(left = min(x_coord) - 50, right =max(x_coord) + 50)
    ax2.set_ylim(top = min(y_coord) - 50, bottom = max(y_coord) + 50)
    ax2.set_title("complete trajectory of the fly")
    ax2.plot(x_coord,y_coord)
    
    
    
    return(anim)

def compute_confidence(data,bootstrap,alpha):
    #95% confidence interval on the mean
    #alpha = 0.95
    #using the more accurate pivot confidence interval
    means = []
    mu = np.mean(data)
    for i in bootstrap:
        sample = np.random.choice(data,size = (1,len(data)))
        mean = np.mean(sample)
        means.append(mean)
    means = means.sort()
    lower = np.percentile(means, 100*(1-alpha)/2)
    upper = np.percentile(means, 100*(alpha+((1-alpha)/2)))
    return([2*mu - upper,2*mu -lower])
