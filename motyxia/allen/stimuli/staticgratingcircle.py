from .stim import Stim
from .util import *
import os
import numpy as np
import matplotlib.pyplot as plt
import random
import time
import h5py
import warnings
from itertools import product
from scipy.stats import truncnorm

from . import tools
from pathlib import Path
import pickle
import uuid

try:
    import skimage.external.tifffile as tf
except ImportError:
    import tifffile as tf


class StaticGratingCircle(Stim):
    """
    Generate static grating circle stimulus

    Stimulus routine presents flashing static grating stimulus inside
    of a circle centered at `center`. The static gratings are determined by
    spatial frequencies, orientation, contrast, radius and phase. The
    routine can generate several different gratings within
    one presentation by specifying multiple values of the parameters which
    characterize the stimulus.

    Parameters
    ----------
    monitor : monitor object
        contains display monitor information
    indicator : indicator object
        contains indicator information
    coordinate : str from {'degree','linear'}, optional
        specifies coordinates, defaults to 'degree'
    background : float, optional
        color of background. Takes values in [-1,1] where -1 is black and 1
        is white
    pregap_dur : float, optional
        amount of time (in seconds) before the stimulus is presented, defaults
        to `2.`
    postgap_dur : float, optional
        amount of time (in seconds) after the stimulus is presented, defaults
        to `3.`
    center : 2-tuple of floats, optional
        coordintes for center of the stimulus (altitude, azimuth)
    sf_list : n-tuple, optional
        list of spatial frequencies in cycles/unit, defaults to `(0.08)`
    ori_list : n-tuple, optional
        list of directions in degrees, defaults to `(0., 90.)`
    con_list : n-tuple, optional
        list of contrasts taking values in [0.,1.], defaults to `(0.5)`
    radius_list : n-tuple, optional
       list of radii of circles, unit defined by `self.coordinate`, defaults
       to `(10.)`
    phase_list : n-tuple, optional
       list of phase of gratings in degrees, default (0., 90., 180., 270.)
    display_dur : float, optional
        duration of each condition in seconds, defaults to `0.25`
    midgap_dur, float, optional
        duration of gap between conditions, defaults to `0.`
    iteration, int, optional
        number of times the stimulus is displayed, defaults to `1`
    is_smooth_edge : bool
        True, smooth circle edge with smooth_width_ratio and smooth_func
        False, do not smooth edge
    smooth_width_ratio : float, should be smaller than 1.
        the ratio between smooth band width and radius, circle edge is the middle
        of smooth band
    smooth_func : function object
        this function take two inputs: 1) ndarray storing the distance from each
        pixel to smooth band center; 2) smooth band width.
        returns smoothed mask with same shape as input ndarray
    is_blank_block : bool, optional
        if True, a full screen background will be displayed as an additional grating.
        The frames of this condition will be: (1, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 or 0.0),
        the meaning of these numbers can be found in self.frame_config
    """

    def __init__(self, monitor, indicator, background=0., coordinate='degree',
                 center=(0., 60.), sf_list=(0.08,), ori_list=(0., 90.), con_list=(0.5,),
                 radius_list=(10.,), phase_list=(0., 90., 180., 270.), display_dur=0.25,
                 midgap_dur=0., iteration=1, pregap_dur=2., postgap_dur=3.,
                 is_smooth_edge=False, smooth_width_ratio=0.2, smooth_func=blur_cos,
                 is_blank_block=True, seed=None):
        """
        Initialize `StaticGratingCircle` stimulus object, inherits Parameters
        from `Stim` class
        """

        super(StaticGratingCircle, self).__init__(monitor=monitor,
                                                  indicator=indicator,
                                                  background=background,
                                                  coordinate=coordinate,
                                                  pregap_dur=pregap_dur,
                                                  postgap_dur=postgap_dur,
                                                  seed=seed)

        #self.stim_name = 'StaticGratingCircle'

        if len(center) != 2:
            raise ValueError("StaticGragingCircle: input 'center' should have "
                             "two elements: (altitude, azimuth).")
        self.center = center
        self.sf_list = list(set(sf_list))
        self.phase_list = list(set([p % 360. for p in phase_list]))
        self.ori_list = list(set([o % 180. for o in ori_list]))
        self.con_list = list(set(con_list))
        self.radius_list = list(set(radius_list))
        self.is_smooth_edge = is_smooth_edge
        self.smooth_width_ratio = smooth_width_ratio
        self.smooth_func = smooth_func

        if display_dur > 0.:
            self.display_dur = float(display_dur)
        else:
            raise ValueError('block_dur should be larger than 0 second.')

        if midgap_dur >= 0.:
            self.midgap_dur = float(midgap_dur)
        else:
            raise ValueError('midgap_dur should be no less than 0 second')

        self.iteration = iteration
        self.frame_config = ('is_display', 'spatial frequency (cycle/deg)',
                             'phase (deg)', 'orientation (deg)',
                             'contrast [0., 1.]', 'radius (deg)', 'indicator_color [-1., 1.]')
        self.is_blank_block = bool(is_blank_block)

    @property
    def midgap_frame_num(self):
        return int(self.midgap_dur * self.monitor.refresh_rate)

    @property
    def display_frame_num(self):
        return int(self.display_dur * self.monitor.refresh_rate)

    @staticmethod
    def _get_dire(ori):
        return (ori + 90.) % 180.

    def _generate_circle_mask_dict(self):
        """
        generate a dictionary of circle masks for each size in size list
        """
        masks = {}
        if self.coordinate == 'degree':
            coord_azi = self.monitor.deg_coord_x
            coord_alt = self.monitor.deg_coord_y
        elif self.coordinate == 'linear':
            coord_azi = self.monitor.lin_coord_x
            coord_alt = self.monitor.lin_coord_y
        else:
            raise ValueError('Do not understand coordinate system: {}. '
                             'Should be either "linear" or "degree".'.
                             format(self.coordinate))

        for radius in self.radius_list:
            curr_mask = get_circle_mask(map_alt=coord_alt, map_azi=coord_azi,
                                        center=self.center, radius=radius,
                                        is_smooth_edge=self.is_smooth_edge,
                                        blur_ratio=self.smooth_width_ratio,
                                        blur_func=self.smooth_func)
            masks.update({radius: curr_mask})

        return masks

    def _generate_all_conditions(self):
        """
        generate all possible conditions for one iteration given the lists of
        parameters

        Returns
        -------
        all_conditions : list of tuples
             all unique combinations of spatial frequency, phase,
             orientation, contrast, and radius. Output depends on initialization
             parameters.

        """
        all_conditions = [(sf, ph, ori, con, radius) for sf in self.sf_list
                          for ph in self.phase_list
                          for ori in self.ori_list
                          for con in self.con_list
                          for radius in self.radius_list]

        if self.is_blank_block:
            all_conditions.append((0., 0., 0., 0., 0.))

        return all_conditions

    def _generate_frames_for_index_display(self):
        """
        generate a tuple of unique frames, each element of the tuple
        represents a unique display condition including gap

        frame structure:
            0. is_display: if gap --> 0; if display --> 1
            1. spatial frequency, cyc/deg
            2. phase, deg
            3. orientation, deg
            4. contrast, [0., 1.]
            5. radius, deg
            6. indicator color, [-1., 1.]
        """

        all_conditions = self._generate_all_conditions()
        gap_frame = (0., None, None, None, None, None, -1.)
        frames_unique = [gap_frame]

        for condition in all_conditions:
            frames_unique.append((1, condition[0], condition[1], condition[2],
                                  condition[3], condition[4], 1.))
            frames_unique.append((1, condition[0], condition[1], condition[2],
                                  condition[3], condition[4], 0.))

        return frames_unique

    def _generate_display_index(self):

        random.seed(self.seed)


        if self.indicator.is_sync:

            display_frame_num = int(self.display_dur * self.monitor.refresh_rate)
            if display_frame_num < 2:
                raise ValueError('StaticGratingCircle: display_dur too short, should be '
                                 'at least 2 display frames.')
            indicator_on_frame_num = display_frame_num // 2
            indicator_off_frame_num = display_frame_num - indicator_on_frame_num

            frames_unique = self._generate_frames_for_index_display()

            if len(frames_unique) % 2 != 1:
                raise ValueError('StaticGratingCircle: the number of unique frames should odd.')
            condition_num = (len(frames_unique) - 1) / 2

            index_to_display = [0] * self.pregap_frame_num

            for iter in range(self.iteration):
                display_sequence = list(range(int(condition_num)))
                random.shuffle(display_sequence)
                for cond_ind in display_sequence:
                    index_to_display += [0] * self.midgap_frame_num
                    index_to_display += [cond_ind * 2 + 1] * indicator_on_frame_num
                    index_to_display += [cond_ind * 2 + 2] * indicator_off_frame_num

            index_to_display += [0] * self.postgap_frame_num

            # remove the extra mid gap
            index_to_display = index_to_display[self.midgap_frame_num:]

            return frames_unique, index_to_display
        else:
            raise NotImplementedError( "method not available for non-sync indicator.")

    def generate_movie_by_index(self):
        """ compute the stimulus movie to be displayed by index. """
        filename=self.stored_filename();
        is_caching_active_resp=self.check_is_caching_active(filename)
        if len(is_caching_active_resp)==2:
            return is_caching_active_resp[0],is_caching_active_resp[1]
        else:
            dirpath,endpath_np_short,endpath_seq_short,fmp,endpath_seq,endpath_np=is_caching_active_resp
            self.frames_unique, self.index_to_display = self._generate_display_index()

            # print '\n'.join([str(f) for f in self.frames_unique])

            mask_dict = self._generate_circle_mask_dict()

            num_unique_frames = len(self.frames_unique)
            num_pixels_width = self.monitor.deg_coord_x.shape[0]
            num_pixels_height = self.monitor.deg_coord_x.shape[1]

            if self.coordinate == 'degree':
                coord_azi = self.monitor.deg_coord_x
                coord_alt = self.monitor.deg_coord_y
            elif self.coordinate == 'linear':
                coord_azi = self.monitor.lin_coord_x
                coord_alt = self.monitor.lin_coord_y
            else:
                raise LookupError("`coordinate` not in {'linear','degree'}")

            indicator_width_min = int(self.indicator.center_width_pixel
                                   - self.indicator.width_pixel / 2)
            indicator_width_max = int(self.indicator.center_width_pixel
                                   + self.indicator.width_pixel / 2)
            indicator_height_min = int(self.indicator.center_height_pixel
                                    - self.indicator.height_pixel / 2)
            indicator_height_max = int(self.indicator.center_height_pixel
                                    + self.indicator.height_pixel / 2)

            mov = self.background * np.ones((num_unique_frames,
                                             num_pixels_width,
                                             num_pixels_height),
                                            dtype=np.float32)

            background_frame = self.background * np.ones((num_pixels_width,
                                                          num_pixels_height),
                                                         dtype=np.float32)

            for i, frame in enumerate(self.frames_unique):

                if frame[0] == 1 and frame[1] != 0:  # not a gap and not a blank grating

                    # curr_ori = self._get_ori(frame[3])

                    curr_grating = get_grating(alt_map=coord_alt,
                                               azi_map=coord_azi,
                                               dire=self._get_dire(frame[3]),
                                               spatial_freq=frame[1],
                                               center=self.center,
                                               phase=frame[2],
                                               contrast=frame[4])

                    curr_grating = curr_grating * 2. - 1.

                    curr_circle_mask = mask_dict[frame[5]]

                    mov[i] = ((curr_grating * curr_circle_mask) +
                              (background_frame * (curr_circle_mask * -1. + 1.)))

                # add sync square for photodiode
                mov[i, indicator_height_min:indicator_height_max,
                indicator_width_min:indicator_width_max] = frame[-1]

            mondict = dict(self.monitor.__dict__)
            indicator_dict = dict(self.indicator.__dict__)
            indicator_dict.pop('monitor')
            self_dict = dict(self.__dict__)
            self_dict.pop('monitor')
            self_dict.pop('indicator')
            self_dict.pop('smooth_func')
            log = {'stimulation': self_dict,
                   'monitor': mondict,
                   'indicator': indicator_dict}
            # end addition
            self.write_seq_to_cache(dirpath,endpath_np_short,log,endpath_seq_short, mov,fmp,endpath_seq,endpath_np)
            return mov, log
