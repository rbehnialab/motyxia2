# Secret key for session management.
import os

SECRET_KEY = 'my precious'
SETTINGS_LIST = [
    'coresettings',
    'aosettings',
    'projectorsettings',
    'monitorsettings',
    'indicatorsettings',
    'backgroundsettings',
    'stimulisettings',
    # 'measurementsettings'
]
NOT_REQUIRED_SETTINGS = [
    'aosettings',
    'indicatorsettings',
    'projectorsettings',
    # 'measurementsettings'
]
AUTOSCRIPT = os.path.join(
    os.path.split(os.path.dirname(__file__))[0], 'autoscript.py')
MEASUREMENTSCRIPT = os.path.join(
    os.path.split(os.path.dirname(__file__))[0], 'measurementscript.py')
TESTSCRIPT = os.path.join(
    os.path.split(os.path.dirname(__file__))[0], 'testscript.py')
BACKGROUNDSCRIPT = os.path.join(
    os.path.split(os.path.dirname(__file__))[0], 'backgroundscript.py')
BEHAVIORSCRIPT = os.path.join(
    os.path.split(os.path.dirname(__file__))[0], 'behaviorscript.py')

if not os.path.exists(AUTOSCRIPT):
    raise NameError(f'autoscript not found: {AUTOSCRIPT}')

if not os.path.exists(MEASUREMENTSCRIPT):
    raise NameError(f'autoscript not found: {MEASUREMENTSCRIPT}')

if not os.path.exists(TESTSCRIPT):
    raise NameError(f'autoscript not found: {TESTSCRIPT}')

if not os.path.exists(BACKGROUNDSCRIPT):
    raise NameError(f'autoscript not found: {BACKGROUNDSCRIPT}')

from .allen.stimuli import LocallySparseNoise, SparseNoise, UniformContrast, \
    FlashingCircle, DriftingGratingCircle, WhiteNoise

from .fullfield.stimuli import Steps, SpectraSteps, SpectraTestSteps

STIMULITYPES = {
    'sparse' : SparseNoise,
    'locallysparse' : LocallySparseNoise,
    'uniform' : UniformContrast,
    'flashingcircle' : FlashingCircle,
    'driftinggrating' : DriftingGratingCircle,
    'whitenoise': WhiteNoise,
    'ff_steps' : Steps,
    'ff_spectra_steps': SpectraSteps,
    'ff_spectra_test_steps': SpectraTestSteps
}
