"""Full-field LED stimuli
"""

from itertools import product
import pandas as pd
import numpy as np
from collections import OrderedDict
import random
from sklearn.metrics.pairwise import euclidean_distances
import matplotlib.pyplot as plt

from ..utils import gaussian, read_json
from ..colorvision.spectrum import Spectrum, Spectra, Filter, Filters


class FfStimulus:
    """
    Base class for full field stimuli.

    Parameters
    ----------
    ao_devices : AoDevices
    pregap_dur, postgap_dur : float (in seconds)
    background : dict (in measurement units)
    iteration : int
    """

    def __init__(
        self, ao_devices,
        background=None,
        pregap_dur=0,
        postgap_dur=0,
        iteration=1,
        background_spectrum=None,
        background_wavelengths=None,
        background_intensity=None,
        ):

        self.bg_spectrum = None
        self.ao_devices = ao_devices
        self.refresh_rate = ao_devices.refresh_rate
        self.pregap_dur = pregap_dur
        self.postgap_dur = postgap_dur
        self.channel_names = ao_devices.channel_order #does not include trigger!
        self.led_spectra = ao_devices.led_spectra

        if isinstance(background_spectrum, str):
            self._background_spectrum = background_spectrum
        else:
            self._background_spectrum = None

        background, bg_intensities = self._get_background_values(
            background,
            background_spectrum,
            background_wavelengths,
            background_intensity
        )

        if not hasattr(self, 'bg_fit'):
            self.bg_fit = None

        self.background = background
        self.iteration = iteration

    def _get_background_values(
        self,
        background,
        background_spectrum,
        background_wavelengths,
        background_intensity
    ):
        """
        """

        if background_spectrum is not None:
            background = self._calculate_background(
                background_spectrum, background_wavelengths,
            )

        bg_intensities = np.array(
            pd.Series(background)[self.channel_names]
        )
        if background_intensity is not None:
            self.bg_intensity = background_intensity
            bg_intensities *= background_intensity/np.sum(bg_intensities)
            background = pd.Series(
                bg_intensities, index=self.channel_names
            ).to_dict()
        else:
            self.bg_intensity = np.sum(bg_intensities)

        return background, bg_intensities

    def generate_volts_by_index(self):
        raise NotImplementedError('generate volts by index')

    def generate_volts(self):
        raise NotImplementedError('generating volts')

    def _iterations(self):
        _mapped_values = self.mapped_values.copy()
        _dataframe = self.dataframe.copy()
        _duration = float(self.duration)

        #iterate over n-1
        for n in range(self.iteration-1):
            _mapped_values['delay'] += _duration
            _mapped_values['end'] += _duration
            _dataframe['delay'] += _duration
            _dataframe['end'] += _duration

            self.duration += _duration
            # TODO change delay and end
            self.mapped_values = self.mapped_values.append(
                _mapped_values, ignore_index=True
            )
            self.dataframe = self.dataframe.append(
                _dataframe, ignore_index=True
            )

    @property
    def name(self):
        return self.__class__.__name__

    def _calculate_background(
        self, background_spectrum,
        background_wavelengths,
    ):
        """calculate intensities for background
        """
        if isinstance(background_spectrum, str):
            if background_spectrum.endswith('.json'):
                return read_json(background_spectrum)

        self.bg_spectrum = Spectrum(
            background_spectrum, background_wavelengths,
            norm_over_wl=True,
        )

        w_bg, res_bg, rank_bg, s_bg = \
            self.led_spectra.fit_spectrum(self.bg_spectrum)

        self.bg_fit = {
            'w_bg': w_bg.tolist(),
            'res_bg': res_bg.tolist(),
            'rank_bg': int(rank_bg),
            's_bg': s_bg.tolist()
        }

        background = pd.Series(w_bg, index=self.channel_names).to_dict()

        return background


class ConstantBackground(FfStimulus):
    """
    Constant background stimulus
    """

    def __init__(
        self, ao_devices,
        duration,
        **kwargs
    ):
        super().__init__(ao_devices, **kwargs)
        self.duration = duration + self.pregap_dur + self.postgap_dur
        self.mapping_names = list(self.background.keys())

        self.dataframe = pd.DataFrame([self.background])
        self.dataframe['delay'] = 0
        self.dataframe['end'] = self.duration

        self._map_values()
        self._iterations()

    def _map_values(self):
        _mapped_bg = self.ao_devices.map(self.background)
        self.mapped_bg = pd.Series(_mapped_bg)[self.mapping_names]
        self.mapped_values = pd.DataFrame([_mapped_bg])[self.mapping_names]
        self.mapped_values['delay'] = self.dataframe['delay']
        self.mapped_values['end'] = self.dataframe['end']

    def generate_volts(self):
        """
        """
        timestamps = np.arange(0, self.duration, 1/self.refresh_rate)
        self.iter_frame_num = timestamps.shape[0]

        #initialize volts
        volts = np.ones((self.iter_frame_num, len(self.mapping_names)))
        volts *= np.array(self.mapped_bg[self.mapping_names])[None, :]

        #order the array correctly
        volts = pd.DataFrame(
            volts, columns=self.mapping_names
            )[self.ao_devices.channel_order].values.astype(np.float64)

        self.dataframe['stimulus_type'] = self.name
        self.dataframe['background_spectrum'] = self._background_spectrum

        volts_log = {
            'dataframe': self.dataframe.to_dict('list'),
            'mapped_values': self.mapped_values[
                self.mapping_names].to_dict('records'),
            'mapped_bg': self.mapped_bg.to_dict(),
            'refresh_rate': self.refresh_rate,
            'pregap_dur': self.pregap_dur,
            'postgap_dur': self.postgap_dur,
            'background': dict(self.background),
            'iteration': self.iteration,
            'duration': self.duration,
            'timestamps': timestamps.tolist(),
            'ao_devices': self.ao_devices.to_dict(),
            'bg_fit': self.bg_fit
        }

        return volts, volts_log


class Steps(FfStimulus):
    """
    Single step stimulus

    Parameters
    ----------
    values_dict : dict of lists
    durations : list
    pause_durations : list
    repeat : int
    randomize : bool
    """

    def __init__(
        self, ao_devices,
        values_dict,
        durations,
        pause_durations,
        repeat=1,
        randomize=True,
        mixing=False,
        **kwargs):
        """
        """
        super().__init__(ao_devices, **kwargs)

        _values_dict = {}
        for mapping_name, values in values_dict.items():
            if values is not None:
                _values_dict[mapping_name] = values
        values_dict = _values_dict

        # for mapping_name, value in self.background.items():
        #     if mapping_name not in values_dict:
        #         values_dict[mapping_name] = [value]
        # assert set(values_dict.keys()) == set(self.background.keys())

        self.values_dict = OrderedDict(values_dict)
        self.durations = durations
        self.pause_durations = pause_durations
        self.randomize = randomize
        self.repeat = repeat
        self.mixing = mixing

        self.mapping_names = list(self.values_dict.keys())

        #background intensities
        bg = []
        for mapping_name in self.mapping_names:
            bg.append(self.background[mapping_name])
        self.bg = tuple(bg)

        self._create_dataframe()
        self._calculate_delay()
        self._map_values()
        self._iterations()

    def _create_dataframe(self):
        """
        """
        values_frame = self._create_values_frame()

        if len(self.durations) == len(self.pause_durations):
            iterable = list(zip(self.durations, self.pause_durations))
        else:
            iterable = list(product(self.durations, self.pause_durations))

        dataframe = pd.DataFrame()
        for n in range(self.repeat):
            for dur, pause in iterable:
                values_frame['dur'] = dur
                values_frame['pause'] = pause
                dataframe = dataframe.append(
                    values_frame, ignore_index=True)

        if self.randomize:
            idcs_random = np.random.permutation(
                np.arange(len(dataframe)))
            dataframe = dataframe.iloc[idcs_random].reset_index(
                ).drop('index', axis=1)
            # columns = dataframe.columns
            # data = list(dataframe)
            # random.shuffle(data)
            # dataframe = pd.DataFrame(data, columns=columns)

        #add non-mentioned mapping names to dataframe
        for mapping_name, value in self.background.items():
            #if mapping_name not in self.mapping_names:
            if mapping_name not in dataframe.columns:
                dataframe[mapping_name] = value
                self.mapping_names.append(mapping_name)

        self.dataframe = dataframe

    def _calculate_delay(self):
        #account for delays
        delays = np.cumsum(
            np.array(self.dataframe[['dur', 'pause']]).sum(1)
        ) + self.pregap_dur
        self.duration = delays[-1] + self.postgap_dur
        delays = np.concatenate([
            [self.pregap_dur], delays[:-1]
        ])
        self.dataframe['delay'] = delays
        self.dataframe['end'] = delays + np.array(self.dataframe['dur'])

    def _map_values(self):
        _mapped_values = self.ao_devices.map(self.dataframe[self.mapping_names].to_dict('list'))
        self.mapped_values = pd.DataFrame(_mapped_values)
        self.mapped_values['delay'] = self.dataframe['delay']
        self.mapped_values['end'] = self.dataframe['end']
        _mapped_bg = self.ao_devices.map(self.background)
        self.mapped_bg = pd.Series(_mapped_bg)[self.mapping_names]

    def generate_volts(self):
        """
        """
        timestamps = np.arange(0, self.duration, 1/self.refresh_rate)
        self.iter_frame_num = timestamps.shape[0]

        #initialize volts
        volts = np.ones((self.iter_frame_num, len(self.mapping_names)))
        volts *= np.array(self.mapped_bg[self.mapping_names])[None, :]

        #change voltages for specified
        for index, row in self.mapped_values.iterrows():
            tbool = (timestamps >= row['delay']) & (timestamps < row['end'])
            volts[tbool] = np.array(row[self.mapping_names])[None, :]

        #order the array correctly
        volts = pd.DataFrame(
            volts, columns=self.mapping_names
            )[self.ao_devices.channel_order].values.astype(np.float64)

        for bg_key, bg_value in self.background.items():
            self.dataframe[f'{bg_key}_background'] = bg_value

        self.dataframe['stimulus_type'] = self.name
        self.dataframe['background_spectrum'] = self._background_spectrum

        volts_log = {
            'dataframe': self.dataframe.to_dict('list'),
            'mapped_values': self.mapped_values[
                self.mapping_names].to_dict('records'),
            'mapped_bg': self.mapped_bg.to_dict(),
            'refresh_rate': self.refresh_rate,
            'pregap_dur': self.pregap_dur,
            'postgap_dur': self.postgap_dur,
            'background': dict(self.background),
            'iteration': self.iteration,
            'duration': self.duration,
            'timestamps': timestamps.tolist(),
            'ao_devices': self.ao_devices.to_dict(),
            'bg_fit': self.bg_fit
        }

        return volts, volts_log

    def _create_values_frame(self):
        """
        """
        if self.mixing:
            values = list(product(*self.values_dict.values()))
        else:
            values = []
            n = 0
            for mapping_name, ivalues in self.values_dict.items():
                _values = np.array([self.bg]*len(ivalues))
                _values[:, n] = np.array(ivalues)
                values.extend(_values.tolist())
                n += 1

        return pd.DataFrame(values, columns=self.mapping_names)


#TODO major mistake with wavelength difference
#OK for now since wavelength difference always 0.5

class SpectraSteps(Steps):
    """create stimuli according to photon capture plane

    Parameters
    ----------
    ao_devices : devices.AoDevices
        the analog output device instance
    single_wavelength_range : tuple or {'max'}
        single wavelength range of interest. If max it will try
        to cover the maximum range.
    number_single_wavelength : int
        number of single wavelengths
    single_wavelength_sigma : float
        the standard deviation for the single wavelength Gaussians.
    number_intersections : int
        number of individual intersections found by combining
        single wavelengths
    wavelength_combinations : int or list of tuples
        single wavelength combinations to test. If int, it will
        find the optimum wavelengths to combine (most informative).
        The optimum is found by calculating pairwise distances in
        photoncapture space.
    combination_steps : int
        number of steps for combinations
    equiluminance_plane : {'linear', 'log', 'isomeasure'}
        Definition of the equiluminance plane.
    background_spectrum: str or numpy.array
        filepath to background spectrum to simulate. The rows should
        correspond to different wavelengths. The First column are the
        wavelength value and the second value are the relative intensity
        values.
    background_intensity : float
        Chosen background intensity (must be in same units as specified in
        ao devices). The sum of photon captures for this intensity will
        specify the equiluminant plane.
    background : dict
        If background spectrum is None, use background as normal
    mix_method : {'constant', 'addition', 'both'}
    kwargs : dict
        dictionary of arguments to pass to Steps instance
    """

    def __init__(
        self,
        ao_devices,
        single_wavelength_range,
        number_single_wavelength,
        single_wavelength_sigma,
        wavelength_combinations,
        combination_steps,
        single_wavelength_method='pdf',
        number_intersections=0,
        luminant_multiples=None,
        equiluminance_plane='linear',
        equiluminance_plane_discard=None,
        background_spectrum=None,
        background_wavelengths=None,
        background_intensity=None,
        combinations_log_distance=False,
        background=None,
        skip_r2_below=None,
        use_minimizer=None,
        mix_method='constant',
        opsin_weights=None,
        cholesky_whitening=False,
        log_fractions=False,
        **kwargs
    ):
        if number_intersections:
            raise NotImplementedError(
                'intersection option.'
            )
        if equiluminance_plane not in ['linear', 'isomeasure', 'log']:
            raise NotImplementedError(
                'only linear equiluminace plane implemented.')
        else:
            self.equiluminance_plane = equiluminance_plane
            self.equiluminance_plane_discard = equiluminance_plane_discard

        self.ao_devices = ao_devices

        if luminant_multiples is None:
            self.luminant_multiples = [1]
        else:
            self.luminant_multiples = luminant_multiples

        self.bg_spectrum = None
        self.opsins = ao_devices.opsins
        self.bg_fit = None
        self.channel_names = ao_devices.channel_order #does not include trigger!
        self.led_spectra = ao_devices.led_spectra
        self.measurement_range = ao_devices.measurement_range
        self.use_minimizer = use_minimizer
        self.mix_method = mix_method
        self.opsin_weights = opsin_weights
        self.log_fractions = log_fractions
        self.cholesky_whitening = cholesky_whitening
        self.single_wavelength_range = single_wavelength_range
        self.single_wavelength_sigma = single_wavelength_sigma
        self.single_wavelength_method = single_wavelength_method
        self.combinations_log_distance = combinations_log_distance

        if self.opsins is None:
            raise ValueError('Must provide opsin data for spectra steps.')
        if self.led_spectra is None:
            raise ValueError('Must provide led spectra data for spectra steps.')

        self.opsins_min = self.opsins.capture(
            self.led_spectra, self.measurement_range[:, 0], True)
        self.opsins_max = self.opsins.capture(
            self.led_spectra, self.measurement_range[:, 1], True)
        self.opsins_range = np.array([self.opsins_min, self.opsins_max]).T

        #should contain all ao mappings
        #but set to None
        #problem if mapping name when setting to None
        #problem for dataframe
        #general purpose fitting
        #should be optimized to mimic background
        background, bg_intensities = self._get_background_values(
            background,
            background_spectrum,
            background_wavelengths,
            background_intensity
        )

        self.bg_q = self.opsins.capture(
            self.led_spectra,
            intensity=bg_intensities, combine=True)

        self.bg_ordered = np.array(
            pd.Series(background)[self.channel_names])

        print('bg q: {}'.format(self.bg_q))

        #find single wavelength simulation intensities
        self.values_frame, self.target_qs = \
            self._find_single_wavelengths_values(
                number_single_wavelength,
            )

        #calculate r2 values for each target q vector
        self.values_frame = self._calculate_residuals(
            self.values_frame,
            self.target_qs)

        #find combined single wavelengths
        if combination_steps > 0:
            combined_wl_values = \
                self._find_combined_wavelengths_values(
                    wavelength_combinations,
                    combination_steps,
                )

            self.values_frame = self.values_frame.append(
                combined_wl_values, ignore_index=True, sort=True
            )

        if 'cdf' in self.single_wavelength_method:
            # does not work with wavelength combinations

            self.values_frame['swl_method'] = 'cdf'
            self.values_frame['ftype'] = 'highpass'
            self.values_frame.loc[
                self.values_frame['single_wls'] < 0, 'ftype'] = 'lowpass'
            self.values_frame['single_wls'] = np.abs(
                self.values_frame['single_wls']
            )
        else:
            self.values_frame['swl_method'] = 'pdf'
            self.values_frame['ftype'] = 'bandpass'


        #skip certain simulations with bad r2 values
        # TODO work with combined
        if skip_r2_below is not None:
            self.values_frame = self.values_frame[
                (self.values_frame['r2_values'] > skip_r2_below)]

        # create values dict as holder
        values_dict = self.values_frame[self.channel_names].to_dict('list')

        #
        super().__init__(
            ao_devices, values_dict,
            background=background,
            **kwargs)

    def _calculate_residuals(self, values_frame, target_qs,
                             message='r2 values for single wavelengths:'):
        """
        """

        #calculate r2 values
        residuals = np.array(values_frame['residuals'].apply(pd.Series))
        actual_qs = target_qs + residuals #look at lnlsq code
        #is this mean target calculation wrong?
        mean_target = np.mean(target_qs, axis=1, keepdims=True)
        sum_total = np.sum((target_qs-mean_target)**2, axis=1)
        sum_res = np.sum(residuals**2, axis=1)
        r2_values = 1 - sum_res/sum_total

        # add r2 values
        values_frame['r2_values'] = r2_values
        values_frame['sum_total'] = sum_total
        values_frame['sum_res'] = sum_res

        print(message)
        print(
            values_frame[
                ['single_wls', 'r2_values', 'mix_method',
                 'luminant_multiple']]
            )

        # add target capture
        for name, target_q, actual_q in zip(
            self.opsins.names, target_qs.T, actual_qs.T):
            values_frame[f'target_q_{name}'] = target_q
            values_frame[f'actual_q_{name}'] = actual_q

        return values_frame

    def _find_efficient_wl_combinations(self, wavelength_combinations):
        """
        """
        if self.combinations_log_distance:
        	qs = np.log(self.simple_equi_qs) * np.asarray(self.opsin_weights)[None, :]

        else:
        	qs = self.simple_equi_qs * np.asarray(self.opsin_weights)[None, :]

        pw_dist = euclidean_distances(
        	qs, qs)

        dist_df = pd.DataFrame(
            pw_dist).stack().sort_values(ascending=False)
        dist_df = dist_df[dist_df != 0]

        elements_used = set()
        sets_used = []
        sets_dict = {
            'new_elements':[],
            'new_sets':[]
        }

        #get correct order for combinations
        for index in dist_df.index:

            if set(index) in sets_used:
                continue

            if elements_used & set(index):
                sets_dict['new_sets'].append(list(index))
            else:
                sets_dict['new_elements'].append(list(index))
                elements_used |= set(index)

            sets_used.append(set(index))

        sets_order = sets_dict['new_elements'] + sets_dict['new_sets']

        wavelength_combinations = [
            tuple(self.wls_to_simulate[set_order])
            for set_order in sets_order
        ][:wavelength_combinations]

        print(f"using wavelength combinations:")
        print(np.array(wavelength_combinations))

        return wavelength_combinations

    def _find_combined_wavelengths_values(
        self,
        wavelength_combinations,
        combination_steps,
    ):
        """
        """
        if wavelength_combinations == 0 or combination_steps == 0:
            return None

        if isinstance(wavelength_combinations, int):
            wavelength_combinations = self._find_efficient_wl_combinations(
                wavelength_combinations)

        #combination steps
        fractions = np.linspace(0, 1, combination_steps+2)[1:-1]
        combined_wl_values = pd.DataFrame()

        for wl1, wl2 in wavelength_combinations:
            sp1 = self.values_frame[
                self.values_frame['single_wls'] == wl1
            ].sort_values(
                ['mix_method', 'luminant_multiple']
            ).reset_index()

            sp2 = self.values_frame[
                self.values_frame['single_wls'] == wl2
            ].sort_values(
                ['mix_method', 'luminant_multiple']
            ).reset_index()

            assert np.all(
                sp1[['mix_method', 'luminant_multiple']]
                == sp2[['mix_method', 'luminant_multiple']]
            )

            target_opsin_names = [
                f'target_q_{name}'
                for name in self.opsins.names
            ]

            for fraction in fractions:
                # TODO implemented the log fraction case

                if self.log_fractions:
                    #need to fit new targets when using log
                    #as it is not a linear system anymore
                    target1 = np.array(sp1[target_opsin_names]) ** fraction
                    target2 = np.array(sp2[target_opsin_names]) ** (1-fraction)
                    target_qs = target1*target2

                    mix_list = sp1['mix_method'].tolist()
                    lum_list = sp2['luminant_multiple'].tolist()

                    combined_wl_results = self._fit_qs(
                        target_qs, mix_list, lum_list, None)

                    _frame = pd.DataFrame([
                        {
                            name: intensity
                            for name, intensity in
                            zip(self.channel_names, result.x)
                        }
                        for result in combined_wl_results
                    ])

                    optimizer_result = self._get_optimizer_frame(
                        combined_wl_results)

                    _frame = pd.concat(
                        [_frame, optimizer_result],
                        axis=1
                    )

                    _frame['mix_method'] = mix_list
                    _frame['luminant_multiple'] = lum_list
                    _frame['single_wls'] = np.nan

                    print(_frame)

                    _frame = self._calculate_residuals(
                        _frame, target_qs,
                        f'r2 values for fitted '
                        f'combined wavelengths ({wl1}, {wl2}):'
                    )

                else:

                    _frame = \
                        sp1[self.channel_names] * fraction \
                        + sp2[self.channel_names] * (1-fraction)

                    for name in self.opsins.names:
                        for itype in ['target', 'actual']:
                            _frame[f'{itype}_q_{name}'] = (
                                sp1[f'{itype}_q_{name}'] * fraction \
                                + sp2[f'{itype}_q_{name}'] * (1-fraction)
                            )

                    _frame['r2_values'] = np.min(
                        [sp1['r2_values'], sp2['r2_values']],
                        axis=0
                    )

                    _frame['mix_method'] = sp1['mix_method']
                    _frame['luminant_multiple'] = sp2['luminant_multiple']

                _frame['spectrum_id'] = [{
                    f'peak{wl1}' : fraction,
                    f'peak{wl2}' : 1-fraction
                }] * len(_frame)

                combined_wl_values = combined_wl_values.append(
                    _frame, ignore_index=True, sort=True
                )

        return combined_wl_values

    def _find_single_wavelengths_values(self, number_single_wavelength):
        """
        """
        if self.single_wavelength_range == 'max':
            raise NotImplementedError('max for single wavelength range')

        elif len(self.single_wavelength_range) > 2:
            wls_to_simulate = np.asarray(self.single_wavelength_range)

        else:
            wls_to_simulate = np.linspace(
                *self.single_wavelength_range, number_single_wavelength)

        avail_wls = np.concatenate([wls_to_simulate, self.opsins.wl])
        wl_max = np.max(avail_wls) + 2 * self.single_wavelength_sigma
        wl_min = np.min(avail_wls) - 2 * self.single_wavelength_sigma
        wl_diff = np.mean(np.diff(self.opsins.wl))

        wls = np.arange(wl_min, wl_max+wl_diff, wl_diff)

        spectra_to_simulate = np.array([
            gaussian(wl, self.single_wavelength_sigma, wls)
            for wl in wls_to_simulate
            ]).T

        #print(np.min(spectra_to_simulate, axis=0), np.max(spectra_to_simulate, axis=0))
        #print(np.sum(spectra_to_simulate, axis=0))

        zero_add = 10**-10

        if self.single_wavelength_method == 'cdf':
            spectra_to_simulate = wl_diff*np.cumsum(spectra_to_simulate, axis=0)
        elif self.single_wavelength_method == 'inverse_cdf':
            spectra_to_simulate = 1-wl_diff*np.cumsum(spectra_to_simulate, axis=0)
            wls_to_simulate = -wls_to_stimulate
        elif self.single_wavelength_method == 'cdf+inverse_cdf':
            spectra_to_simulate1 = wl_diff*np.cumsum(spectra_to_simulate, axis=0)
            spectra_to_simulate2 = 1-spectra_to_simulate1
            spectra_to_simulate = np.hstack([
                spectra_to_simulate1, spectra_to_simulate2])
            print(np.max(spectra_to_simulate))
            wls_to_simulate = np.concatenate([
                wls_to_simulate, -wls_to_simulate])

        # if 'cdf' in self.single_wavelength_method and self.use_minimizer is not None:
        # 	#avoiding zeros
        # 	spectra_to_simulate = np.clip(spectra_to_simulate, 0+zero_add, 1-zero_add)

        #print('here', np.min(spectra_to_simulate), np.max(spectra_to_simulate))
        #print(np.ptp(spectra_to_simulate, axis=0))
        #print(spectra_to_simulate.sum(0))

        self.wls_to_simulate = wls_to_simulate

        names = [{f'peak{wl}':1} for wl in self.wls_to_simulate]

        if 'filter' in self.mix_method:
            assert self.bg_spectrum is not None, "need to supply background spectrum"

            bg_spectrum = Spectrum(
            	self.bg_spectrum.s,
            	self.bg_spectrum.wl,
            	self.bg_intensity
            	)

            #plt.plot(bg_spectrum.wl, bg_spectrum.s)
            #plt.show()

            spectra_to_simulate = bg_spectrum \
                * Filters(spectra_to_simulate, wls)

            spectra_to_simulate.names = names

        else:

            spectra_to_simulate = Spectra(
                spectra_to_simulate, wls,
                intensities=self.bg_intensity, names=names
            )

        # plt.plot(spectra_to_simulate.wl, (spectra_to_simulate.s*spectra_to_simulate.i[None, :]))
        # plt.show()

        equi_qs = self._calculate_qs(spectra_to_simulate)

        self.simple_equi_qs = equi_qs.copy()

        all_qs, lum_list, mix_list, all_wls_to_simulate = \
            self._get_all_qs(equi_qs)

        if np.any(all_qs < 0):
            qbool = np.any(all_qs < 0, axis=1)
            print(np.unique(np.array(lum_list)[qbool]))
            print(np.unique(np.array(mix_list)[qbool]))
            print(np.unique(np.array(all_wls_to_simulate)[qbool]))
            print("all_qs", all_qs)
            raise ValueError("relative photon capture below zero")

        if np.any(all_qs == 0) and self.use_minimizer is not None:
            print('adjusting zero q for fitting to log')
            qbool = all_qs == 0
            min_q = np.min(all_qs[~qbool]) * 10 ** -3
            all_qs[qbool] = min_q

        single_wl_results = self._fit_qs(
            all_qs, mix_list, lum_list, all_wls_to_simulate)

        single_wl_values = self._get_values_frame(single_wl_results, names)

        optimizer_result = self._get_optimizer_frame(single_wl_results)

        single_wl_values = pd.concat(
            [single_wl_values, optimizer_result],
            axis=1
        )

        return single_wl_values, all_qs

    def _fit_qs(
        self, all_qs, mix_list, lum_list, all_wls_to_simulate):
        """
        """
        results = []

        if all_wls_to_simulate is None:
            all_wls_to_simulate = [None] * len(all_qs)

        for q, mix, lum, wl in zip(
            all_qs, mix_list, lum_list, all_wls_to_simulate):

            measurement_range = self.measurement_range.copy()
            if 'addition' in mix:
                if lum > 0:
                    measurement_range[:, 0] = self.bg_ordered
                else:
                    measurement_range[:, 1] = self.bg_ordered

            results.append(
                self.opsins.fit(
                    self.led_spectra, q,
                    spectra_bounds=measurement_range,
                    bg_q=self.bg_q,
                    use_minimizer=self.use_minimizer,
                    weights=self.opsin_weights,
                    cholesky_whitening=self.cholesky_whitening
                )
            )

        return results

    def _get_values_frame(self, results, names):
        """
        """

        values_frame = pd.DataFrame([
            {
                name: intensity
                for name, intensity in
                zip(self.channel_names, result.x)
            }
            for result in results
        ])

        #metadata
        if self.mix_method == 'both':
            values_frame['mix_method'] = np.repeat(
                ['constant', 'addition'], len(values_frame)//2)
            values_frame['spectrum_id'] = \
                names * len(self.luminant_multiples) * 2
            values_frame['luminant_multiple'] = np.repeat(
                self.luminant_multiples, len(names)
            ).tolist() * 2
            values_frame['single_wls'] = \
                self.wls_to_simulate.tolist() * \
                len(self.luminant_multiples) * 2

        else:
            values_frame['spectrum_id'] = names * len(
                self.luminant_multiples)
            values_frame['single_wls'] = \
                self.wls_to_simulate.tolist() * len(self.luminant_multiples)
            values_frame['luminant_multiple'] = np.repeat(
                self.luminant_multiples, len(names)
            ).tolist()
            values_frame['mix_method'] = self.mix_method

        return values_frame

    def _get_optimizer_frame(self, results):
        """
        """
        optimizer_result = []
        for result in results:

            optimizer_result.append([
                result.cost,
                result.fun.tolist(),
                result.optimality,
                result.active_mask.tolist(),
                # single_wl_result.nit,
                result.status,
                result.residuals.tolist()
            ])

        optimizer_result = pd.DataFrame(
            optimizer_result,
            columns=['cost', 'fun', 'optimality',
                     'active_mask', 'status',
                     'residuals'
                     ])

        return optimizer_result

    def _get_all_qs(self, qs):
        """
        """
        def clip(qs, luminant_multiple):
            """clipping negative multiples
            """

            multiple = qs * luminant_multiple

            if luminant_multiple < 0:
                # a tenth of a wiggle room
                multiple_bool = multiple < -1 + luminant_multiple * 0.1

                if np.any(multiple_bool):
                    print('clipping negative multiples below zero qs.')

                return np.clip(multiple, -1, None)

            else:

                return multiple

        all_qs = np.vstack([
            clip(qs, luminant_multiple)
            for luminant_multiple in self.luminant_multiples
        ])
        lum_list = np.repeat(self.luminant_multiples, qs.shape[0]).tolist()

        if self.mix_method == 'both':
            mix_list = np.repeat(
                ['constant', 'addition'], all_qs.shape[0]
            ).tolist()
            all_qs = np.vstack([
                    all_qs,
                    all_qs+np.ones(self.bg_q.size)[None, :]
            ])
            lum_list *= 2
            all_wls_to_simulate = self.wls_to_simulate.tolist() * 2 * len(
                self.luminant_multiples)
        elif 'addition' in self.mix_method:
            mix_list = [self.mix_method] * all_qs.shape[0]
            all_qs += np.ones(self.bg_q.size)[None, :]
            all_wls_to_simulate = self.wls_to_simulate.tolist() * len(
                self.luminant_multiples)
        elif 'constant' in self.mix_method:
            mix_list = [self.mix_method] * all_qs.shape[0]
            all_wls_to_simulate = self.wls_to_simulate.tolist() * len(
                self.luminant_multiples)
        else:
            raise NameError(f'no mix method {self.mix_method}')

        return all_qs, lum_list, mix_list, all_wls_to_simulate

    def _calculate_qs(self, spectra):
        """
        """
        #calculate total photon capture of
        qs = self.opsins.capture(spectra) #Spectrum x opsin
        #photon capture normalized
        equi_qs = qs / self.bg_q[None, :]
        #relative photon capture same as background photon capture
        if self.equiluminance_plane_discard is not None:
            keep = np.isin(
                self.opsins.names,
                self.equiluminance_plane_discard, invert=True)
        else:
            keep = np.ones(self.bg_q.shape).astype(bool)
        #c = (w * q1)/b1 + (w * q2)/b2 + ...
        if self.equiluminance_plane == 'linear':
            # qs /= qs.sum(1, keepdims=True)
            equi_qs = equi_qs / np.mean(
                equi_qs[:, keep], 1, keepdims=True)

        elif self.equiluminance_plane == 'log':
            assert not np.any(equi_qs <= 0), (
                'zeros or smaller in relative captures; cannot use with '
                'equiluminace plane set to "log"'
            )

            #basically calculating the geometric mean here
            equi_qs = (
                # np.log(equi_qs)/
                # np.mean(np.log(equi_qs[:, keep]),
                #         1, keepdims=True)
                equi_qs /
                np.prod(
                    equi_qs[:, keep], 1, keepdims=True
                    ) ** (1/self.bg_q[keep].shape[0])
            )

        return equi_qs

    def _create_values_frame(self):
        """create dataframe for values
        """
        return self.values_frame

    def generate_volts(self):

        volts, volts_log = super().generate_volts()

        volts_log.update({
            'wls_to_simulate' : self.wls_to_simulate.tolist(),
            'single_wavelength_range' : self.single_wavelength_range,
            'single_wavelength_sigma' : self.single_wavelength_sigma,
            'equiluminance_plane' : self.equiluminance_plane,
            'luminant_multiples' : self.luminant_multiples,
        })

        return volts, volts_log

class SpectraTestSteps(SpectraSteps):
    """
    """

    def __init__(
        self,
        ao_devices,
        led_to_test,
        luminant_multiples=None,
        background_spectrum=None,
        background_wavelengths=None,
        background_intensity=None,
        background=None,
        use_minimizer=None,
        mix_method='constant',
        opsin_weights=None,
        cholesky_whitening=False,
        **kwargs
    ):

        self.equiluminance_plane_discard = None
        self.equiluminance_plane = None

        self.ao_devices = ao_devices

        if luminant_multiples is None:
            self.luminant_multiples = [1]
        else:
            self.luminant_multiples = luminant_multiples

        self.bg_spectrum = None
        self.opsins = ao_devices.opsins
        self.bg_fit = None
        self.channel_names = ao_devices.channel_order #does not include trigger!
        self.led_spectra = ao_devices.led_spectra
        self.measurement_range = ao_devices.measurement_range
        self.use_minimizer = use_minimizer
        self.mix_method = mix_method
        self.opsin_weights = opsin_weights
        self.cholesky_whitening = cholesky_whitening
        self.led_to_test = led_to_test

        if self.opsins is None:
            raise ValueError('Must provide opsin data for spectra steps.')
        if self.led_spectra is None:
            raise ValueError('Must provide led spectra data for spectra steps.')

        #should contain all ao mappings
        #but set to None
        #problem if mapping name when setting to None
        #problem for dataframe
        #general purpose fitting
        #should be optimized to mimic background
        background, bg_intensities = self._get_background_values(
            background,
            background_spectrum,
            background_wavelengths,
            background_intensity
        )
        ###

        self.bg_q = self.opsins.capture(
            self.led_spectra,
            intensity=bg_intensities, combine=True)

        self.bg_ordered = np.array(
            pd.Series(background)[self.channel_names])

        print('bg q: {}'.format(self.bg_q))

        self.ch_bool = np.array(self.channel_names) == led_to_test

        # simply set measurement range of led test to null basically
        led_bg = background[led_to_test]

        self.measurement_range[
            self.ch_bool, 0
            ] = led_bg - led_bg * 10 ** -10
        self.measurement_range[
            self.ch_bool, 1
            ] = led_bg + led_bg * 10 ** -10

        self.opsins_min = self.opsins.capture(
            self.led_spectra, self.measurement_range[:, 0], True)
        self.opsins_max = self.opsins.capture(
            self.led_spectra, self.measurement_range[:, 1], True)
        self.opsins_range = np.array([self.opsins_min, self.opsins_max]).T

        #find single wavelength simulation intensities
        self.values_frame, self.target_qs = self._find_single_values()

        #calculate r2 values for each target q vector
        self.values_frame = self._calculate_residuals(
            self.values_frame,
            self.target_qs)

        actual_frame = self._get_actual_led_ints()

        self.values_frame = self.values_frame.append(
            actual_frame, ignore_index=True, sort=True)

        # create values dict as holder
        values_dict = self.values_frame[self.channel_names].to_dict('list')

        #
        Steps.__init__(
            self, ao_devices, values_dict,
            background=background,
            **kwargs)


    def _find_single_values(self):
        """
        """

        spectra_to_simulate = Spectra(
            self.led_spectra.s[:, self.ch_bool],
            self.led_spectra.wl,
            intensities=self.bg_intensity,
            names=[self.led_to_test]
        )

        self.wls_to_simulate = np.array([self.led_to_test])

        names = [{f'{wl}':1} for wl in self.wls_to_simulate]

        self.led_to_test_id = names

        equi_qs = self._calculate_qs(spectra_to_simulate)

        self.simple_equi_qs = equi_qs.copy()

        all_qs, lum_list, mix_list, all_to_simulate = \
            self._get_all_qs(equi_qs)

        if np.any(all_qs < 0):
            qbool = np.any(all_qs < 0, axis=1)
            print(np.unique(np.array(lum_list)[qbool]))
            print(np.unique(np.array(mix_list)[qbool]))
            print(np.unique(np.array(all_to_simulate)[qbool]))
            print("all_qs", all_qs)
            raise ValueError("relative photon capture below zero")

        if np.any(all_qs == 0) and self.use_minimizer is not None:
            print('adjusting zero q for fitting to log')
            qbool = all_qs == 0
            min_q = np.min(all_qs[~qbool]) * 10 ** -3
            all_qs[qbool] = min_q

        single_results = self._fit_qs(
            all_qs, mix_list, lum_list, all_to_simulate)

        single_values = self._get_values_frame(single_results, names)

        optimizer_result = self._get_optimizer_frame(single_results)

        single_values = pd.concat(
            [single_values, optimizer_result],
            axis=1
        )

        return single_values, all_qs

    def _get_actual_led_ints(self):
        """
        """
        intensities = []

        bg_ints = self.bg_ordered.copy()
        nobg_ints = np.zeros(self.bg_ordered.size)

        ch_idx = np.where(self.ch_bool)[0][0]

        ints = np.asarray(self.luminant_multiples) * self.bg_intensity

        bg_ints = np.asarray([bg_ints]*len(ints))

        nobg_ints = np.asarray([nobg_ints]*len(ints))

        bg_ints[:, ch_idx] += ints
        nobg_ints[:, ch_idx] += ints

        if self.mix_method == 'both':

            all_ints = np.vstack([bg_ints, nobg_ints])
            actual_frame = pd.DataFrame(
                all_ints, columns=self.channel_names)
            actual_frame['mix_method'] = np.repeat(
                ['addition', 'constant'],
                len(ints)
            )
            actual_frame['luminant_multiple'] = self.luminant_multiples * 2

        elif 'addition' in self.mix_method:

            all_ints = bg_ints
            actual_frame = pd.DataFrame(
                all_ints, columns=self.channel_names)
            actual_frame['mix_method'] = self.mix_method
            actual_frame['luminant_multiple'] = self.luminant_multiples

        elif 'constant' in self.mix_method:

            all_ints = nobg_ints
            actual_frame = pd.DataFrame(
                all_ints, columns=self.channel_names)
            actual_frame['mix_method'] = self.mix_method
            actual_frame['luminant_multiple'] = self.luminant_multiples

        else:
            raise NameError(f'no mix method {self.mix_method}')

        actual_frame['single_wls'] = self.led_to_test
        actual_frame['spectrum_id'] = self.led_to_test_id \
            * len(actual_frame)

        return actual_frame

    def generate_volts(self):

        volts, volts_log = Steps.generate_volts(self)

        volts_log.update({
            'led_to_test' : self.led_to_test,
            'luminant_multiples' : self.luminant_multiples,
        })

        return volts, volts_log


class CaptureNoise(Steps):
    """create stimuli according to photon capture plane

    Parameters
    ----------
    ao_devices : devices.AoDevices
        the analog output device instance
    """

    def __init__(
        self,
        ao_devices,
        noise_duration=30,
        noise_generator='truncnorm',
        noise_mean=0,
        noise_std=1,
        time_constant=0,
        receptor_model='linear',
        update_rate=None,
        constant_correlation=None,
        background_spectrum=None,
        background_wavelengths=None,
        background_intensity=None,
        background=None,
        skip_r2_below=None,
        use_minimizer=None,
        opsin_weights=None,
        cholesky_whitening=False,
        **kwargs
    ):

        self.ao_devices = ao_devices

        self.bg_spectrum = None
        self.opsins = ao_devices.opsins
        self.bg_fit = None
        self.channel_names = ao_devices.channel_order #does not include trigger!
        self.led_spectra = ao_devices.led_spectra
        self.measurement_range = ao_devices.measurement_range
        self.use_minimizer = use_minimizer
        self.opsin_weights = opsin_weights
        self.noise_duration = noise_duration
        self.update_rate = update_rate
        self.constant_correlation = constant_correlation
        self.cholesky_whitening = cholesky_whitening
        self.noise_generator = noise_generator
        self.noise_mean = noise_mean
        self.noise_std = noise_std
        self.receptor_model = receptor_model
        self.time_constant = time_constant

        if self.opsins is None:
            raise ValueError('Must provide opsin data for spectra steps.')
        if self.led_spectra is None:
            raise ValueError('Must provide led spectra data for spectra steps.')

        #should contain all ao mappings
        #but set to None
        #problem if mapping name when setting to None
        #problem for dataframe
        #general purpose fitting
        #should be optimized to mimic background
        background, bg_intensities = self._get_background_values(
            background,
            background_spectrum,
            background_wavelengths,
            background_intensity
        )

        self.bg_q = self.opsins.capture(
            self.led_spectra,
            intensity=bg_intensities, combine=True)

        self.bg_ordered = np.array(
            pd.Series(background)[self.channel_names])

        print('bg q: {}'.format(self.bg_q))

        # need to calculate relative photon capture
        self.opsins_min = self.opsins.capture(
            self.led_spectra, self.measurement_range[:, 0], True
            )/self.bg_q
        self.opsins_max = self.opsins.capture(
            self.led_spectra, self.measurement_range[:, 1], True
            )/self.bg_q
        self.opsins_range = np.array(
            [self.opsins_min, self.opsins_max]).T

        #find single wavelength simulation intensities
        self.values_frame, self.target_qs = self._get_values_frame()

        #calculate r2 values for each target q vector
        self.values_frame = self._calculate_residuals(
            self.values_frame,
            self.target_qs)

        #skip certain simulations with bad r2 values
        # TODO work with combined
        if skip_r2_below is not None:
            self.values_frame = self.values_frame[
                (self.values_frame['r2_values'] > skip_r2_below)]

        # create values dict as holder
        values_dict = self.values_frame[self.channel_names].to_dict('list')

        #
        super().__init__(
            ao_devices, values_dict,
            background=background,
            randomize=False,
            pause_durations=[0],
            durations=[1/self.update_rate],
            repeat=1,
            **kwargs)

    def _get_values_frame(self):
        """
        """

    def _calculate_residuals(
        self, values_frame, target_qs,
        message='r2 value for noisy stimulus:'):
        """
        """

        #calculate r2 values
        residuals = np.array(values_frame['residuals'].apply(pd.Series))
        actual_qs = target_qs + residuals #look at lnlsq code
        mean_target = np.mean(target_qs, axis=0, keepdims=True)
        sum_total = np.sum((target_qs-mean_target)**2, axis=1)
        sum_res = np.sum(residuals**2, axis=1)
        r2_values = 1 - sum_res/sum_total

        # add r2 values
        values_frame['r2_values'] = r2_values
        values_frame['sum_total'] = sum_total
        values_frame['sum_res'] = sum_res

        print(message)
        print(values_frame['r2_values'].describe())

        # add target capture
        for name, target_q, actual_q in zip(
            self.opsins.names, target_qs.T, actual_qs.T):
            values_frame[f'target_q_{name}'] = target_q
            values_frame[f'actual_q_{name}'] = actual_q

        return values_frame

    def _create_values_frame(self):
        """create dataframe for values
        """
        return self.values_frame

    def generate_volts(self):

        volts, volts_log = super().generate_volts()

        volts_log.update({
            'update_rate': self.update_rate,
            'noise_duration': self.noise_duration,
            'constant_correlation': self.constant_correlation,
            'noise_generator': self.noise_generator,
            'noise_mean': self.noise_mean,
            'noise_std': self.noise_std
        })

        return volts, volts_log
