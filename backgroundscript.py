"""
"""

import os
import json
import argparse

from motyxia.utils import read_json
from motyxia.projector import Window
from motyxia.stimuli_combiner import StimuliCombiner
from motyxia.animate import BackgroundAnimator

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-l", "--location", help="location of basedir", type=str)

    args = parser.parse_args()

    config = read_json(args.location)

    window = Window(
        monitor_params=config['monitorsettings'],
        indicator_params=config['indicatorsettings'],
        background_params=config['backgroundsettings'],
        **config['projectorsettings'],
        **config['coresettings'],
        **config['aosettings']
    )

    animator = BackgroundAnimator(
        window
    )

    animator.run_background()

    #TODO disconnect failing
