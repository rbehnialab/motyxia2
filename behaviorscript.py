"""
"""
import argparse
import subprocess as sp
import psutil
import pandas as pd
import numpy as np
import os

from motyxia.utils import read_json
from motyxia.projector import Window
from motyxia.stimuli_combiner import StimuliCombiner
from motyxia.fullfield.stimuli import Steps

from motyxia.utils import write_json, yn_choice

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-l", "--location", help="location of basedir", type=str)

    args = parser.parse_args()

    config = read_json(args.location)
    
    if isinstance(config['stimulisettings']['ff_steps'], list):
        
        # default values as not needed for behavior
        config['stimulisettings']['ff_steps'][0]['durations'] = [1]
        config['stimulisettings']['ff_steps'][0]['pause_durations'] = [1]
        config['stimulisettings']['ff_steps'][0]['pregap_dur'] = 1
        config['stimulisettings']['ff_steps'][0]['postgap_dur'] = 1
        config['stimulisettings']['ff_steps'][0]['iteration'] = 1
        
        # fetch opposing led
        opposing_dict = config['stimulisettings']['ff_steps'][0]['opposing_dict']
        
        del config['stimulisettings']['ff_steps'][0]['opposing_dict']
       
        window = Window(
            monitor_params=config['monitorsettings'],
            indicator_params=config['indicatorsettings'],
            background_params=config['backgroundsettings'],
            **config['projectorsettings'],
            **config['coresettings'],
            **config['aosettings']
        )
    
        stimuli_combiner = StimuliCombiner(
            window=window,
            **config['stimulisettings']
        )
        
        lambda1 = Steps(
            window.ao_devices,
            opposing_dict,
            durations=[1],
            pause_durations=[1],
            background=config['stimulisettings']['ff_steps'][0]['background']
        )
        
        opposingLED = pd.DataFrame(lambda1.generate_volts()[1]['mapped_values'])
        
        pwm_values = pd.DataFrame(stimuli_combiner.ao_seq_log['mapped_values']) #We have to convert that to a cvs
        pwm_values = pd.concat([opposingLED, pwm_values],axis = 0)
        bg_values = pd.DataFrame(stimuli_combiner.ao_seq_log['mapped_bg'])
          
    if isinstance(config['stimulisettings']['ff_spectra_steps'],list):
        
        window = Window(
            monitor_params=config['monitorsettings'],
            indicator_params=config['indicatorsettings'],
            background_params=config['backgroundsettings'],
            **config['projectorsettings'],
            **config['coresettings'],
            **config['aosettings']
        )
    
        stimuli_combiner = StimuliCombiner(
            window=window,
            **config['stimulisettings']
        )
        
        pwm_values = pd.DataFrame(stimuli_combiner.ao_seq_log['mapped_values'])
        bg_values = pd.DataFrame(stimuli_combiner.ao_seq_log['mapped_bg'])
        n_wls = config['stimulisettings']['ff_spectra_steps'][0][ 'number_single_wavelength']
        pwm_values = pwm_values.drop([n_wls*i for i in range(1,len(pwm_values)//n_wls)])
        if 'background':
            background = bg_values * config['stimulisettings']['ff_spctra_steps'][0]['luminant_multiples']
            pwm_values = pd.concat([pwm_values,background],axis = 0)
    
    
    
    
    pwm_values = pwm_values[['UV','green','red','yellow','purple','blue']]
    bg_values = bg_values[['UV','green','red','yellow','purple','blue']]
        
    pwm_values = np.round(pwm_values).astype(int) #the pwm values must be integer
    bg_values = np.round(bg_values)
        
    pwm_values.to_csv("C:\\Users\\rbehnialab\\Documents\\MATLAB\\behavior\\experiments\\led_ymaze_custom\\pwmValues.csv",index = False)
    bg_values.to_csv("C:\\Users\\rbehnialab\\Documents\\MATLAB\\behavior\\experiments\\led_ymaze_custom\\bgValues.csv",index = False)
       
    subject_name = config['recordingsettings']['subject_name']
    experimenter_name = config['recordingsettings']['experimenter_name']
    recording_id = config['recordingsettings']['recording_id']
    
#    log_dir = os.path.join(config['basedir'], 'logs')
#    
#    log_save_loc = os.path.join(log_dir,
#            experimenter_name,
#            subject_name,
#            'recording_' + str(recording_id)
#        )
#    write_json(os.path.join(log_save_loc, 'config.json'), config)
    
    
    print('The values of the first arm will be: \n')
    print(pwm_values.iloc[:1,:])
    print('\n')
    print('The values of the second arm will be: \n')
    print(pwm_values.iloc[1:,:])
    print('The values of the background will be: \n')
    print(bg_values)
    
    response = yn_choice("Is this correct?")
    
#TODO add a tick box to know what kind of experiment we are in    
    
    if response:  
    
        #killing the existing matlab application running
        PROCNAME = 'MATLAB.exe'
        for proc in psutil.process_iter():
          try:
              if proc.name() == PROCNAME:
                  proc.kill()
          except:
            pass
    
        #Launching margo
        command =''' matlab  -r "cd(fullfile('C:\\Users\\rbehnialab\\Documents\\MATLAB\\behavior')), margo" ''' 
        sh=sp.Popen(command, stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
    
    
    
        print(f'Initiating {config["recordingsettings"]}...')
    
        print(f'Stimulus complete for {config["recordingsettings"]}.')
    
        #TODO disconnect failing
        
    else:
        raise Exception('cancelled experiment')
        print("Cancelling the experiment")
    