import os
import numpy as np
from flask import render_template, request, flash, url_for, redirect

from . import SETTINGS_LIST, AUTOSCRIPT, STIMULITYPES, \
    MEASUREMENTSCRIPT, TESTSCRIPT, BACKGROUNDSCRIPT, BEHAVIORSCRIPT
from .app import app
from .forms import RecordingForm, \
    stimuli_form_dynamic, background_form_dynamic, \
    monitor_form_dynamic, indicator_form_dynamic, \
    core_form_dynamic, projector_form_dynamic, ao_form_dynamic, \
    measurement_form_dynamic, BehaviorForm
from .utils import write_json, read_json, json2table
from .allen.monitor import Monitor
import subprocess

#TODO View for running baseline background intensity

class Animation:
    """container class
    """
    running = False #if animation is running
    p = None #subprocess instance
    names = {
        settings_name.replace('settings', ''): None
        for settings_name in SETTINGS_LIST}
    class_mapping = {
        "monitor": Monitor
    }
    monitor = None
    recording_kwargs = {}
    measurement_kwargs = {}


@app.route('/')
def home():
    return render_template('pages/home.html')


@app.route('/about')
def about():
    return render_template('pages/about.html')


@app.route('/core', methods=['GET', 'POST'])
def core():
    return form_handling('core', core_form_dynamic)


@app.route('/ao', methods=['GET', 'POST'])
def ao():
    return form_handling('ao', ao_form_dynamic)


@app.route('/projector', methods=['GET', 'POST'])
def projector():
    return form_handling('projector', projector_form_dynamic)


@app.route('/monitor', methods=['GET', 'POST'])
def monitor():
    return form_handling('monitor', monitor_form_dynamic)


@app.route('/indicator', methods=['GET', 'POST'])
def indicator():
    return form_handling('indicator', indicator_form_dynamic)


@app.route('/background', methods=['GET', 'POST'])
def background():
    return form_handling('background', background_form_dynamic)


@app.route('/stimuli', methods=['GET', 'POST'])
def stimuli():
    if Animation.monitor is not None:
        flash_message_1 = (
            "min&max alt/azi (deg): {:10.2f} to"
            " {:10.2f}, {:10.2f} to {:10.2f}"
        ).format(
            Animation.monitor.deg_coord_y.min(),
            Animation.monitor.deg_coord_y.max(),
            Animation.monitor.deg_coord_x.min(),
            Animation.monitor.deg_coord_x.max()
        )
        flash_message_2 = (
            "min&max x/y (cm): {:10.2f} to {:10.2f},"
            " {:10.2f} to {:10.2f}"
        ).format(
            Animation.monitor.lin_coord_x.min(),
            Animation.monitor.lin_coord_x.max(),
            Animation.monitor.lin_coord_y.min(),
            Animation.monitor.lin_coord_y.max()
        )
        flash_messages = [flash_message_1, flash_message_2]

    else:
        flash_messages = []

    return form_handling(
        'stimuli', stimuli_form_dynamic, *flash_messages)


@app.route('/delete/', methods=['GET', 'POST'])
@app.route('/delete/<formtype>', methods=['GET', 'POST'])
def delete(formtype):
    name = request.args.get('name', None)
    target = url_for(f'{formtype}')

    if request.method == 'POST':

        submit = request.form.get('submit', None)

        if submit == 'Delete':

            filepath = app.config[f'{formtype}settings_file']
            json_data = read_json(filepath)
            json_data.pop(name, None)
            write_json(filepath, json_data)
            flash('Entry successfully deleted.')

        return redirect(target)

    return render_template(
        'pages/delete.html', formtype=formtype, name=name)

def _check_running():
    """check if process is running
    """
    if Animation.p is not None:
        if Animation.p.poll() is not None:
            Animation.running = False

            if Animation.p.returncode == 0:
                flash("Process complete")
                Animation.p = None
            else:
                flash(f"Status: FAIL - {Animation.p.returncode}", "error")

        else:
            Animation.running = True
            flash('Process still running.')

def _get_settings_complete(settings_list):
    """
    """
    settings_complete = {}

    for settings_name in settings_list:
        key = settings_name.replace('settings', '')
        if app.config[settings_name] is None:
            settings_complete[key] = False
        else:
            settings_complete[key] = True

    return settings_complete

@app.route('/measurement', methods=['GET', 'POST'])
def measurement():
    form = measurement_form_dynamic(
        request.form, app_config=app.config,
        **Animation.measurement_kwargs)

    settings_complete = _get_settings_complete(
        ['aosettings', 'coresettings']
    )

    _check_running()

    if request.method == 'POST':
        submit = request.form.get('submit', None)

        if form.validate_on_submit():
            formatted_dict = form.get_formatted()
            Animation.measurement_kwargs = formatted_dict

            if submit == 'Run' \
                and not Animation.running \
                and all(settings_complete.values()):

                #sanity check
                if Animation.p is not None:
                    Animation.p.terminate()

                Animation.running = True

                formatted_dict = form.get_formatted()

                config = {}
                config['measurementsettings'] = formatted_dict
                config['aosettings'] = app.config['aosettings']

                config_filepath = os.path.join(
                    app.config['basedir'], 'temp_config.json')
                write_json(config_filepath, config)

                p = subprocess.Popen(
                    ["python", f"{MEASUREMENTSCRIPT}",
                     "--location", f"{config_filepath}"],
                    shell=False
                )
                Animation.p = p

            elif submit == 'Abort':
                if Animation.p is not None:
                    Animation.p.terminate()
                    flash('Aborting measurement...', 'error')

                else:
                    flash('No measurement is running.')

            else:
                flash('Process running.')

    return render_template(
        'pages/measure.html',
        settings_complete=settings_complete,
        names=Animation.names,
        form=form
        )



@app.route('/experiment', methods=['GET', 'POST'])
def experiment():
    form = RecordingForm(request.form, **Animation.recording_kwargs)

    settings_complete = _get_settings_complete(SETTINGS_LIST)

    _check_running()

    if request.method == 'POST':
        submit = request.form.get('submit', None)

        if form.validate_on_submit():
            formatted_dict = form.get_formatted()
            Animation.recording_kwargs = formatted_dict

            if submit in ['Run', 'Test', 'Background'] \
                and not Animation.running \
                and all(settings_complete.values()):

                #sanity check
                if Animation.p is not None:
                    Animation.p.terminate()

                Animation.running = True

                formatted_dict = form.get_formatted()

                config = {}
                config['recordingsettings'] = formatted_dict
                config['stimulisettings'] = app.config['stimulisettings']
                config['indicatorsettings'] = app.config['indicatorsettings']
                config['projectorsettings'] = app.config['projectorsettings']
                config['monitorsettings'] = app.config['monitorsettings']
                config['backgroundsettings'] = app.config['backgroundsettings']
                config['aosettings'] = app.config['aosettings']
                config['coresettings'] = app.config['coresettings']
                config['basedir'] = app.config['basedir']

                config_filepath = os.path.join(
                    app.config['basedir'], 'temp_config.json')
                write_json(config_filepath, config)

                if submit == 'Run':

                    p = subprocess.Popen(
                        ["python", f"{AUTOSCRIPT}",
                         "--location", f"{config_filepath}"],
                        shell=False
                    )

                elif submit == 'Background':

                    p = subprocess.Popen(
                        ["python", f"{BACKGROUNDSCRIPT}",
                         "--location", f"{config_filepath}"],
                        shell=False
                    )

                else:

                    p = subprocess.Popen(
                        ["python", f"{TESTSCRIPT}",
                         "--location", f"{config_filepath}"],
                        shell=False
                    )

                Animation.p = p

            elif submit == 'Abort':
                if Animation.p is not None:
                    Animation.p.terminate()
                    flash('Aborting stimulus presentation...', 'error')

                else:
                    flash('No animation is running.')

            else:
                flash('Data incomplete or animator running.')

    return render_template(
        'pages/experiment.html', form=form,
        settings_complete=settings_complete,
        names=Animation.names
        )
    
@app.route('/behavior', methods=['GET', 'POST'])
def behavior():
    form = BehaviorForm(request.form, **Animation.recording_kwargs)
    
    settings_complete =     settings_complete = _get_settings_complete(
        ['aosettings', 'coresettings','backgroundsettings','stimulisettings']
    )

    _check_running()

    if request.method == 'POST':
        submit = request.form.get('submit', None)

        if form.validate_on_submit():
            formatted_dict = form.get_formatted()
            Animation.recording_kwargs = formatted_dict

            if submit in ['Run', 'Test', 'Background'] \
                and not Animation.running \
                and all(settings_complete.values()):

                #sanity check
                if Animation.p is not None:
                    Animation.p.terminate()

                Animation.running = True

                formatted_dict = form.get_formatted()

                config = {}
                config['recordingsettings'] = formatted_dict
                config['stimulisettings'] = app.config['stimulisettings']
                config['indicatorsettings'] = app.config['indicatorsettings']
                config['projectorsettings'] = app.config['projectorsettings']
                config['monitorsettings'] = app.config['monitorsettings']
                config['backgroundsettings'] = app.config['backgroundsettings']
                config['aosettings'] = app.config['aosettings']
                config['coresettings'] = app.config['coresettings']
                config['basedir'] = app.config['basedir']

                config_filepath = os.path.join(
                    app.config['basedir'], 'temp_config.json')
                write_json(config_filepath, config)

                if submit == 'Run':

                    p = subprocess.Popen(
                        ["python", f"{BEHAVIORSCRIPT}",
                         "--location", f"{config_filepath}"],
                        shell=False
                    )

                elif submit == 'Background':

                    p = subprocess.Popen(
                        ["python", f"{BACKGROUNDSCRIPT}",
                         "--location", f"{config_filepath}"],
                        shell=False
                    )

                else:

                    p = subprocess.Popen(
                        ["python", f"{TESTSCRIPT}",
                         "--location", f"{config_filepath}"],
                        shell=False
                    )

                Animation.p = p

            elif submit == 'Abort':
                if Animation.p is not None:
                    Animation.p.terminate()
                    flash('Aborting stimulus presentation...', 'error')

                else:
                    flash('No animation is running.')

            else:
                flash('Data incomplete or animator running.')

    return render_template(
        'pages/experiment.html', form=form,
        settings_complete=settings_complete,
        names=Animation.names
        )
    



def form_handling(formtype, formclass, *flash_messages):
    """For handling forms
    """
    for flash_message in flash_messages:
        flash(f"{flash_message}")

    form = formclass(request.form, app_config=app.config)

    name = request.args.get('name', None)

    json_data = read_json(app.config[f'{formtype}settings_file'])
    setup_url = url_for(f'{formtype}')
    delete_url = url_for(f'delete', formtype=formtype)
    edit_url = url_for(f'{formtype}')
    data = json2table(
        json_data, setup_url=setup_url, delete_url=delete_url,
        edit_url=edit_url)

    if request.method == 'POST':
        submit = request.form.get('submit', None)

        form.rm_hidden_entries()

        if submit is None:
            pass

        elif submit in ['Save', 'Overwrite', 'Setup']:

            if form.validate_on_submit():
                formatted_dict, name = form.get_formatted()

                if name in json_data and submit == 'Save':
                    flash("Name already exists!")
                elif submit == 'Setup':
                    app.config[f'{formtype}settings'] = formatted_dict
                    Animation.names[f'{formtype}'] = name

                    if formtype in Animation.class_mapping:
                        setattr(
                            Animation,
                            formtype,
                            Animation.class_mapping[formtype](
                                **formatted_dict))

                    flash(f"{formtype} set and ready.")
                else:
                    json_data[name] = formatted_dict
                    write_json(
                        app.config[f'{formtype}settings_file'], json_data
                    )
                    flash("New data has been entered.")
            else:
                flash("Data incomplete.")

        else:
            raise NotImplementedError(f'submit type: {submit}')

        form.append_hidden_entries()

    elif name is not None:

        formatted_dict = json_data.get(name, None)
        edit = eval(request.args.get('edit', "False"))

        if formatted_dict is None:
            flash(f"Name {name} not found in {formtype} saved table.")

        elif edit:
            formatted_dict['name'] = name
            form.populate_form(formatted_dict)

        else:
            Animation.names[f'{formtype}'] = name
            app.config[f'{formtype}settings'] = formatted_dict

            if formtype in Animation.class_mapping:
                setattr(
                    Animation,
                    formtype,
                    Animation.class_mapping[formtype](
                        **formatted_dict))

            flash(f"{formtype} set and ready.")

    toggle_off_keys = []
    if data != "None":
        for key in STIMULITYPES.keys():
            if key in data['head']:
                key_idx = np.where(np.array(data['head']) == key)[0][0]
                toggle_off_keys.append(key_idx)

    return render_template(
        f'pages/form.html', form=form, data=data, formtype=formtype,
        toggle_off_keys=toggle_off_keys
        )

# def test_stimulus(config):
#
#     from .projectors import Window
#     from .stimuli_combiner import StimuliCombiner
#
#     window = Window(
#         monitor_params=config['monitorsettings'],
#         indicator_params=config['indicatorsettings'],
#         background_params=config['backgroundsettings'],
#         **config['projectorsettings'],
#         **config['coresettings'],
#         **config['aosettings']
#     )
#
#     stimuli_combiner = StimuliCombiner(
#         window=window,
#         **config['stimulisettings']
#     )
#
#     stimuli_combiner.build_stimulus()
