"""Abstract classes
"""


class Params:
    """
    Abstract Mixin class for params attribute
    """

    def __getitem__(self, key):
        return self.params[key]

    def __setitem__(self, key, item):
        self.params[key] = item

    def __getattr__(self, name):
        try:
            return super().__getattr__(name)
        except AttributeError:
            return self.params[name]
