"""Animator Class
"""

import os
import sys
import subprocess
import time
from tqdm import trange
import numpy as np
import pickle
import warnings

from . import SETTINGS_LIST
from .utils import write_json


class BackgroundAnimator:
    """class to animate background
    """

    def __init__(
        self, window
    ):
        self.window = window

    def run_background(self, run_projectors=True):
        """run background
        """
        if self.window.has_ao:
            self.window.ao_devices.send_values(
                self.window.ao_background
            )

        if self.window.has_projectors and run_projectors:
            warnings.warn(
                'setting up background for projectors not implemented')


class Animator(BackgroundAnimator):
    """class to animate list of stimuli in window
    """

    def __init__(
        self, experimenter_name,
        subject_name, recording_id,
        window, stimuli_combiner,
        verbose=True, capture=False,
        config=None, subrecording_id=None,
    ):
        if config is None:
            from .app import app
            config = app.config

        self.config = config

        log_dir = os.path.join(
            config['basedir'], 'logs')
        capture_dir = os.path.join(
            config['basedir'], 'captures')

        self.experimenter_name = experimenter_name
        self.subject_name = subject_name
        self.capture = False #TODO
        self.recording_id = recording_id
        self.subrecording_id = subrecording_id
        self.window = window
        self.stimuli_combiner = stimuli_combiner
        self.log_dir = log_dir
        self.verbose = verbose
        self.capture_dir = capture_dir

        #add directories
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        if not os.path.exists(capture_dir):
            os.makedirs(capture_dir)

        self.cap_save_loc = os.path.join(
            self.capture_dir,
            experimenter_name,
            subject_name,
            'recording_' + str(recording_id)
        )
        self.log_save_loc = os.path.join(
            self.log_dir,
            experimenter_name,
            subject_name,
            'recording_' + str(recording_id)
        )

        if subrecording_id is not None:
            self.cap_save_loc = os.path.join(
                self.cap_save_loc, 'subrecording_id' + str(subrecording_id)
            )
            self.log_save_loc = os.path.join(
                self.log_save_loc, 'subrecording_id' + str(subrecording_id)
            )

        self._check_paths()
        if not os.path.exists(self.log_save_loc):
            os.makedirs(self.log_save_loc)
        if not os.path.exists(self.cap_save_loc) and capture:
            os.makedirs(self.cap_save_loc)

        self.running = False

    def _check_paths(self):
        if os.path.exists(self.log_save_loc):

            if len(os.listdir(self.log_save_loc)):

                raise NameError('recording already exists.')

    def run(self):
        """Create Stimuli and run program
        """
        self.running = True
        self.window.make_wins()
        self.stimuli_combiner.build_stimulus()
        self.ts_start = []
        self.ts_end = []
        self.frames_drawn = []

        #self.window.trigger_frequency
        self.num_frames = self.stimuli_combiner.iter_frame_num

        self.window.framepacking()

        for w in self.window.wins:
            w.recordFrameIntervals = True
            w.frameIntervals = []

        self.start_time = time.clock()
        self.window.projectors.enable_projectors()
        proj_color_change = self.stimuli_combiner.proj_color_change

        if (self.window.trigger_frequency > 0) and self.window.has_u3:
            trigger_frames = int(
                self.window.refresh_rate / self.window.trigger_frequency)
        else:
            trigger_frames = None

        self.window.projectors.enable_color(self.window.color_mode)
        self.window.send_trigger()
        now = time.clock()
        dt = 1/self.window.refresh_rate
        self.trigger_time = now - self.start_time

        #need to create task after sending trigger
        self.window.create_task()

        print('refresh rate:', self.window.refresh_rate)

        for frame in trange(self.num_frames):

            if not self.running:
                break

            if trigger_frames is not None:
                if (frame % trigger_frames) == 0:
                    self.window.send_trigger()

            self.frames_drawn.append(frame)
            self.ts_start.append(time.clock() - self.start_time)

            self.stimuli_combiner.animate(frame)

            #
            if self.window.has_projectors:
                self.window.projectors.color_mode_change(
                    proj_color_change, frame)

                if self.capture:
                    self._save_image(frame)
                else:
                    self.window.flip()
            else:
                #TODO test if this gives the right timing
                while dt > time.clock() - now:
                    pass
                #raise NotImplementedError('timing for analog output only')

            now = time.clock()
            self.ts_end.append(now - self.start_time)

        cutoff = self.window.get_frame_cutoff()

        self.total_time = time.clock() - self.start_time

        self.dropped = 0
        for w in self.window.wins:
            w.recordFrameIntervals = False
            f = np.array(w.frameIntervals)
            dropped = (f > cutoff).sum()
            self.dropped = np.max([dropped, self.dropped])

        try:
            self.window.clear_windows()
            self.window.projectors.disable_projectors()

        except AttributeError:
            pass

        if self.verbose:
            print(self._create_statement())

        self.log()
        print('Experiment complete.')

        if self.capture:
            self.save_movie()

        self.running = False

        self.window.close_wins()
        self.window.projectors.disconnect()

        return self

    def _get_summary(self):
        frames_displayed = len(self.frames_drawn)
        stim_no = len(self.stimuli_combiner.stimuli_list)
        average_fps = frames_displayed/self.total_time

        self.summary_dict = dict(
            frames_displayed=int(frames_displayed),
            total_frames=int(self.num_frames),
            stim_no=stim_no,
            average_fps=float(average_fps),
            dropped=int(self.dropped),
            total_time=float(self.total_time),
            trigger_time=float(self.trigger_time)
        )

        return self.summary_dict

    def _create_statement(self):
        self.summary_dict = self._get_summary()
        return """
        {} stim(s) generated.
        {}/{} frames displayed.
        Average fps: {} Hz.
        {} frames missed.
        Elapsed time: {} seconds.
        """.format(
            self.summary_dict['stim_no'],
            self.summary_dict['frames_displayed'],
            self.summary_dict['total_frames'],
            np.round(self.summary_dict['average_fps'], 2),
            self.dropped, np.round(self.total_time, 3)
        )

    def _save_image(self, frame):
        filename = os.path.join(
            self.cap_save_loc,
            f'capture_{str(frame + 1).zfill(5)}.png')
        for w in self.window.wins:
            img = w._getRegionOfFrame(buffer='back')
            img.save(filename, 'PNG')
            sys.stdout.write('\r')
            sys.stdout.write(
                str(int(frame / float(self.num_frames) * 100) +1) + '%')
            sys.stdout.flush()
            w.clearBuffer()

    def save_movie(self):
        """
        Saves movies to file from saved pngs

        :param current_time: current time
        :param save_loc: save location
        """
        fps = self.window.refresh_rate

        save_name = 'capture_video.mpg'
        save_name_gray = 'capture_video_gray.mpg'

        args = ['ffmpeg',
                '-f', 'image2',
                '-framerate', str(fps),
                '-i', os.path.join(self.cap_save_loc, 'capture_%05d.png'),
                '-b:v', '20M',
                os.path.join(self.cap_save_loc, save_name)]

        # make movie using ffmpeg
        print('ffmpeg...')

        process = subprocess.Popen(args,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        print(stdout, stderr)

        args = ['ffmpeg',
                 '-i', os.path.join(self.cap_save_loc, save_name),
                 '-vf', 'format=gray',
                 '-qscale', '0',
                 os.path.join(self.cap_save_loc, save_name_gray)]

        print('\ngrayscale conversion...')

        process = subprocess.Popen(args,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        print(stdout, stderr)

        print('Saved in: {}'.format(self.cap_save_loc))
        print('\nDONE')

    def log(self):
        self._create_txtfile()
        self._create_summaryfile()
        self._create_jsonfile()
        self._create_timestampsfile()
        self._create_allenfiles()
        self._create_aofiles()

    @staticmethod
    def _get_filename(name, endswith):
        return f'{name}.{endswith}'

    def _create_timestampsfile(self):
        path = self.log_save_loc
        filename = self._get_filename('timestamps', 'npy')
        timestamps = np.array([self.frames_drawn, self.ts_start, self.ts_end]).T
        np.save(os.path.join(path, filename), timestamps)

    def _create_txtfile(self):
        filename = self._get_filename('logtext', 'txt')
        with open(os.path.join(self.log_save_loc, filename), 'w') as f:
            f.write(self._create_statement())

    def _create_summaryfile(self):
        filename = self._get_filename('logsummary', 'json')
        summary_dict = self._get_summary()
        summary_dict['dual'] = self.window.dual
        summary_dict['system'] = self.window.system
        write_json(os.path.join(self.log_save_loc, filename), summary_dict)

    def _create_jsonfile(self):
        filename = self._get_filename('params', 'json')
        json_dict = {}
        for settings_name in SETTINGS_LIST:
            json_dict[settings_name] = self.config[settings_name]
        write_json(os.path.join(self.log_save_loc, filename), json_dict)

    def _create_aofiles(self):

        if not self.window.has_ao:
            pass

        else:

            filename = self._get_filename('ao_log', 'json')
            write_json(
                os.path.join(self.log_save_loc, filename),
                self.stimuli_combiner.ao_seq_log
            )

            filename = self._get_filename('ao_sequence', 'npy')
            np.save(
                os.path.join(self.log_save_loc, filename),
                self.stimuli_combiner.ao_sequence
            )

    def _create_allenfiles(self):

        if not self.window.has_projectors:
            pass

        elif self.window.dual:

            filename = self._get_filename('allen_uv', 'pkl')
            with open(os.path.join(self.log_save_loc, filename), 'wb') as f:
                pickle.dump(self.stimuli_combiner.seq_log_uv, f)

            filename = self._get_filename('allen_uv', 'npy')
            np.save(os.path.join(self.log_save_loc, filename),
                    self.stimuli_combiner.sequence_uv)

            filename = self._get_filename('allen_rgb', 'pkl')
            with open(os.path.join(self.log_save_loc, filename), 'wb') as f:
                pickle.dump(self.stimuli_combiner.seq_log_rgb, f)

            filename = self._get_filename('allen_rgb', 'npy')
            np.save(os.path.join(self.log_save_loc, filename),
                    self.stimuli_combiner.sequence_rgb)

        else:

            filename = self._get_filename('allen', 'pkl')
            with open(os.path.join(self.log_save_loc, filename), 'wb') as f:
                pickle.dump(self.stimuli_combiner.seq_log, f)

            filename = self._get_filename('allen', 'npy')
            np.save(os.path.join(self.log_save_loc, filename),
                    self.stimuli_combiner.sequence)
