"""Full-field LED stimuli
"""
from .spectrasteps import SpectraSteps
from itertools import product
import pandas as pd
import numpy as np
from collections import OrderedDict
import random
from sklearn.metrics.pairwise import euclidean_distances
import matplotlib.pyplot as plt

from ...utils import gaussian, read_json
from ...colorvision.spectrum import Spectrum, Spectra, Filter, Filters
class SpectraTestSteps(SpectraSteps):
    """
    """

    def __init__(
        self,
        ao_devices,
        led_to_test,
        luminant_multiples=None,
        background_spectrum=None,
        background_wavelengths=None,
        background_intensity=None,
        background=None,
        use_minimizer=None,
        mix_method='constant',
        opsin_weights=None,
        cholesky_whitening=False,
        **kwargs
    ):

        self.equiluminance_plane_discard = None
        self.equiluminance_plane = None

        self.ao_devices = ao_devices

        if luminant_multiples is None:
            self.luminant_multiples = [1]
        else:
            self.luminant_multiples = luminant_multiples

        self.bg_spectrum = None
        self.opsins = ao_devices.opsins
        self.bg_fit = None
        self.channel_names = ao_devices.channel_order #does not include trigger!
        self.led_spectra = ao_devices.led_spectra
        self.measurement_range = ao_devices.measurement_range
        self.use_minimizer = use_minimizer
        self.mix_method = mix_method
        self.opsin_weights = opsin_weights
        self.cholesky_whitening = cholesky_whitening
        self.led_to_test = led_to_test

        if self.opsins is None:
            raise ValueError('Must provide opsin data for spectra steps.')
        if self.led_spectra is None:
            raise ValueError('Must provide led spectra data for spectra steps.')

        #should contain all ao mappings
        #but set to None
        #problem if mapping name when setting to None
        #problem for dataframe
        #general purpose fitting
        #should be optimized to mimic background
        background, bg_intensities = self._get_background_values(
            background,
            background_spectrum,
            background_wavelengths,
            background_intensity
        )
        ###

        self.bg_q = self.opsins.capture(
            self.led_spectra,
            intensity=bg_intensities, combine=True)

        self.bg_ordered = np.array(
            pd.Series(background)[self.channel_names])

        print('bg q: {}'.format(self.bg_q))

        self.ch_bool = np.array(self.channel_names) == led_to_test

        # simply set measurement range of led test to null basically
        led_bg = background[led_to_test]

        self.measurement_range[
            self.ch_bool, 0
            ] = led_bg - led_bg * 10 ** -10
        self.measurement_range[
            self.ch_bool, 1
            ] = led_bg + led_bg * 10 ** -10

        self.opsins_min = self.opsins.capture(
            self.led_spectra, self.measurement_range[:, 0], True)
        self.opsins_max = self.opsins.capture(
            self.led_spectra, self.measurement_range[:, 1], True)
        self.opsins_range = np.array([self.opsins_min, self.opsins_max]).T

        #find single wavelength simulation intensities
        self.values_frame, self.target_qs = self._find_single_values()

        #calculate r2 values for each target q vector
        self.values_frame = self._calculate_residuals(
            self.values_frame,
            self.target_qs)

        actual_frame = self._get_actual_led_ints()

        self.values_frame = self.values_frame.append(
            actual_frame, ignore_index=True, sort=True)

        # create values dict as holder
        values_dict = self.values_frame[self.channel_names].to_dict('list')

        #
        Steps.__init__(
            self, ao_devices, values_dict,
            background=background,
            **kwargs)


    def _find_single_values(self):
        """
        """

        spectra_to_simulate = Spectra(
            self.led_spectra.s[:, self.ch_bool],
            self.led_spectra.wl,
            intensities=self.bg_intensity,
            names=[self.led_to_test]
        )

        self.wls_to_simulate = np.array([self.led_to_test])

        names = [{f'{wl}':1} for wl in self.wls_to_simulate]

        self.led_to_test_id = names

        equi_qs = self._calculate_qs(spectra_to_simulate)

        self.simple_equi_qs = equi_qs.copy()

        all_qs, lum_list, mix_list, all_to_simulate = \
            self._get_all_qs(equi_qs)

        if np.any(all_qs < 0):
            qbool = np.any(all_qs < 0, axis=1)
            print(np.unique(np.array(lum_list)[qbool]))
            print(np.unique(np.array(mix_list)[qbool]))
            print(np.unique(np.array(all_to_simulate)[qbool]))
            print("all_qs", all_qs)
            raise ValueError("relative photon capture below zero")

        if np.any(all_qs == 0) and self.use_minimizer is not None:
            print('adjusting zero q for fitting to log')
            qbool = all_qs == 0
            min_q = np.min(all_qs[~qbool]) * 10 ** -3
            all_qs[qbool] = min_q

        single_results = self._fit_qs(
            all_qs, mix_list, lum_list, all_to_simulate)

        single_values = self._get_values_frame(single_results, names)

        optimizer_result = self._get_optimizer_frame(single_results)

        single_values = pd.concat(
            [single_values, optimizer_result],
            axis=1
        )

        return single_values, all_qs

    def _get_actual_led_ints(self):
        """
        """
        intensities = []

        bg_ints = self.bg_ordered.copy()
        nobg_ints = np.zeros(self.bg_ordered.size)

        ch_idx = np.where(self.ch_bool)[0][0]

        ints = np.asarray(self.luminant_multiples) * self.bg_intensity

        bg_ints = np.asarray([bg_ints]*len(ints))

        nobg_ints = np.asarray([nobg_ints]*len(ints))

        bg_ints[:, ch_idx] += ints
        nobg_ints[:, ch_idx] += ints

        if self.mix_method == 'both':

            all_ints = np.vstack([bg_ints, nobg_ints])
            actual_frame = pd.DataFrame(
                all_ints, columns=self.channel_names)
            actual_frame['mix_method'] = np.repeat(
                ['addition', 'constant'],
                len(ints)
            )
            actual_frame['luminant_multiple'] = self.luminant_multiples * 2

        elif 'addition' in self.mix_method:

            all_ints = bg_ints
            actual_frame = pd.DataFrame(
                all_ints, columns=self.channel_names)
            actual_frame['mix_method'] = self.mix_method
            actual_frame['luminant_multiple'] = self.luminant_multiples

        elif 'constant' in self.mix_method:

            all_ints = nobg_ints
            actual_frame = pd.DataFrame(
                all_ints, columns=self.channel_names)
            actual_frame['mix_method'] = self.mix_method
            actual_frame['luminant_multiple'] = self.luminant_multiples

        else:
            raise NameError(f'no mix method {self.mix_method}')

        actual_frame['single_wls'] = self.led_to_test
        actual_frame['spectrum_id'] = self.led_to_test_id \
            * len(actual_frame)

        return actual_frame

    def generate_volts(self):

        volts, volts_log = Steps.generate_volts(self)

        volts_log.update({
            'led_to_test' : self.led_to_test,
            'luminant_multiples' : self.luminant_multiples,
        })

        return volts, volts_log
