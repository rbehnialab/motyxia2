"""
"""

import os
import argparse
import seabreeze.spectrometers as sb

from motyxia.utils import read_json
from motyxia.fullfield import devices

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-l", "--location", help="location of basedir", type=str)

    args = parser.parse_args()

    config = read_json(args.location)
    
    sb_device = sb.list_devices()[0]
    
    #Never overwrite
    for ao_param in config['aosettings']['ao_params']:
            ao_param['measurement_file'] = None

    
    if 'arduino_pin' in config['aosettings']['ao_params'][0]: #The user checked "arduino" in the core configuration so we have an arduino
    
        arduino_devices = devices.ArduinoDevices(
            *config['aosettings']['ao_params']
        )
    
        for arduino_mapping in arduino_devices.arduino_mappings.values():
    
            arduino_mapping.measure(sb_device, **config['measurementsettings'])
    
        print('all measurements completed.')
        
    else:
        ao_devices = devices.AoDevices(
            *config['aosettings']['ao_params']
        )
    
    
        for ao_mapping in ao_devices.ao_mappings.values():
    
            ao_mapping.measure(sb_device, **config['measurementsettings'])
    
        print('all measurements completed.')

