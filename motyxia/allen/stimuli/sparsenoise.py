from .stim import Stim
from .util import *
import os
import numpy as np
import matplotlib.pyplot as plt
import random
import time
import h5py
import warnings
from itertools import product
from scipy.stats import truncnorm
from pathlib import Path
import pickle
import uuid

from . import tools

try:
    import skimage.external.tifffile as tf
except ImportError:
    import tifffile as tf


class SparseNoise(Stim):
    """
    generate sparse noise stimulus integrates flashing indicator for photodiode

    This stimulus routine presents quasi-random noise in a specified region of
    the monitor. The `background` color can be customized but defaults to a
    grey value. Can specify the `subregion` of the monitor where the pixels
    will flash on and off (black and white respectively)

    Parameters
    ----------
    monitor : monitor object
        contains display monitor information
    indicator : indicator object
        contains indicator information
    coordinate : str from {'degree','linear'}, optional
        specifies coordinates, defaults to 'degree'
    background : float, optional
        color of background. Takes values in [-1,1] where -1 is black and 1
        is white
    pregap_dur : float, optional
        amount of time (in seconds) before the stimulus is presented, defaults
        to `2.`
    postgap_dur : float, optional
        amount of time (in seconds) after the stimulus is presented, defaults
        to `3.`
    grid_space : 2-tuple of floats, optional
        first coordinate is altitude, second coordinate is azimuth
    probe_size : 2-tuple of floats, optional
        size of flicker probes. First coordinate defines the width, and
        second coordinate defines the height
    probe_orientation : float, optional
        orientation of flicker probes
    probe_frame_num : int, optional
        number of frames for each square presentation
    subregion : list or tuple
        the region on the monitor that will display the sparse noise,
        list or tuple, [min_alt, max_alt, min_azi, max_azi]
    sign : {'ON-OFF', 'ON', 'OFF'}, optional
        determines which pixels appear in the `subregion`, defaults to
        `'ON-Off'` so that both on and off pixels appear. If `'ON` selected
        only on pixels (white) are displayed in the noise `subregion while if
        `'OFF'` is selected only off (black) pixels are displayed in the noise
    iteration : int, optional
        number of times to present stimulus, defaults to `1`
    is_include_edge : bool, default True,
        if True, the displayed probes will cover the edge case and ensure that
        the entire subregion is covered.
        If False, the displayed probes will exclude edge case and ensure that all
        the centers of displayed probes are within the subregion.
    """

    def __init__(self, monitor, indicator, background=0., coordinate='degree',
                 grid_space=(10., 10.), probe_size=(10., 10.), probe_orientation=0.,
                 probe_frame_num=6, subregion=None, sign='ON-OFF', iteration=1,
                 pregap_dur=2., postgap_dur=3., is_include_edge=True,
                 minmax=(-1, 1), refresh_distance=0., pause_frame_num=0,
                 seed=None
                 ):
        """
        Initialize sparse noise object, inherits Parameters from Stim object
        """

        super(SparseNoise, self).__init__(monitor=monitor,
                                          indicator=indicator,
                                          background=background,
                                          coordinate=coordinate,
                                          pregap_dur=pregap_dur,
                                          postgap_dur=postgap_dur,
                                          seed=seed)

        #self.stim_name = self.__class__.__name__
        self.grid_space = grid_space
        self.probe_size = probe_size
        self.probe_orientation = probe_orientation
        self.minmax = minmax
        self.refresh_distance = refresh_distance
        self.pause_frame_num = pause_frame_num

        if probe_frame_num >= 2.:
            self.probe_frame_num = int(probe_frame_num)
        else:
            raise ValueError('SparseNoise: probe_frame_num should be no less than 2.')

        self.is_include_edge = is_include_edge
        self.frame_config = ('is_display', 'probe center (altitude, azimuth)',
                             'polarity (-1 or 1)', 'indicator color [-1., 1.]')

        if subregion is None:
            if self.coordinate == 'degree':
                self.subregion = [np.amin(self.monitor.deg_coord_y),
                                  np.amax(self.monitor.deg_coord_y),
                                  np.amin(self.monitor.deg_coord_x),
                                  np.amax(self.monitor.deg_coord_x)]
            if self.coordinate == 'linear':
                self.subregion = [np.amin(self.monitor.lin_coord_y),
                                  np.amax(self.monitor.lin_coord_y),
                                  np.amin(self.monitor.lin_coord_x),
                                  np.amax(self.monitor.lin_coord_x)]
        else:
            self.subregion = subregion

        self.sign = sign
        if iteration >= 1:
            self.iteration = int(iteration)
        else:
            raise ValueError('iteration should be no less than 1.')

        self.clear()

    def _get_grid_locations(self, is_plot=False):
        """
        generate all the grid points in display area (covered by both subregion and
        monitor span)

        Returns
        -------
        grid_points : n x 2 array,
            refined [alt, azi] pairs of probe centers going to be displayed
        """

        # get all the visual points for each pixels on monitor
        if self.coordinate == 'degree':
            monitor_azi = self.monitor.deg_coord_x
            monitor_alt = self.monitor.deg_coord_y
        elif self.coordinate == 'linear':
            monitor_azi = self.monitor.lin_coord_x
            monitor_alt = self.monitor.lin_coord_y
        else:
            raise ValueError('Do not understand coordinate system: {}. '
                             'Should be either "linear" or "degree".'.
                             format(self.coordinate))

        grid_locations = get_grid_locations(subregion=self.subregion, grid_space=self.grid_space,
                                            monitor_azi=monitor_azi, monitor_alt=monitor_alt,
                                            is_include_edge=self.is_include_edge, is_plot=is_plot)

        return grid_locations

    def _shuffle_grid_points(self, grid_points, probe_ind=None):
        random.seed(self.seed)
        if self.refresh_distance == 0 or self.refresh_distance is None:
            if probe_ind is not None:
                random.shuffle(probe_ind)
                return probe_ind
            else:
                random.shuffle(grid_points)
                return grid_points

        else:
            #raise NotImplementedError("sparse noise with refresh distance")
            if probe_ind is not None:
                random.shuffle(probe_ind)
            else:
                random.shuffle(grid_points)

            #print('gridpoints', grid_points)
            grid_points = np.array(grid_points)
            #measure distances
            distances = np.sqrt(np.sum(
                (grid_points[None, ...] - grid_points[:, None, :])**2,
                axis=-1))
            allowed_distances = distances > self.refresh_distance
            #current_state = np.diag(allowed_distances, k=1)
            if not np.all(np.any(allowed_distances, axis=1)):
                raise ValueError('refresh distances too spread!')
            #print(allowed_distances, 'distances')

            if probe_ind is not None:
                allowed_distances = allowed_distances[probe_ind, :][:, probe_ind]

            graph = create_graph_from_adjacency(allowed_distances)

            #loop to find a path
            #will reuse certain grid_points
            node = random.choice(list(graph))
            sequence = [node]
            #TODO stop endless loop if exists
            while set(graph) - set(sequence):
                #print(node)
                edges = graph[node]
                available_edges = list(set(edges) - set(sequence))
                edges = list(set(edges) - set([node]))

                if not available_edges:
                    edge = random.choice(edges)
                else:
                    edge = random.choice(available_edges)

                sequence.append(edge)
                node = edge

            if probe_ind is None:
                return grid_points[np.array(sequence)].tolist()
            else:
                return sequence


    def _generate_grid_points_sequence(self):
        """
        generate pseudorandomized grid point sequence. if ON-OFF, consecutive
        frames should not present stimulus at same location

        Returns
        -------
        all_grid_points : list
            list of the form [grid_point, sign]
        """

        grid_points = self._get_grid_locations()

        if self.sign == 'ON':
            grid_points = self._shuffle_grid_points(grid_points)
            grid_points = [[x, self.minmax[1]] for x in grid_points]
            return grid_points
        elif self.sign == 'OFF':
            grid_points = self._shuffle_grid_points(grid_points)
            grid_points = [[x, self.minmax[0]] for x in grid_points]
            return grid_points
        elif self.sign == 'ON-OFF':
            grid_points = self._shuffle_grid_points(grid_points)
            all_grid_points = (
                [[x, self.minmax[1]] for x in grid_points]
                + [[x, self.minmax[0]] for x in grid_points])
            # remove coincident hit of same location by continuous frames
            print('removing coincident hit of same location with continuous frames:')
            while True:
                iteration = 0
                coincident_hit_num = 0
                for i, grid_point in enumerate(all_grid_points[:-3]):
                    if (all_grid_points[i][0] == all_grid_points[i + 1][0]).all():
                        all_grid_points[i + 1], all_grid_points[i + 2] = all_grid_points[i + 2], all_grid_points[i + 1]
                        coincident_hit_num += 1
                iteration += 1
                print('iteration:' ,iteration ,'  continous hits number:' , coincident_hit_num)
                if coincident_hit_num == 0:
                    break

            return all_grid_points

    def generate_frames(self):
        """
        function to generate all the frames needed for SparseNoise stimulus

        returns a list of information of all frames as a list of tuples

        Information contained in each frame:
             first element - int
                  when stimulus is displayed value is equal to 1, otherwise
                  equal to 0,
             second element - tuple,
                  retinotopic location of the center of current square,[alt, azi]
             third element -
                  polarity of current square, 1 -> bright, -1-> dark
             forth element - color of indicator
                  if synchronized : value equal to 0 when stimulus is not
                       begin displayed, and 1 for onset frame of stimulus for
                       each square, -1 for the rest.
                  if non-synchronized: values alternate between -1 and 1
                       at defined frequency

             for gap frames the second and third elements should be 'None'
        """

        frames = []
        if self.probe_frame_num == 1:
            indicator_on_frame = 1
        elif self.probe_frame_num > 1:
            indicator_on_frame = self.probe_frame_num // 2
        else:
            raise ValueError('`probe_frame_num` should be an int larger than 0!')

        indicator_off_frame = self.probe_frame_num - indicator_on_frame

        frames += [[0., None, None, -1.]] * self.pregap_frame_num

        for i in range(self.iteration):

            iter_grid_points = self._generate_grid_points_sequence()

            for grid_point in iter_grid_points:
                frames += [[1., grid_point[0], grid_point[1], 1.]] * indicator_on_frame
                frames += [[1., grid_point[0], grid_point[1], -1.]] * indicator_off_frame

        frames += [[0., None, None, -1.]] * self.postgap_frame_num

        if not self.indicator.is_sync:
            indicator_frame = self.indicator.frame_num
            for m in range(len(frames)):
                if np.floor(m // indicator_frame) % 2 == 0:
                    frames[m][3] = 1.
                else:
                    frames[m][3] = -1.

        frames = [tuple(x) for x in frames]

        return tuple(frames)

    def _generate_frames_for_index_display(self):
        """ compute the information that defines the frames used for index display"""
        if self.indicator.is_sync:
            frames_unique = []

            gap = [0., None, None, -1.]
            frames_unique.append(gap)
            grid_points = self._get_grid_locations()
            for grid_point in grid_points:
                if self.sign == 'ON':
                    frames_unique.append([1., grid_point, 1., 1.])
                    frames_unique.append([1., grid_point, 1., -1.])
                elif self.sign == 'OFF':
                    frames_unique.append([1., grid_point, -1., 1.])
                    frames_unique.append([1., grid_point, -1., -1])
                elif self.sign == 'ON-OFF':
                    frames_unique.append([1., grid_point, 1., 1.])
                    frames_unique.append([1., grid_point, 1., -1.])
                    frames_unique.append([1., grid_point, -1., 1.])
                    frames_unique.append([1., grid_point, -1., -1])
                else:
                    raise ValueError('SparseNoise: Do not understand "sign", should '
                                     'be one of "ON", "OFF" and "ON-OFF".')

            frames_unique = tuple([tuple(f) for f in frames_unique])

            return frames_unique
        else:
            raise NotImplementedError( "method not available for non-sync indicator")

    def _get_probe_index_for_one_iter_on_off(self, frames_unique):
        """
        get shuffled probe indices from frames_unique generated by
        self._generate_frames_for_index_display(), only for 'ON-OFF' stimulus

        the first element of frames_unique should be gap frame, the following
        frames should be [
                          (probe_i_ON, indictor_ON),
                          (probe_i_ON, indictor_OFF),
                          (probe_i_OFF, indictor_ON),
                          (probe_i_OFF, indictor_OFF),
                          ]

        it is designed such that no consecutive probes will hit the same visual
        field location

        return list of integers, indices of shuffled probe
        """

        if len(frames_unique) % 4 == 1:
            probe_num = (len(frames_unique) - 1) // 2
        else:
            raise ValueError('number of frames_unique should be 4x + 1')

        probe_locations = [f[1] for f in frames_unique[1::2]]
        # probe_locations = self._shuffle_grid_points(probe_locations)
        probe_ind = np.arange(probe_num).astype(int)
        # print(len(probe_ind), 'before')
        probe_ind = self._shuffle_grid_points(probe_locations, probe_ind)
        probe_num = len(probe_ind)
        # print(len(probe_ind), 'after')
        # np.random.shuffle(probe_ind)

        is_overlap = True
        while is_overlap:
            is_overlap = False
            for i in np.arange(probe_num - 1):
                probe_loc_0 = probe_locations[probe_ind[i]]
                probe_loc_1 = probe_locations[probe_ind[i + 1]]
                if np.array_equal(probe_loc_0, probe_loc_1):
                    ind_temp = probe_ind[i + 1]
                    probe_ind[i + 1] = probe_ind[(i + 2) % probe_num]
                    probe_ind[(i + 2) % probe_num] = ind_temp
                    is_overlap = True

        return probe_ind

    def _generate_display_index(self):
        """ compute a list of indices corresponding to each frame to display. """

        frames_unique = self._generate_frames_for_index_display()
        probe_on_frame_num = self.probe_frame_num // 2
        probe_off_frame_num = self.probe_frame_num - probe_on_frame_num

        if self.sign in ['ON', 'OFF']:

            if len(frames_unique) % 2 == 1:
                probe_num = (len(frames_unique) - 1) / 2
            else:
                raise ValueError(
                    'SparseNoise: number of unique frames is not correct.'
                    ' Should be odd.')

            index_to_display = []

            index_to_display += [0] * self.pregap_frame_num

            probe_locations = [f[1] for f in frames_unique[1::2]]

            for iter in range(self.iteration):

                probe_sequence = np.arange(probe_num).astype(int)
                probe_sequence = self._shuffle_grid_points(
                    probe_locations, probe_sequence)
                #np.random.shuffle(probe_sequence)

                for probe_ind in probe_sequence:
                    index_to_display += [probe_ind * 2 + 1] * probe_on_frame_num
                    index_to_display += [probe_ind * 2 + 2] * probe_off_frame_num
                    index_to_display += [0] * self.pause_frame_num

            index_to_display += [0] * self.postgap_frame_num

        elif self.sign == 'ON-OFF':
            if len(frames_unique) % 4 != 1:
                raise ValueError('number of frames_unique should be 4x + 1')

            index_to_display = []

            index_to_display += [0] * self.pregap_frame_num

            for iter in range(self.iteration):
                probe_inds = self._get_probe_index_for_one_iter_on_off(frames_unique)

                for probe_ind in probe_inds:
                    index_to_display += [probe_ind * 2 + 1] * probe_on_frame_num
                    index_to_display += [probe_ind * 2 + 2] * probe_off_frame_num
                    index_to_display += [0] * self.pause_frame_num

            index_to_display += [0] * self.postgap_frame_num

        else:
            raise ValueError('SparseNoise: Do not understand "sign", should '
                             'be one of "ON", "OFF" and "ON-OFF".')

        return frames_unique, index_to_display

    def generate_movie_by_index(self):
        """ compute the stimulus movie to be displayed by index. """
        #start addition
        filename=self.stored_filename()
        is_caching_active_resp=self.check_is_caching_active(filename)
        if len(is_caching_active_resp)==2:
            return is_caching_active_resp
        else:
            dirpath,endpath_np_short,endpath_seq_short,fmp,endpath_seq,endpath_np=is_caching_active_resp
            self.frames_unique, self.index_to_display = self._generate_display_index()

            num_unique_frames = len(self.frames_unique)
            num_pixels_width = self.monitor.deg_coord_x.shape[0]
            num_pixels_height = self.monitor.deg_coord_x.shape[1]

            if self.coordinate == 'degree':
                coord_azi = self.monitor.deg_coord_x
                coord_alt = self.monitor.deg_coord_y
            elif self.coordinate == 'linear':
                coord_azi = self.monitor.lin_coord_x
                coord_alt = self.monitor.lin_coord_y
            else:
                raise ValueError('Do not understand coordinate system: {}. '
                                 'Should be either "linear" or "degree".'.
                                 format(self.coordinate))

            indicator_width_min = int(self.indicator.center_width_pixel
                                   - self.indicator.width_pixel / 2)
            indicator_width_max = int(self.indicator.center_width_pixel
                                   + self.indicator.width_pixel / 2)
            indicator_height_min = int(self.indicator.center_height_pixel
                                    - self.indicator.height_pixel / 2)
            indicator_height_max = int(self.indicator.center_height_pixel
                                    + self.indicator.height_pixel / 2)

            full_seq = self.background * \
                       np.ones((num_unique_frames, num_pixels_width, num_pixels_height), dtype=np.float32)

            for i, frame in enumerate(self.frames_unique):
                if frame[0] == 1:
                    if frame[0] == 1:
                        curr_probes = self._format_probe_info(frame)
                        disp_mat = get_warped_probes(
                            deg_coord_alt=coord_azi,
                            deg_coord_azi=coord_alt,
                            probes=curr_probes,
                            width=self.probe_size[0],
                            height=self.probe_size[1],
                            ori=self.probe_orientation,
                            background_color=self.background)
                        full_seq[i] = disp_mat


                full_seq[i, indicator_height_min:indicator_height_max,
                indicator_width_min:indicator_width_max] = frame[3]

            mondict = dict(self.monitor.__dict__)
            indicator_dict = dict(self.indicator.__dict__)
            indicator_dict.pop('monitor')
            SNdict = dict(self.__dict__)
            SNdict.pop('monitor')
            SNdict.pop('indicator')
            full_dict = {'stimulation': SNdict,
                         'monitor': mondict,
                         'indicator': indicator_dict}
            # end addition
            self.write_seq_to_cache(
                dirpath,
                endpath_np_short,
                full_seq,
                endpath_seq_short, 
                full_dict, 
                fmp,endpath_seq,
                endpath_np)

            return full_seq, full_dict

    def _format_probe_info(self, frame):
        return ([frame[1][0], frame[1][1], frame[2]],)


    def _format_probe_info(self, frame):
        return ([frame[1][0], frame[1][1], frame[2]],)

    def generate_movie(self):
        """
        generate movie for display frame by frame
        """
        #start addition
        filename=self.stored_filename();
        is_caching_active_resp=self.check_is_caching_active(filename)
        if len(is_caching_active_resp)==2:
            return is_caching_active_resp[0],is_caching_active_resp[1]

        else:
            dirpath,endpath_np_short,endpath_seq_short,fmp,endpath_seq,endpath_np=is_caching_active_resp
            self.frames = self.generate_frames()
            if self.coordinate == 'degree':
                coord_x = self.monitor.deg_coord_x
                coord_y = self.monitor.deg_coord_y
            elif self.coordinate == 'linear':
                coord_x = self.monitor.lin_coord_x
                coord_y = self.monitor.lin_coord_y
            else:
                raise ValueError('Do not understand coordinate system: {}. '
                                 'Should be either "linear" or "degree".'.
                                 format(self.coordinate))

            indicator_width_min = int(self.indicator.center_width_pixel
                                   - self.indicator.width_pixel / 2)
            indicator_width_max = int(self.indicator.center_width_pixel
                                   + self.indicator.width_pixel / 2)
            indicator_height_min = int(self.indicator.center_height_pixel
                                    - self.indicator.height_pixel / 2)
            indicator_height_max = int(self.indicator.center_height_pixel
                                    + self.indicator.height_pixel / 2)

            full_seq = np.ones((len(self.frames),
                                self.monitor.deg_coord_x.shape[0],
                                self.monitor.deg_coord_x.shape[1]),
                               dtype=np.float32) * self.background

            for i, curr_frame in enumerate(self.frames):
                if curr_frame[0] == 1:  # not a gap

                    curr_probes = ([curr_frame[1][0], curr_frame[1][1], curr_frame[2]],)

                    if i == 0:  # first frame and (not a gap)

                        curr_disp_mat = get_warped_probes(deg_coord_alt=coord_y,
                                                          deg_coord_azi=coord_x,
                                                          probes=curr_probes,
                                                          width=self.probe_size[0],
                                                          height=self.probe_size[1],
                                                          ori=self.probe_orientation,
                                                          background_color=self.background)
                    else:  # (not first frame) and (not a gap)
                        if self.frames[i - 1][1] is None:  # (not first frame) and (not a gap) and (new square from gap)
                            curr_disp_mat = get_warped_probes(deg_coord_alt=coord_y,
                                                              deg_coord_azi=coord_x,
                                                              probes=curr_probes,
                                                              width=self.probe_size[0],
                                                              height=self.probe_size[1],
                                                              ori=self.probe_orientation,
                                                              background_color=self.background)
                        elif (curr_frame[1] != self.frames[i - 1][1]).any() or (curr_frame[2] != self.frames[i - 1][2]):
                            # (not first frame) and (not a gap) and (new square from old square)
                            curr_disp_mat = get_warped_probes(deg_coord_alt=coord_y,
                                                              deg_coord_azi=coord_x,
                                                              probes=curr_probes,
                                                              width=self.probe_size[0],
                                                              height=self.probe_size[1],
                                                              ori=self.probe_orientation,
                                                              background_color=self.background)

                    # assign current display matrix to full sequence
                    full_seq[i] = curr_disp_mat

                # add sync square for photodiode
                full_seq[i, indicator_height_min:indicator_height_max,
                indicator_width_min:indicator_width_max] = curr_frame[3]

                if i in range(0, len(self.frames), len(self.frames) // 10):
                    print('Generating numpy sequence: ' +
                           str(int(100 * (i + 1) / len(self.frames))) + '%')

            # generate log dictionary
            mondict = dict(self.monitor.__dict__)
            indicator_dict = dict(self.indicator.__dict__)
            indicator_dict.pop('monitor')
            SNdict = dict(self.__dict__)
            SNdict.pop('monitor')
            SNdict.pop('indicator')
            full_dict = {'stimulation': SNdict,
                         'monitor': mondict,
                         'indicator': indicator_dict}
            # end addition
            self.write_seq_to_cache(dirpath,endpath_np_short,full_dict,endpath_seq_short, full_seq,fmp,endpath_seq,endpath_np)
            return full_seq, full_dict
