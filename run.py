from motyxia.app import app
import os
import json

# Grabs the folder where the script runs.
BASEDIR = os.path.abspath(os.path.dirname(__file__))
MEASUREMENTSDIR = os.path.join(BASEDIR, 'measurements')
SETTINGSDIR = os.path.join(BASEDIR, 'settings')
DATADIR = os.path.join(BASEDIR, 'data')
MONITORFILENAME = 'monitorsettings.json'
INDICATORFILENAME = 'indicatorsettings.json'
STIMULIFILENAME = 'stimulisettings.json'
PROJECTORFILENAME = 'projectorsettings.json'
BACKGROUNDFILENAME = 'backgroundsettings.json'
AOFILENAME = 'aosettings.json'
COREFILENAME = 'coresettings.json'
MEASUREMENTFILENAME = 'measurementsettings.json'
MONITORSETTINGS = os.path.join(SETTINGSDIR, MONITORFILENAME)
INDICATORSETTINGS = os.path.join(SETTINGSDIR, INDICATORFILENAME)
STIMULISETTINGS = os.path.join(SETTINGSDIR, STIMULIFILENAME)
PROJECTORSETTINGS = os.path.join(SETTINGSDIR, PROJECTORFILENAME)
BACKGROUNDSETTINGS = os.path.join(SETTINGSDIR, BACKGROUNDFILENAME)
AOSETTINGS = os.path.join(SETTINGSDIR, AOFILENAME)
CORESETTINGS = os.path.join(SETTINGSDIR, COREFILENAME)
MEASUREMENTSETTINGS = os.path.join(SETTINGSDIR, MEASUREMENTFILENAME)


if not os.path.exists(MEASUREMENTSDIR):
    os.makedirs(MEASUREMENTSDIR)

if not os.path.exists(SETTINGSDIR):
    os.makedirs(SETTINGSDIR)

if not os.path.exists(DATADIR):
    os.makedirs(DATADIR)

if not os.path.exists(MONITORSETTINGS):
    with open(MONITORSETTINGS, 'w') as f:
        json.dump({}, f)

if not os.path.exists(INDICATORSETTINGS):
    with open(INDICATORSETTINGS, 'w') as f:
        json.dump({}, f)

if not os.path.exists(STIMULISETTINGS):
    with open(STIMULISETTINGS, 'w') as f:
        json.dump({}, f)

if not os.path.exists(PROJECTORSETTINGS):
    with open(PROJECTORSETTINGS, 'w') as f:
        json.dump({}, f)

if not os.path.exists(BACKGROUNDSETTINGS):
    with open(BACKGROUNDSETTINGS, 'w') as f:
        json.dump({}, f)

if not os.path.exists(AOSETTINGS):
    with open(AOSETTINGS, 'w') as f:
        json.dump({}, f)

if not os.path.exists(CORESETTINGS):
    with open(CORESETTINGS, 'w') as f:
        json.dump({}, f)

if not os.path.exists(MEASUREMENTSETTINGS):
    with open(MEASUREMENTSETTINGS, 'w') as f:
        json.dump({}, f)

# Enable debug mode.
DEBUG = False

app.config['basedir'] = BASEDIR
app.config['measurementsdir'] = MEASUREMENTSDIR
app.config['datadir'] = DATADIR
app.config['settingsdir'] = SETTINGSDIR
app.config['monitorsettings_file'] = MONITORSETTINGS
app.config['indicatorsettings_file'] = INDICATORSETTINGS
app.config['stimulisettings_file'] = STIMULISETTINGS
app.config['projectorsettings_file'] = PROJECTORSETTINGS
app.config['backgroundsettings_file'] = BACKGROUNDSETTINGS
app.config['aosettings_file'] = AOSETTINGS
app.config['coresettings_file'] = CORESETTINGS
app.config['measurementsettings_file'] = MEASUREMENTSETTINGS
app.run(debug=DEBUG)
