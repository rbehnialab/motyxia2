"""
"""

from .util import *
from .sparsenoise import SparseNoise
import os
import numpy as np
import matplotlib.pyplot as plt
import random
import time
import h5py
import warnings
from itertools import product
from scipy.stats import truncnorm

from . import tools

try:
    import skimage.external.tifffile as tf
except ImportError:
    import tifffile as tf


class LocallySparseNoise(SparseNoise):
    """
    generate locally sparse noise stimulus integrates flashing indicator for
    photodiode

    This stimulus routine presents quasi-random noise in a specified region of
    the monitor. The `background` color can be customized but defaults to a
    grey value. Can specify the `subregion` of the monitor where the pixels
    will flash on and off (black and white respectively)

    Different from SparseNoise stimulus which presents only one probe at a time,
    the LocallySparseNoise presents multiple probes simultaneously to speed up
    the sampling frequency. The sparsity of probes is defined by minimum distance
    in visual degree: in any given frame, the centers of any pair of two probes
    will have distance larger than minimum distance in visual degrees. The
    method generate locally sparse noise here insures, for each iteration, all
    the locations in the subregion will be sampled once and only once.

    Parameters
    ----------
    monitor : monitor object
        contains display monitor information
    indicator : indicator object
        contains indicator information
    coordinate : str from {'degree','linear'}, optional
        specifies coordinates, defaults to 'degree'
    background : float, optional
        color of background. Takes values in [-1,1] where -1 is black and 1
        is white
    pregap_dur : float, optional
        amount of time (in seconds) before the stimulus is presented, defaults
        to `2.`
    postgap_dur : float, optional
        amount of time (in seconds) after the stimulus is presented, defaults
        to `3.`
    min_distance : float, default 20.
        the minimum distance in visual degree for any pair of probe centers
        in a given frame
    grid_space : 2-tuple of floats, optional
        first coordinate is altitude, second coordinate is azimuth
    probe_size : 2-tuple of floats, optional
        size of flicker probes. First coordinate defines the width, and
        second coordinate defines the height
    probe_orientation : float, optional
        orientation of flicker probes
    probe_frame_num : int, optional
        number of frames for each square presentation
    subregion : list or tuple
        the region on the monitor that will display the sparse noise,
        list or tuple, [min_alt, max_alt, min_azi, max_azi]
    sign : {'ON-OFF', 'ON', 'OFF'}, optional
        determines which pixels appear in the `subregion`, defaults to
        `'ON-Off'` so that both on and off pixels appear. If `'ON` selected
        only on pixels (white) are displayed in the noise `subregion while if
        `'OFF'` is selected only off (black) pixels are displayed in the noise
    iteration : int, optional
        number of times to present stimulus with random order, the total number
        a paticular probe will be displayded will be iteration * repeat,
        defaults to `1`
    repeat : int, optional
        number of repeat of whole sequence, the total number a paticular probe
        will be displayded will be iteration * repeat, defaults to `1`
    is_include_edge : bool, default True,
        if True, the displayed probes will cover the edge case and ensure that
        the entire subregion is covered.
        If False, the displayed probes will exclude edge case and ensure that all
        the centers of displayed probes are within the subregion.
    """

    def __init__(
        self, monitor, indicator, min_distance=20., background=0., coordinate='degree',
        grid_space=(10., 10.), probe_size=(10., 10.), probe_orientation=0.,
        probe_frame_num=6, subregion=None, sign='ON-OFF', iteration=1, repeat=1,
        pregap_dur=2., postgap_dur=3., is_include_edge=True, minmax=(-1, 1),
        refresh_distance=None, pause_frame_num=0, seed=None
        ):
        """
        Initialize sparse noise object, inherits Parameters from Stim object
        """

        super().__init__(
            monitor=monitor, indicator=indicator,
            background=background, coordinate=coordinate,
            pregap_dur=pregap_dur, postgap_dur=postgap_dur,
            grid_space=grid_space, probe_size=probe_size,
            probe_orientation=probe_orientation,
            minmax=minmax,
            refresh_distance=refresh_distance,
            pause_frame_num=pause_frame_num,
            is_include_edge=is_include_edge,
            #frame_config=frame_config,
            probe_frame_num=probe_frame_num,
            subregion=subregion,
            sign=sign,
            iteration=iteration,
            seed=seed
        )

        self.min_distance = float(min_distance)

        if repeat >= 1:
            self.repeat = int(repeat)
        else:
            raise ValueError('repeat should be no less than 1.')

        self.clear()

    def _get_grid_locations(self, is_plot=False):
        """
        generate all the grid points in display area (covered by both subregion and
        monitor span)

        Returns
        -------
        grid_points : n x 2 array,
            refined [azi, alt] pairs of probe centers going to be displayed
        """

        # get all the visual points for each pixels on monitor
        if self.coordinate == 'degree':
            monitor_azi = self.monitor.deg_coord_x
            monitor_alt = self.monitor.deg_coord_y
        elif self.coordinate == 'linear':
            monitor_azi = self.monitor.lin_coord_x
            monitor_alt = self.monitor.lin_coord_y
        else:
            raise ValueError('Do not understand coordinate system: {}. Should be either "linear" or "degree".'.
                             format(self.coordinate))

        grid_locations = get_grid_locations(subregion=self.subregion, grid_space=self.grid_space,
                                            monitor_azi=monitor_azi, monitor_alt=monitor_alt,
                                            is_include_edge=self.is_include_edge, is_plot=is_plot)

        return grid_locations

    def _generate_all_probes(self):
        """
        return all possible (grid location + sign) combinations within the subregion,
        return a list of probe parameters, each element in the list is
        [center_altitude, center_azimuth, sign]
        """
        grid_locs = self._get_grid_locations()
        self.grid_locs = grid_locs

        grid_locs = list([list(gl) for gl in grid_locs])

        if self.sign == 'ON':
            all_probes = [gl + [self.minmax[1]] for gl in grid_locs]
        elif self.sign == 'OFF':
            all_probes = [gl + [self.minmax[0]] for gl in grid_locs]
        elif self.sign == 'ON-OFF':
            all_probes = (
                [gl + [self.minmax[1]] for gl in grid_locs]
                + [gl + [self.minmax[0]] for gl in grid_locs])
        else:
            raise ValueError('LocallySparseNoise: Cannot understand self.sign, should be '
                             'one of "ON", "OFF", "ON-OFF".')
        return all_probes

    def _generate_probe_locs_one_frame(self, probes):
        """
        given the available probes, generate a sublist of the probes for a single frame,
        all the probes in the sublist will have their visual space distance longer than
        self.min_distance. This function will also update input probes, remove the
        elements that have been selected into the sublist.

        parameters
        ----------
        probes : list of all available probes
            each elements is [center_altitude, center_azimuth, sign] for a particular probe
        min_dis : float
            minimum distance to reject probes too close to each other

        returns
        -------
        probes_one_frame : list of selected probes fo one frame
            each elements is [center_altitude, center_azimuth, sign] for a selected probe
        """

        np.random.shuffle(probes)
        probes_one_frame = []

        probes_left = list(probes)

        for probe in probes:

            is_overlap = False

            for probe_frame in probes_one_frame:

                curr_dis = tools.distance([probe[0], probe[1]], [probe_frame[0], probe_frame[1]])
                if curr_dis <= self.min_distance:
                    is_overlap = True
                    break

            if not is_overlap:
                probes_one_frame.append(probe)
                probes_left.remove(probe)

        return probes_one_frame, probes_left

    def _generate_probe_sequence_one_iteration(self, all_probes, is_redistribute=True):
        """
        given all probes to be displayed and minimum distance between any pair of two probes
        return frames of one iteration that ensure all probes will be present once

        parameters
        ----------
        all_probes : list
            all probes to be displayed, each element (center_alt, center_azi, sign). ideally
            outputs of self._generate_all_probes()
        is_redistribute : bool
            redistribute the probes among frames after initial generation or not.
            redistribute will use self._redistribute_probes() and try to minimize the difference
            of probe numbers among different frames

        returns
        -------
        frames : tuple
            each element of the frames tuple represent one display frame, the element itself
            is a tuple of the probes to be displayed in this particular frame
        """

        all_probes_cpy = list(all_probes)

        frames = []

        while len(all_probes_cpy) > 0:
            curr_frames, all_probes_cpy = self._generate_probe_locs_one_frame(probes=all_probes_cpy)
            frames.append(curr_frames)

        if is_redistribute:
            frames = self._redistribute_probes(frames=frames)

        frames = tuple(tuple(f) for f in frames)

        return frames

    def _redistribute_one_probe(self, frames):

        # initiate is_moved variable
        is_moved = False

        # reorder frames from most probes to least probes
        new_frames = sorted(frames, key=lambda frame: len(frame))
        probe_num_most = len(new_frames[-1])

        # the indices of frames in new_frames that contain most probes
        frame_ind_most = []

        # the indices of frames in new_frames that contain less probes
        frame_ind_less = []

        for frame_ind, frame in enumerate(new_frames):
            if len(frame) == probe_num_most:
                frame_ind_most.append(frame_ind)
            elif len(frame) <= probe_num_most - 2:  # '-1' means well distributed
                frame_ind_less.append(frame_ind)

        # constructing a list of probes that potentially can be moved
        # each element is [(center_alt, center_azi, sign), frame_ind]
        probes_to_be_moved = []
        for frame_ind in frame_ind_most:
            frame_most = new_frames[frame_ind]
            for probe in frame_most:
                probes_to_be_moved.append((probe, frame_ind))

        # loop through probes_to_be_moved to see if any of them will fit into
        # frames with less probes, once find a case, break the loop and return
        for probe, frame_ind_src in probes_to_be_moved:
            frame_src = new_frames[frame_ind_src]
            for frame_ind_dst in frame_ind_less:
                frame_dst = new_frames[frame_ind_dst]
                if self._is_fit(probe, frame_dst):
                    frame_src.remove(probe)
                    frame_dst.append(probe)
                    is_moved = True
                    break
            if is_moved:
                break

        return is_moved, new_frames

    def _is_fit(self, probe, probes):
        """
        test if a given probe will fit a group of probes without breaking the
        sparcity

        parameters
        ----------
        probe : list or tuple of three floats
            (center_alt, center_azi, sign)
        probes : list of probes
            [(center_alt, center_zai, sign), (center_alt, center_azi, sign), ...]

        returns
        -------
        is_fit : bool
            the probe will fit or not
        """

        is_fit = True
        for probe2 in probes:
            if tools.distance([probe[0], probe[1]], [probe2[0], probe2[1]]) <= self.min_distance:
                is_fit = False
                break
        return is_fit

    def _redistribute_probes(self, frames):
        """
        attempt to redistribute probes among frames for one iteration of display
        the algorithm is to pick a probe from the frames with most probes to the
        frames with least probes and do it iteratively until it can not move
        anymore and the biggest difference of probe numbers among all frames is
        no more than 1 (most evenly distributed).

        the algorithm is implemented by self._redistribute_probes() function,
        this is just to roughly massage the probes among frames, but not the
        attempt to find the best solution.

        parameters
        ----------
        frames : list
            each element of the frames list represent one display frame, the element
            itself is a list of the probes (center_alt, center_azi, sign) to be
            displayed in this particular frame

        returns
        -------
        new_frames : list
            same structure as input frames but with redistributed probes
        """

        new_frames = list(frames)
        is_moved = True
        probe_nums = [len(frame) for frame in new_frames]
        probe_nums.sort()
        probe_diff = probe_nums[-1] - probe_nums[0]

        while is_moved and probe_diff > 1:

            is_moved, new_frames = self._redistribute_one_probe(new_frames)
            probe_nums = [len(frame) for frame in new_frames]
            probe_nums.sort()
            probe_diff = probe_nums[-1] - probe_nums[0]
        else:
            if not is_moved:
                # print ('redistributing probes among frames: no more probes can be moved.')
                pass
            if probe_diff <= 1:
                # print ('redistributing probes among frames: probes already well distributed.')
                pass

        return new_frames

    def _generate_frames_for_index_display(self):
        """
        compute the information that defines the frames used for index display

        parameters
        ----------
        all_probes : list
            all probes to be displayed, each element (center_alt, center_azi, sign). ideally
            outputs of self._generate_all_probes()

        returns
        -------
        frames_unique : tuple
        """
        all_probes = self._generate_all_probes()

        frames_unique = []

        gap = [0., None, None, -1.]
        frames_unique.append(gap)
        for i in range(self.iteration):
            probes_iter = self._generate_probe_sequence_one_iteration(all_probes=all_probes,
                                                                      is_redistribute=True)
            for probes in probes_iter:
                frames_unique.append([1., probes, i, 1.])
                frames_unique.append([1., probes, i, -1.])

        frames_unique = tuple([tuple(f) for f in frames_unique])
        print('frames_unique[:2] in generate_frames_for_index_dislay:',frames_unique[:2])

        return frames_unique

    def _generate_display_index(self):
        """
        compute a list of indices corresponding to each frame to display.
        """

        if self.indicator.is_sync:

            frames_unique = self._generate_frames_for_index_display()
            if len(frames_unique) % 2 == 1:
                display_num = (len(frames_unique) - 1) // 2  # number of each unique display frame
            else:
                raise ValueError('LocallySparseNoise: number of unique frames is not correct. Should be odd.')

            probe_on_frame_num = self.probe_frame_num // 2
            probe_off_frame_num = self.probe_frame_num - probe_on_frame_num

            index_to_display = []

            for display_ind in np.arange(display_num).astype(int):
                index_to_display += [display_ind * 2 + 1] * probe_on_frame_num
                index_to_display += [display_ind * 2 + 2] * probe_off_frame_num
                index_to_display += [0] * self.pause_frame_num

            index_to_display = index_to_display * self.repeat

            index_to_display = [0] * self.pregap_frame_num + index_to_display + [0] * self.postgap_frame_num

            print('Length index_to_display:',len(index_to_display))

            return frames_unique, index_to_display

        else:
            raise NotImplementedError( "method not available for non-sync indicator")

    # def generate_movie_by_index(self):
    #
    #     self.frames_unique, self.index_to_display = self._generate_display_index()
    #
    #     num_unique_frames = len(self.frames_unique)
    #     num_pixels_width = self.monitor.deg_coord_x.shape[0]
    #     num_pixels_height = self.monitor.deg_coord_x.shape[1]
    #
    #     if self.coordinate == 'degree':
    #         coord_azi = self.monitor.deg_coord_x
    #         coord_alt = self.monitor.deg_coord_y
    #     elif self.coordinate == 'linear':
    #         coord_azi = self.monitor.lin_coord_x
    #         coord_alt = self.monitor.lin_coord_y
    #     else:
    #         raise ValueError('Do not understand coordinate system: {}. '
    #                          'Should be either "linear" or "degree".'.
    #                          format(self.coordinate))
    #
    #     indicator_width_min = int(self.indicator.center_width_pixel
    #                            - self.indicator.width_pixel / 2)
    #     indicator_width_max = int(self.indicator.center_width_pixel
    #                            + self.indicator.width_pixel / 2)
    #     indicator_height_min = int(self.indicator.center_height_pixel
    #                             - self.indicator.height_pixel / 2)
    #     indicator_height_max = int(self.indicator.center_height_pixel
    #                             + self.indicator.height_pixel / 2)
    #
    #     full_seq = self.background * \
    #                np.ones((num_unique_frames, num_pixels_width, num_pixels_height), dtype=np.float32)
    #
    #     #no duration of each_frame
    #     dis_bool = get_warped_gridlocs(
    #         deg_coord_alt=coord_alt,
    #         deg_coord_azi=coord_azi,
    #         grid_locs=self.grid_locs,
    #         width=self.probe_size[0],
    #         height=self.probe_size[1],
    #         ori=self.probe_orientation,
    #     )
    #    	###
    #     for i, frame in enumerate(self.frames_unique):
    #         if frame[0] == 1.:
    #             #need to check since subclassed -- TODO rewrite
    #             disp_mat = get_warped_probes_with_dis_bool(
    #                 dis_bool=dis_bool,
    #                 probes=frame[1],
    #                 background_color=self.background
    #             )
    #
    #             full_seq[i] = disp_mat
    #
    #         full_seq[i, indicator_height_min:indicator_height_max,
    #         indicator_width_min:indicator_width_max] = frame[3]
    #
    #     mondict = dict(self.monitor.__dict__)
    #     indicator_dict = dict(self.indicator.__dict__)
    #     indicator_dict.pop('monitor')
    #     SNdict = dict(self.__dict__)
    #     SNdict.pop('monitor')
    #     SNdict.pop('indicator')
    #     full_dict = {'stimulation': SNdict,
    #                  'monitor': mondict,
    #                  'indicator': indicator_dict}
    #
    #     return full_seq, full_dict

    def _format_probe_info(self, frame):
        return frame[1]
