from __future__ import print_function

import time

import usb.core
import usb.util
from usb.core import USBError

"""
Adapted for lcr4500 from https://github.com/csi-dcsc/Pycrafter6500

DLPC350 is the controller chip on the LCR4500.

Docs: http://www.ti.com/lit/ug/dlpu010f/dlpu010f.pdf
Doc strings adapted from dlpc350_api.cpp source code.

To connect to LCR4500, install libusb-win32 driver. Recommended way to do is this is
with Zadig utility (http://zadig.akeo.ie/)
"""


def conv_len(a, l):
    """
    Function that converts a number into a bit string of given length

    :param a: number to convert
    :param l: length of bit string
    :return: padded bit string
    """
    b = bin(a)[2:]
    padding = l - len(b)
    b = '0' * padding + b
    return b


def bits_to_bytes(a, reverse=True):
    """
    Function that converts bit string into a given number of bytes

    :param a: bites to convert
    :param reverse: whether or not to reverse the byte list
    :return: list of bytes
    """
    bytelist = []

    # check if needs padding
    if len(a) % 8 != 0:
        padding = 8 - len(a) % 8
        a = '0' * padding + a

    # convert to bytes
    for i in range(len(a) // 8):
        bytelist.append(int(a[8 * i:8 * (i + 1)], 2))

    if reverse:
        bytelist.reverse()
    return bytelist


class dlpc350(object):
    """
    Class representing dmd controller.
    Can connect to different DLPCs by changing product ID. Check IDs in
    device manager.
    """
    def __init__(self, device, uv=False):
        """
        connects device

        :param device: lcr4500 usb device
        """
        self.dlpc = device
        self.uv = uv

    def command(self,
                mode,
                sequence_byte,
                com1,
                com2,
                data=None):
        """
        Sends a command to the dlpc
        :param mode: whether reading or writing
        :param sequence_byte:
        :param com1: command 1
        :param com2: command 3
        :param data: data to pass with command
        """

        buffer = []

        if mode == 'r':
            flagstring = 0xc0  # 0b11000000
        else:
            flagstring = 0x40  # 0b01000000

        data_len = conv_len(len(data) + 2, 16)
        data_len = bits_to_bytes(data_len)

        buffer.append(flagstring)
        buffer.append(sequence_byte)
        buffer.extend(data_len)
        buffer.append(com2)
        buffer.append(com1)

        # if data fits into single buffer, write all and fill
        if len(buffer) + len(data) < 65:
            for i in range(len(data)):
                buffer.append(data[i])

            # append empty data to fill buffer
            for i in range(64 - len(buffer)):
                buffer.append(0x00)

            self.dlpc.write(1, buffer)

        # else, keep filling buffer and pushing until data all sent
        else:
            for i in range(64 - len(buffer)):
                buffer.append(data[i])

            self.dlpc.write(1, buffer)
            buffer = []

            j = 0
            while j < len(data) - 58:
                buffer.append(data[j + 58])
                j += 1

                if j % 64 == 0:
                    self.dlpc.write(1, buffer)
                    buffer = []

            if j % 64 != 0:
                while j % 64 != 0:
                    buffer.append(0x00)
                    j += 1

                self.dlpc.write(1, buffer)

        # wait a bit between commands
        # time.sleep(0.02)
        # time.sleep(0.02)

        # done writing, read feedback from dlpc
        try:
            self.ans = self.dlpc.read(0x81, 64)
            # self.read_reply()
        except USBError as e:
            print('USB Error:', e)

        time.sleep(0.02)

    def read_reply(self):
        """
        Reads in reply
        """
        for i in self.ans:
            print(hex(i), '(', i, ')')
        print(' ')

    def set_black_test_pattern(self):
        """
        Sets the default internal test pattern as solid black.
        """
        self.command('w', 0x00, 0x12, 0x03, [0x00])
        self.command('w', 0x00, 0x12, 0x04, [0x00 for k in range(12)])

    def set_power_mode(self, do_standby=False):
        """
        The Power Control places the DLPC350 in a low-power state and powers down the DMD interface. Standby mode should
        only be enabled after all data for the last frame to be displayed has been transferred to the DLPC350. Standby
        mode must be disabled prior to sending any new data.
        (USB: CMD2: 0x02, CMD3: 0x00)

        :param do_standby: True = Standby mode. Places DLPC350 in low power state and powers down the DMD interface
                           False = Normal operation. The selected external source will be displayed
        """
        do_standby = int(do_standby)
        self.command('w', 0x00, 0x02, 0x00, [do_standby])

    def set_input_source(self, mode='interface', bit_depth=30):
        """
        Selects the input source.
        (USB: CMD2: 0x1A, CMD3: 0x00)

        :param mode: 0 = interface (parallel)
                     1 = test_pattern
        :param bit_depth: parallel interface bit depth: 0 = 30 bits
                                                        1 = 24
                                                        2 = 20
                                                        3 = 16
                                                        4 = 10
                                                        5 = 8
        """
        modes = ['interface', 'test_pattern']
        if mode in modes:
            mode = conv_len(modes.index(mode), 3)

        bits = [30, 24, 20, 16, 10, 8]
        if bit_depth in bits:
            bit_depth = conv_len(bits.index(bit_depth), 3)

        payload = bits_to_bytes(bit_depth + mode)

        self.command('w', 0x00, 0x1a, 0x00, payload)

    def set_black(self):
        """
        Sets projector output to black by setting display mode to test pattern which is solid black
        """
        self.set_input_source(mode='test_pattern')

    def initialize(self):
        """
        Initializes projector by setting default internal pattern as black and put it on
        """
        if self.uv:
            self.set_led_states(uv=1)
        else:
            self.set_led_states(red=1, green=1, blue=1)
        self.set_black_test_pattern()
        self.set_black()

    def set_display_mode(self, mode='video'):
        """
        Selects the input mode for the projector.
        (USB: CMD2: 0x1A, CMD3: 0x1B)

        :param mode: 0 = video mode
                     1 = pattern mode
        """
        modes = ['video', 'pattern']
        if mode in modes:
            mode = modes.index(mode)

        self.command('w', 0x00, 0x1a, 0x1b, [mode])

    def get_led_states(self):
        """
        Gets the LEDs states of the LCR.
        """
        self.command('r', 0x00, 0x1a, 0x07, [])
        # self.read_reply()

    def set_led_states(self, red=0, green=0, blue=0, uv=0):
        """
        Enables/disables the LEDs of the LCR.
        """
        bits_val = 4*uv if self.uv else red + 2*green + 4*blue
        payload = bits_to_bytes(conv_len(bits_val, 8))
        self.command('w', 0x00, 0x1a, 0x07, payload)

    def power_down(self):
        """
        Puts LCR4500 into standby mode.
        """
        self.set_power_mode(do_standby=True)

    def power_up(self):
        """
        Wakes LCR4500 up from standby mode.
        """
        self.set_power_mode(do_standby=False)

    def enable(self):
        """
        Puts LCR4500 into video mode.
        """
        self.set_input_source()

    def disable(self):
        """
        Puts LCR4500 into black test pattern.
        """
        self.set_black()

    def current_color_mode(self):
        """
        Gets LCR4500 current color mode.
        """
        red, green, blue = self.get_led_states()

        if self.uv:
            return 'uv' if bool(blue) else ''

        if bool(red) and bool(green) and bool(blue):
            return 'rgb'

        elif bool(red) and not bool(green) and not bool(blue):
            return 'red'

        elif not bool(red) and bool(green) and not bool(blue):
            return 'green'

        elif not bool(red) and not bool(green) and bool(blue):
            return 'blue'

        else:
            return ''

    def color_mode(self, color="rgb"):
        """
        Puts LCR4500 into given color mode.
        """
        red = (color == 'rgb' or color == 'red')
        green = (color == 'rgb' or color == 'green')
        blue = (color == 'rgb' or color == 'blue' or color == 'uv')

        self.set_led_states(red=red, green=green, blue=blue)

        # print(self.current_color_mode())

    def disconnect(self):
        """
        Releases LCR4500 USB connection.
        """
        self.dlpc.reset()
        del self.dlpc
        del self

def connect(vid=0x0451, pid=0x6401, uv=False):
    """
    Connects to LCR4500 as a USB device.
    """
    try:
        device = usb.core.find(find_all=True, idVendor=vid, idProduct=pid)
        if uv:
            next(device)
        device = next(device)
        device.set_configuration()
        lcr = dlpc350(device)
        lcr.initialize()
        print('LCR4500 (UV) connected!' if uv else 'LCR4500 connected!')
        return lcr

    except StopIteration:
        print('USB connection failed: no LCR4500 (UV) found.' if uv else 'USB connection failed: no LCR4500 found.')
        return None
    
    except usb.core.NoBackendError:
        print('No driver found for LCR4500. You should install libusb for your projectors using Zadig.')
        return None