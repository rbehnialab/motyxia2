"""class to handle spectra
"""

import numpy as np
import pandas as pd
from scipy.interpolate import interp1d

# TODO illuminant and reflectant object

WL_COLUMNS = set(
    ['wl', 'wls', 'wavelength', 'wavelengths',
     'wavelength (nm)', 'wavelengths (nm)',
     'wavelength(nm)', 'wavelengths(nm)'])


def _load_data(filename):
    if filename.endswith('.npy'):
        data = np.load(filename)

    elif filename.endswith('.csv'):
        data = pd.read_csv(filename)

    else:
        raise NameError(
            f'filename does not end '
            f'with .csv or .npy, but with '
            f'{filename.rsplit(".", 1)[-1]}')

    return data

def _return_arr(data, ndim=1):

    if isinstance(data, str):
        data = _load_data(data)

    if not isinstance(data, np.ndarray):
        data = np.array(data)

    if data.ndim != ndim:
        data = np.squeeze(data)

    if data.ndim != ndim:
        raise ValueError(
            f'data is not correct dimensionality: '
            f'{data.ndim} dimensions instead of {ndim}')

    return data

def _find_optimal_wl(*wls):
    """for arrays of wavelengths find a new encompassing array
    of wavelengths
    """
    wl_max = np.min([np.max(wl) for wl in wls])
    wl_min = np.max([np.min(wl) for wl in wls])
    wl_diff = np.max([np.diff(wl).mean() for wl in wls])
    new_wl = np.arange(
        wl_min, np.nextafter(wl_max, wl_max+wl_diff), wl_diff)

    return new_wl

def _interpolate_spectra(wl1, spectra1, wl2, spectra2):
    """interpolate two spectra to the same wavelengths

    Returns
    -------
    new_wl : np.ndarray
    new_spectra1 : np.ndarray
    new_spectra2 : np.ndarray
    """
    new_wl = _find_optimal_wl(wl1, wl2)

    new_spectra1 = interp1d(wl1, spectra1, axis=0)(new_wl)
    new_spectra2 = interp1d(wl2, spectra2, axis=0)(new_wl)

    return new_wl, new_spectra1, new_spectra2


def concat_spectra(spectras):
    """concatenate spectrum and spectra instances
    """
    wls = [spectra.wl for spectra in spectras]
    new_wl = _find_optimal_wl(*wls)
    #what to do with duplicate names?
    names = []
    intensities = []
    new_spectra = np.zeros((len(new_wl), len(spectras)))
    #loop over spectra
    for n, spectra in enumerate(spectras):
        _spectra = interp1d(spectra.wl, spectra.s, axis=0)(new_wl)
        #append names and intensities
        if isinstance(spectra, Spectrum):
            names.append(spectra.name)
            intensities.append(spectra.i)
        elif isinstance(spectra, Spectra):
            names.extend(spectra.names)
            intensities.extend(spectra.i.tolist())
        else:
            raise TypeError(f'not spectra type: {type(spectra)}')

        new_spectra[:, n] = _spectra

        if isinstance(spectra, Filter) or isinstance(spectra, Filters):
            raise NotImplementedError('concatenating filters')

    return Spectra(
        new_spectra, new_wl, names=names,
        intensities=intensities
    )


class Spectrum:
    """base class to handle spectra

    Parameters
    ----------
    spectrum : numpy.ndarray or pandas.DataFrame or pandas.Series
    wavelengths : numpy.ndarray, optional
    intensity : float, optional
    name : str, optional
    """


    def __init__(
        self, spectrum, wavelengths=None,
        intensity=None, name=None, norm_over_wl=False):

        spectrum, wavelengths, name = self._process_data(
            spectrum, wavelengths, name)

        if intensity is None:
            if norm_over_wl:
                intensity = np.sum(spectrum) * np.mean(np.diff(wavelengths))
            else:
                intensity = np.sum(spectrum)

        spectrum = spectrum / np.sum(spectrum)

        if name is None:
            name = 'spectrum'

        self.spectrum = spectrum
        self.name = name
        self.wavelengths = wavelengths
        self.intensity = intensity

    @staticmethod
    def load(filename):
        return _load_data(filename)

    @staticmethod
    def _returnarray(data, ndim=1):
        return _return_arr(data, ndim)

    def _process_data(self, spectrum, wavelengths, name):

        if isinstance(spectrum, str):
            spectrum = self.load(spectrum)

        if wavelengths is None:

            if isinstance(spectrum, np.ndarray):

                wavelengths = spectrum[:,0]
                spectrum = spectrum[:,1]

            if isinstance(spectrum, pd.DataFrame):

                columns = set(spectrum.columns)
                wl_columns = columns & WL_COLUMNS
                other_columns = columns - WL_COLUMNS

                if len(wl_columns) == 1 and len(other_columns) == 1:
                    wl_column = wl_columns.pop()
                    wavelengths = np.array(spectrum[wl_column])
                    other_column = other_columns.pop()
                    spectrum = np.array(spectrum[other_column])

                    if name is None:
                        name = other_column

                else:
                    raise ValueError(
                        'more than one wavelength or spectrum column'
                    )

            else:
                raise TypeError(
                    f'spectrum must be numpy.ndarray '
                    f'or pandas.DataFrame, but is {type(spectrum)}')

        wavelengths = self._returnarray(wavelengths)
        spectrum = self._returnarray(spectrum)

        argsort = np.argsort(wavelengths)
        wavelengths = wavelengths[argsort]
        spectrum = spectrum[argsort]

        return spectrum, wavelengths, name

    def plot(self, ax):
        """
        """
        ax.plot(self.wl, self.s * self.i, label=self.name)

    @property
    def absolute(self):
        return self * self.intensity

    @property
    def i(self):
        return self.intensity

    @property
    def s(self):
        return self.spectrum

    @property
    def wl(self):
        return self.wavelengths

    @property
    def wl_min(self):
        return np.min(self.wl)

    @property
    def wl_max(self):
        return np.max(self.wl)

    @property
    def wl_range(self):
        return (self.wl_min, self.wl_max)

    @property
    def peak(self):
        return self.wl[np.argmax(self.s)]

    @property
    def wpeak(self):
        return np.average(self.wl, weights=self.s)

    def __repr__(self):
        return (
            "Spectrum("
            "peak={:.2f}, wpeak={:.2f}, "
            "wl_range=({:.2f}, {:.2f}), "
            "intensity={:.2e}"
            ")"
        ).format(
            self.peak, self.wpeak, self.wl_min, self.wl_max,
            self.i
        )

    def _interpolate(self, other):
        """interpolate two spectrum instances to the same wavelengths

        Returns
        -------
        new_wl : np.ndarray
        new_self : np.ndarray
        new_other : np.ndarray
        """
        return _interpolate_spectra(
            self.wl, self.s, other.wl, other.s
        )

    def smooth(self):
        """smooth spectrum
        """

    def __add__(self, other):
        """adding to spectra together
        """
        if isinstance(other, Spectrum):
            if np.all(self.wl == other.wl):
                return Spectrum(
                    self.s + other.s,
                    self.wl,
                    self.i + other.i
                )
            else:
                #incase the wavelength steps are not equal interpolate
                new_wl, new_self, new_other = self._interpolate(other)

                #create new spectrum instance
                return Spectrum(
                    new_self + new_other,
                    new_wl,
                    self.i + other.i
                )
        else:
            raise TypeError(f'other must be type '
                            f'Spectrum, but is {type(other)}')

    def __mul__(self, other):
        """multiplication equals filtering or increasing intensity
        """
        if isinstance(other, Spectrum):
            if not isinstance(self, Filter) and not isinstance(other, Filter):
                raise ValueError(
                    'Two spectra do not multiply!'
                    ' One must be a filter.'
                    )

            if np.all(self.wl == other.wl):
                return Spectrum(
                    spectrum=self.s * other.s * self.i * other.i,
                    wavlengths=self.wl,
                )
            else:

                assert np.mean(np.diff(other.wl)) == np.mean(np.diff(self.wl))

                #incase the wavelength steps are not equal interpolate
                new_wl, new_self, new_other = self._interpolate(other)

                #create new spectrum instance
                return Spectrum(
                    spectrum=new_self * self.i * other.i * new_other,
                    wavelengths=new_wl,
                )

        elif isinstance(other, Spectra):
            if not isinstance(self, Filter) and not isinstance(other, Filters):
                raise ValueError(
                    'Two spectra do not multiply!'
                    ' One must be a filter.'
                    )

            # TODO handling wavelength difference
            # only accurate with same wavelength difference
            new_wl, new_self, new_other = self._interpolate(other)

            return Spectra(
                spectra=self.i * new_self[:, None] * new_other * other.i[None, :],
                wavelengths=new_wl,
            )

        elif isinstance(other, (float, int)):
            return Spectrum(
                spectrum=self.s,
                wavelengths=self.wl,
                intensity=self.i * other,
                name=self.name
            )

        else:
            raise TypeError(f'other must be type int or '
                            f'Spectrum, but is {type(other)}')

class Spectra:
    """class to handle multiple spectra
    """

    def __init__(
        self, spectra, wavelengths=None,
        intensities=None, names=None):

        spectra, wavelengths, names = self._process_data(
            spectra, wavelengths, names)

        if intensities is None:
            intensities = np.sum(spectra, axis=0)

        if isinstance(intensities, (int, float)):
            intensities = [intensities] * spectra.shape[1]
            # intensity = intensities
            # intensities = np.sum(spectra, axis=0)
            # intensities *= intensity/np.sum(intensities)

        spectra = spectra / np.sum(spectra, axis=0, keepdims=True)

        if names is None:
            names = [f'spectra{i}' for i in range(spectra.shape[1])]

        self.spectra = spectra
        self.names = names
        self.wavelengths = wavelengths
        self.intensities = np.array(intensities)

    def fit_spectrum(self, spectrum):
        """
        """

        intensity = spectrum.i

        wl, spectra, spectrum = self._interpolate(spectrum)

        spectra *= self.intensities[None, :]
        spectrum *= intensity

        return np.linalg.lstsq(spectra, spectrum)

    @staticmethod
    def load(filename):
        return _load_data(filename)

    @staticmethod
    def _returnarray(data, ndim=1):
        return _return_arr(data, ndim)

    def _process_data(self, spectra, wavelengths, names):

        if isinstance(spectra, str):
            spectra = self.load(spectra)

        if isinstance(wavelengths, str):
            wavelengths = self.load(wavelengths)

        if wavelengths is None:

            if isinstance(spectra, np.ndarray):
                wavelengths = spectra[:, 0]
                spectra = spectra[:, 1:]

            elif isinstance(spectra, pd.DataFrame):

                column = set(spectra.columns) & WL_COLUMNS

                if len(column) == 1:
                    column = column.pop()
                    wavelengths = spectra[column]
                    spectra = spectra.drop(
                        column, axis=1)

                else:
                    raise NameError(
                        'no or more than one wavelength column in dataframe:'
                        f' {column}'
                    )

                if names is not None:
                    spectra = spectra[names]
                else:
                    names = list(spectra.columns)

        elif isinstance(spectra, pd.DataFrame) and names is None:
            names = list(spectra.columns)

        spectra = self._returnarray(spectra, 2)
        wavelengths = self._returnarray(wavelengths)

        argsort = np.argsort(wavelengths)
        wavelengths = wavelengths[argsort]
        spectra = spectra[argsort]

        if names is not None:
            if len(names) != spectra.shape[1]:
                raise ValueError(
                    f'number of names  ({len(names)}) given do not '
                    f'match number of spectra ({spectra.shape[1]}).')

        return spectra, wavelengths, names

    def append(self, spectra):
        """append spectra or a spectrum
        """
        return concat_spectra([self, spectra])

    def plot(self, ax):
        """
        """
        for s, i, name in zip(self.s.T, self.i, self.names):
            ax.plot(self.wl, s*i, label=name)
        ax.legend()

    @property
    def i(self):
        return self.intensities

    @property
    def alli(self):
        return np.sum(self.intensities)

    @property
    def s(self):
        return self.spectra

    @property
    def wl(self):
        return self.wavelengths

    @property
    def wl_min(self):
        return np.min(self.wl)

    @property
    def wl_max(self):
        return np.max(self.wl)

    @property
    def wl_range(self):
        return (self.wl_min, self.wl_max)

    @property
    def peaks(self):
        return self.wl[np.argmax(self.s, axis=0)]

    @property
    def wpeaks(self):
        return np.array([
            np.average(self.wl, weights=s)
            for s in self.s.T
            ])

    def _interpolate(self, other):
        """interpolate spectra to the same wavelengths

        Returns
        -------
        new_wl : np.ndarray
        new_self : np.ndarray
        new_other : np.ndarray
        """
        return _interpolate_spectra(
            self.wl, self.s, other.wl, other.s
        )

    def __repr__(self):
        return (
            "Spectrum("
            "peaks={}, wpeak={}, "
            "wl_range=({:.2f}, {:.2f}), "
            "intensities={}, "
            "overall_intensity={:.2e}"
            ")"
        ).format(
            self.peaks, self.wpeaks, self.wl_min, self.wl_max,
            self.i, self.alli
        )

    def __mul__(self, other):
        """power method
        """
        # TODO
        raise NotImplementedError(
            'Multiplication method Spectra class')

    def __add__(self, other):
        raise NotImplementedError('Add method Spectra class')


class Filter(Spectrum):

    def __init__(
        self, spectrum, wavelengths=None,
        name=None, relative=True
    ):

        spectrum, wavelengths, name = self._process_data(
            spectrum, wavelengths, name)

        if relative:
            spectrum = spectrum/np.max(spectrum)

        if name is None:
            name = 'filter'

        self.name = name
        self.wavelengths = wavelengths
        self.intensity = 1
        self.spectrum = spectrum
        if not relative:
            raise NotImplementedError('non relative filters')
        self.relative = relative


class Filters(Spectra):

    def __init__(
        self, spectra, wavelengths=None,
        names=None, relative=True
    ):

        spectra, wavelengths, names = self._process_data(
            spectra, wavelengths, names)

        if relative:
            spectra = spectra/np.max(spectra, axis=0, keepdims=True)

        if names is None:
            names = [
                f'filter{i}'
                for i in range(spectra.shape[1])]

        self.spectra = spectra
        self.names = names
        self.wavelengths = wavelengths
        self.intensities = np.ones(spectra.shape[1])
        if not relative:
            raise NotImplementedError('non relative filters')
        self.relative = relative
