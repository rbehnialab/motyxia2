"""
Import projector modules
"""

from collections import namedtuple
import numpy as np
import time

from psychopy import visual
from psychopy.monitors import Monitor
from psychopy.visual import globalVars
from psychopy.visual.windowframepack import ProjectorFramePacker
from pyglet import gl

try:
    import u3
except ImportError:
    print('LabJack Python module not present')

try:
    import nidaqmx
    import nidaqmx.system
    from nidaqmx.stream_writers import AnalogSingleChannelWriter
    NI = True
except ImportError:
    NI = False
    print('NI DAQmx python module not imported.')

from . import dlpc350
from .abstract import Params
from .allen import monitor as allen_monitor
from .fullfield import devices
from .fullfield.stimuli import FfStimulus

#for dlpc350
VID = 0x0451
PID = 0x6401


class Projectors(namedtuple('Projectors', ['rgb', 'uv'])):

    def __new__(cls, rgb_projector, uv_projector, vid=VID, pid=PID):
        """
        initialize projector object
        """
        projectors = {}

        if rgb_projector is not None:
            projectors['rgb'] = dlpc350.connect(
                vid=VID, pid=PID)
        else:
            projectors['rgb'] = None

        if uv_projector is not None:
            projectors['uv'] = dlpc350.connect(
                vid=VID, pid=PID, uv=True)
        else:
            projectors['uv'] = None

        return super().__new__(cls, **projectors)

    def enable_projectors(self):
        for projector in self:
            if projector is not None:
                projector.enable()

    def disable_projectors(self):
        for projector in self:
            if projector is not None:
                projector.disable()

    def disconnect(self):
        for projector in self:
            if projector is not None:
                projector.disconnect()

    def color_mode_change(self, proj_color_change, frame):
        #TODO separate rgb from uv
        if proj_color_change[frame] is not None:
            self.enable_color(proj_color_change[frame])

    def enable_color(self, color_mode):
        if self.rgb is not None \
            and color_mode in ['rgb', 'green', 'blue', 'red']:
            self.rgb.color_mode(color=color_mode)

class Window(Params):

    def __init__(
        self,
        color_mode=None,
        has_u3=False,
        trigger_device=None,
        trigger_frequency=1,
        rgb_projector=None,
        uv_projector=None,
        alignment_shift=None,
        framepack=False,
        position=(0,0),
        fullscreen=True,
        offset=(0,0),
        scale=(1,1),
        single_tasks=False,
        indicator_params={},
        monitor_params={},
        ao_params=[],
        background_params={},
        opsin_sensitivities=None,
        name=None, **kwargs
        ):

        self.name = name
        self.params = monitor_params
        self.single_tasks = single_tasks
        self.opsin_sensitivities = opsin_sensitivities
        self.ao_params = ao_params.copy()
        self.indicator_params = indicator_params.copy()
        self.indicator_params.pop('name', None)
        self.color_mode = background_params.get('color_mode', None)


        self.background_params = background_params
        self.has_u3 = has_u3
        self.trigger_frequency = trigger_frequency
        self.trigger_device = trigger_device
        self.rgb_projector = rgb_projector
        self.uv_projector = uv_projector
        self.alignment_shift = alignment_shift
        self.framepack = framepack
        self.framepackers = None
        self.position = position
        self.fullscreen = fullscreen
        self.offset = offset
        self.scale = scale
        self.u3 = None
        self.wins = None

        self.projectors = Projectors(rgb_projector, uv_projector)

        self.system = []
        if ao_params:
            self.system.append('ao')
        if rgb_projector is not None:
            self.system.append('rgb')
        if uv_projector is not None:
            self.system.append('uv')
        if (trigger_device is not None) and (trigger_frequency > 0):
            self.has_ni_sync = True
        else:
            self.has_ni_sync = False

        if 'rgb' not in self.system and 'uv' not in self.system:
            self.has_projectors = False
        else:
            self.has_projectors = True

        if 'ao' in self.system:
            self.has_ao = True
        else:
            self.has_ao = False

        self.system = tuple(self.system)

        if rgb_projector is None or uv_projector is None:
            self.dual = False
        else:
            self.dual = True

        if ao_params:
            self.set_ao_devices()
            if not "arduino_pin" in self.ao_params[0]:
                self.set_monitor()
        else:
            self.set_monitor()

        if self.has_ao:

            background_kws = {}

            background_spectrum = background_params.pop('background_spectrum', None)
            background_intensity = background_params.pop('background_intensity', None)

            if background_spectrum is None:
                for key, value in background_params.items():
                    if key not in ['uv_intensity', 'rgb_intensity', 'color_mode']:

                        background_kws[key] = value

            ffstim = FfStimulus(
                self.ao_devices, background=background_kws,
                background_spectrum=background_spectrum,
                background_intensity=background_intensity
            )

            self.ao_background = {}

            for key, value in ffstim.background.items():
                self.ao_background[key] = [value]

            print("background values: \n{}".format(self.ao_background))

            print("fit of background spectrum \n{}".format(ffstim.bg_fit))

        else:

            self.ao_background = {}


    def set_ao_devices(self):
        """Set NI DAQmx devices
        """
        if "arduino_pin" in self.ao_params[0]:
            self.ao_devices = devices.ArduinoDevices(
            *self.ao_params,
            single_tasks=True,
            opsin_sensitivities=self.opsin_sensitivities
        )
        else:
            self.ao_devices = devices.AoDevices(
            *self.ao_params, refresh_rate=self.refresh_rate,
            trigger_frequency=self.trigger_frequency,
            trigger_device=self.trigger_device,
            single_tasks=self.single_tasks,
            opsin_sensitivities=self.opsin_sensitivities
        )

    def create_task(self):
        if self.has_ao and "NI" in self.ao_params[0]:
            self.ao_devices.create()

    def set_monitor(self):
        """set allen monitor
        """
        self.allen_monitor = allen_monitor.Monitor(
            **self.params)
        self.allen_indicator = allen_monitor.Indicator(
            monitor=self.allen_monitor,
            **self.indicator_params)

    def make_wins(self):
        """Method to create windows from window parameters.
        """
        #initialize labjack
        if self.has_u3:
            try:
                import u3
                self.u3 = u3.U3()
            except ImportError:
                print('Could not import labjack')
                self.has_u3 = False
                self.u3 = None
        else:
            self.u3 = None

        if self.has_projectors:
            self.monitor = Monitor(
                'one', width=self['mon_width_cm'])
        else:
            self.monitor = None

        self.wins = []

        if 'rgb' in self.system:
            self.wins.append(visual.Window(
                monitor=self.monitor,
                units='pix',
                colorSpace='rgb',
                winType='pyglet',
                allowGUI=False,
                color=[-1, -1, -1], #TODO
                size=self.resolution,
                pos=self.position,
                fullscr=self.fullscreen,
                viewPos=self.offset,
                viewScale=self.scale,
                screen=self.rgb_projector-1, #zero base index
                useFBO=True,
                # checkTiming=False,
                # waitBlanking=False,
                allowStencil=True))

        if 'uv' in self.system:
            self.wins.append(visual.Window(
                monitor=self.monitor,
                units='pix',
                colorSpace='rgb',
                winType='pyglet',
                allowGUI=False,
                color=[-1, -1, -1], #TODO
                size=self.resolution,
                pos=self.position,
                fullscr=self.fullscreen,
                viewPos=self.offset,
                viewScale=self.scale,
                screen=self.uv_projector-1,
                useFBO=True,
                # checkTiming=False,
                # waitBlanking=False,
                allowStencil=False))

        for w in self.wins:
            w.mouseVisible = True

        self.wins = tuple(self.wins)

    def close_wins(self):
        if self.u3 is not None:
            self.u3.close()
            self.u3 = None

        if self.wins:
            for w in self.wins:
                w.close()

        if self.has_ao:
            self.ao_devices.close()

        self.wins = None
        self.framepackers = None

    def change_color(self, color):
        """method to live update the background of the windows.

        Parameters
        ----------
        color : list
            RGB list used to change window defaults.
        """
        # TODO: implement classproperty to make setter
        try:
            if self.wins:

                color = np.clip(color, -1, 1)

                self.color_mode = color

                for w in self.wins:
                    w.color = color

                # need to flip twice because of
                # how color is updated on back
                # buffer
                self.flip()
                self.flip()
                if self.framepack:
                    for i in range(5):
                        self.flip()

        except (ValueError, AttributeError, TypeError):
            pass

    def flip(self):
        """Makes proper calls to flip windows
        """
        if self.wins:
            for w in self.wins:
                w.winHandle.switch_to()
                globalVars.currWindow = w
                gl.glBindFramebufferEXT(
                    gl.GL_FRAMEBUFFER_EXT, w.frameBuffer)
                w.flip()

    def send_trigger(self):
        """Triggers recording device by sending short voltage spike from LabJack
        U3-HV. Spike lasts approximately 0.4 ms if connected via high speed USB
        (2.0). Ensure high enough sampling rate to reliably detect triggers.
        Set to use flexible IO #4 (FIO4) or DAC0.
        """

        if self.has_ni_sync:

            with nidaqmx.Task() as task:

                task.ao_channels.add_ao_voltage_chan(
                    self.trigger_device,
                    max_val=5, min_val=0)

                writer = AnalogSingleChannelWriter(task.out_stream)

                writer.write_one_sample(5)

                if self.trigger_frequency <= 0:

                    time.sleep(0.003) #3msec sleep

                    writer.write_one_sample(0)

                time.sleep(1) # one second delay

                # task.close()

        elif self.has_u3:
            try:
                # DAC output
                DAC0_ON = self.u3.voltageToDACBits(
                    5, dacNumber=0, is16Bits=False)
                DAC0_OFF = self.u3.voltageToDACBits(
                    0, dacNumber=0, is16Bits=False)

                self.u3.getFeedback(
                	u3.DAC0_8(DAC0_ON),
                	u3.WaitLong(3),
                	u3.DAC0_8(DAC0_OFF)
                )
                #self.u3.getFeedback(
                #    self.u3.setFIOState(4, 1),
                #    u3.WaitLong(3),
                #    self.u3.setFIOState(4, 0)
                #)
                #TODO other option
                # voltage spike; 0 is low, 1 is high,
                # on flexible IO #4
                #self.d.setFIOState(4, 1)
                #reset
                #self.d.setFIOState(4, 0)

            except Exception as e:
                print(f'Triggering Error: {e}', 'error')

    def framepacking(self):
        if self.framepack:
            self.framepackers = [
                ProjectorFramePacker(w)
                for w in self.wins
            ]

    def clear_windows(self):

        for w in self.wins:
            w.clearBuffer()
            w.winHandle.switch_to()
            globalVars.currWindow = w
            gl.glBindFramebufferEXT(
                gl.GL_FRAMEBUFFER_EXT, w.frameBuffer)

        self.flip()
        if self.framepack:
            # need at least 5 flips
            # (i.e. total of 6 if only 1 frame drawn)
            for i in range(4):
                self.flip()

    def get_frame_cutoff(self):
        if self.framepack:
            return 1./self.refresh_rate * 3 + 0.005 # 5ms range
        else:
            return 1./self.refresh_rate + 0.005 # 5ms range
