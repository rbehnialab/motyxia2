import json
import pandas as pd
import numpy as np


def read_json(filename):
    with open(filename, 'r') as f:
        data = json.load(f)
    return data


def write_json(filename, data):
    with open(filename, 'w') as f:
        json.dump(data, f, indent=4)


def gaussian(mu, sigma, x):
    """Gaussian function
    """
    return (
        1 / np.sqrt(2 * np.pi * sigma ** 2)
        * np.exp(
            -((x - mu) ** 2)
            /(2 * sigma ** 2)
            )
        )


def json2table(
    data, name='table', delete_url=None,
    edit_url=None, setup_url=None):
    """convert loaded json data into table format for datatables.
    See templates/macros/tables.html
    """
    if data:
        data = pd.DataFrame.from_dict(data, 'index')
        data.index.name = 'name'
        data = data.reset_index().fillna('')
        table = {}
        table['delete_url'] = str(delete_url)
        table['edit_url'] = str(edit_url)
        table['setup_url'] = str(setup_url)
        table['execute'] = 'True'
        table['id'] = name
        table['head'] = list(data.columns)
        table['data'] = data.values
        return table
    else:
        return "None"

def yn_choice(message):
    
    """Ask a yes/no question via input() and return their answer.
    
    "message" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).
    
    The "answer" return value is True for "yes" or False for "no".
    """
    
    choices = 'y/n'
    choice = input("%s (%s) " % (message, choices))
    values = ('y', 'yes', '') if choices == 'y/n' else ('y', 'yes')
    if choice.strip().lower() in values:
       return(True)
    else:
        return(False)
